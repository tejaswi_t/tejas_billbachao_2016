package com.app.Billbachao.selfhelp.contents;

import android.os.Bundle;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.externalsdk.AppsFlyerUtils;
import com.app.Billbachao.selfhelp.contents.fragment.SettingsFragment;

public class SettingsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        initView();
        AppsFlyerUtils.trackEvent(this, AppsFlyerUtils.SELF_HELP);
    }

    void initView() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, new
                SettingsFragment()).commit();
    }
}
