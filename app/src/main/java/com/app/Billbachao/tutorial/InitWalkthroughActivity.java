package com.app.Billbachao.tutorial;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.dashboard.DashboardActivity;
import com.app.Billbachao.registration.contents.activities.RegistrationActivity;
import com.app.Billbachao.tutorial.utils.WalkthroughUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.widget.view.DismissLastViewPager;
import com.app.Billbachao.widget.view.DotPagerIndicator;

public class InitWalkthroughActivity extends BaseActivity {

    DismissLastViewPager mPager;

    DotPagerIndicator mPagerIndicator;

    CustomPagerAdapter mPagerAdapter;

    public static final String LAUNCH_NEXT_SCREEN = "launchNext";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);

        initView();
    }

    void initView() {
        mPager = (DismissLastViewPager) findViewById(R.id.pager);
        mPager.setOnSwipeOutListener(mSwipeOutListener);
        mPagerIndicator = (DotPagerIndicator) findViewById(R.id.pager_indicator);
        mPagerAdapter = new CustomPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPagerIndicator.setViewPager(mPager);
        findViewById(R.id.skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishTutorial();
            }
        });
    }

    private class CustomPagerAdapter extends FragmentPagerAdapter {

        public CustomPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return WalkthroughFragment.getInstance(WalkthroughUtils.PAGES[position]);
        }

        @Override
        public int getCount() {
            return WalkthroughUtils.PAGES.length;
        }
    }

    DismissLastViewPager.OnSwipeOutListener mSwipeOutListener = new DismissLastViewPager.OnSwipeOutListener() {
        @Override
        public void onSwipeOutAtEnd() {
            finishTutorial();
        }
    };

    void finishTutorial() {
        if (getIntent().getBooleanExtra(LAUNCH_NEXT_SCREEN, false)) {
            storeTutorialSeen();
            launchNextScreen();
        }
        finish();
    }

    void launchNextScreen() {
        Intent launchIntent;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences
                (getApplicationContext());
        if (!PreferenceUtils.isRegistered(this)) {
            launchIntent = new Intent(this, RegistrationActivity.class);
        } else {
            launchIntent = new Intent(this, DashboardActivity.class);
        }
        startActivity(launchIntent);
    }

    void storeTutorialSeen() {
        TutorialHelper.setTutorialShown(this, WalkthroughUtils.INITIAL);
    }
}
