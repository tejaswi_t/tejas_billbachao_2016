package com.app.Billbachao.model;

/**
 * Created by BB-001 on 5/9/2016.
 */
public class RechargeSMSBean {

    // Number from witch the sms was send
    private String number="NA";
    // SMS text body
    private String body="NA";

    String call_sms_to = "NA";
    String activity = "NA";
    String call_leg = "NA";
    String total_duration = "NA";
    String total_cost = "NA";
    String data_used = "NA";
    String data_left = "NA";
    String pack_exp = "NA";
    String bal_left = "NA";
    String capture_date = "NA";
    String ussd_text = "NA";

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSms_text() {
        return ussd_text;
    }

    public void setSms_text(String ussd_text) {
        this.ussd_text = ussd_text;
    }

    public String getBal_left() {
        return bal_left;
    }

    public void setBal_left(String bal_left) {
        this.bal_left = bal_left;
    }

    public String getPack_exp() {
        return pack_exp;
    }

    public void setPack_exp(String pack_exp) {
        this.pack_exp = pack_exp;
    }

    public String getCall_sms_to() {
        return call_sms_to;
    }

    public void setCall_sms_to(String call_sms_to) {
        this.call_sms_to = call_sms_to;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getCall_leg() {
        return call_leg;
    }

    public void setCall_leg(String call_leg) {
        this.call_leg = call_leg;
    }

    public String getTotal_duration() {
        return total_duration;
    }

    public void setTotal_duration(String total_duration) {
        this.total_duration = total_duration;
    }

    public String getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(String total_cost) {
        this.total_cost = total_cost;
    }

    public String getData_used() {
        return data_used;
    }

    public void setData_used(String data_used) {
        this.data_used = data_used;
    }

    public String getCapture_date() {
        return capture_date;
    }

    public void setCapture_date(String capture_date) {
        this.capture_date = capture_date;
    }


    public String getData_left() {
        return data_left;
    }

    public void setData_left(String data_left) {
        this.data_left = data_left;
    }
}
