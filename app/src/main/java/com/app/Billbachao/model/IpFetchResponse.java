package com.app.Billbachao.model;

import com.google.myjson.annotations.SerializedName;

/**
 * Created by BB-001 on 5/11/2016.
 */
public class IpFetchResponse {

    @SerializedName("ip")
    public String ipAddress;

    public String getIpAddress() {
        return ipAddress;
    }
}
