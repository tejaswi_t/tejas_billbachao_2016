package com.app.Billbachao;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.app.Billbachao.common.menus.CommonMenuHelper;
import com.app.Billbachao.externalsdk.google.AppIndexBehavior;
import com.app.Billbachao.externalsdk.google.AppIndexHelper;
import com.app.Billbachao.shareandrate.SendFeedbackTask;
import com.app.Billbachao.shareandrate.contents.ContactUsDialog;
import com.app.Billbachao.shareandrate.contents.FeedbackDialog;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by mihir on 10-03-2016.
 */
public abstract class BaseActivity extends AppCompatActivity implements FeedbackDialog
        .OnFeedbackListener, ContactUsDialog.OnContactUsCallback {

    CoordinatorLayout mCoordinatorLayout;

    ProgressDialog mProgressDialog;

    CommonMenuHelper mMenuHelper;

    GoogleApiClient mClient;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMenuHelper = new CommonMenuHelper(this, isCartItemSupported());
        if (this instanceof AppIndexBehavior) {
            mClient = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initActionBar();
        initSnack();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (this instanceof AppIndexBehavior) {
            AppIndexBehavior appIndexBehavior = (AppIndexBehavior) this;
            AppIndexHelper.sendAppIndexEvent(mClient, appIndexBehavior.getAppIndexTitle(),
                    appIndexBehavior.getAppIndexAppendedUrl(), true);
        }
    }

    @Override
    protected void onStop() {
        if (this instanceof AppIndexBehavior) {
            AppIndexBehavior appIndexBehavior = (AppIndexBehavior) this;
            AppIndexHelper.sendAppIndexEvent(mClient, appIndexBehavior.getAppIndexTitle(),
                    appIndexBehavior.getAppIndexAppendedUrl(), false);
        }
        super.onStop();
    }

    /**
     * This function is for assigning back button on actionbar.
     * Should be overriden as false for those not wanting back functionality
     *
     * @return
     */
    protected boolean isHomeAsUpEnabled() {
        return true;
    }

    private void initActionBar() {
        if (isHomeAsUpEnabled()) {
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenuHelper.onCreateOptionsMenu(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mMenuHelper.onPrepareOptionsMenu(menu);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mMenuHelper.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        mMenuHelper.destroy();
        super.onDestroy();
    }

    /**
     * Override this in sub class to hide cart
     *
     * @return
     */
    protected boolean isCartItemSupported() {
        return true;
    }

    private void initSnack() {
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
    }

    public boolean showSnack(int stringResId) {
        return showSnack(getString(stringResId));
    }

    public boolean showSnack(String string) {
        if (mCoordinatorLayout != null) {
            Snackbar.make(mCoordinatorLayout, string, Snackbar.LENGTH_LONG).show();
            return true;
        } else {
            showToast(string);
            return false;
        }
    }

    public void showToast(int stringResId) {
        showToast(getString(stringResId));
    }

    public void showToast(String string) {
        Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
    }


    // Progress dialog common code
    protected void showProgressDialog() {
        showProgressDialog(null);
    }

    protected ProgressDialog showProgressDialog(String message) {
        initProgressDialog();
        if (!mProgressDialog.isShowing())
            mProgressDialog.show();
        if (message != null) mProgressDialog.setMessage(message);

        return mProgressDialog;
    }

    protected void initProgressDialog() {
        if (mProgressDialog != null) return;
        mProgressDialog = new ProgressDialog(this);
    }

    protected void dismissProgressDialog() {
        if (mProgressDialog == null) return;
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    /**
     * Feedback dialog
     * @param ratingValue
     */
    public void showFeedbackDialog(int ratingValue) {
        FeedbackDialog dialog = FeedbackDialog.getInstance(ratingValue);
        dialog.show(getSupportFragmentManager(), FeedbackDialog.TAG);
    }

    public void showFeedbackDialog() {
        showFeedbackDialog(-1);
    }

    @Override
    public void onFeedbackSent(int rating, String feedback) {
        new SendFeedbackTask(this, rating, feedback, true).trigger();
    }

    @Override
    public void onLaunchContactInfo() {
        ContactUsDialog dialog = ContactUsDialog.getInstance();
        dialog.show(getSupportFragmentManager(), ContactUsDialog.TAG);
    }

    @Override
    public void onFeedbackRequested() {
        showFeedbackDialog();
    }
}