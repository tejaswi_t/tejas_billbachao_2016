package com.app.Billbachao.background;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.app.Billbachao.model.RechargeSMSBean;
import com.app.Billbachao.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BB-001 on 5/9/2016.
 */
public class SmsReader {
    public List<RechargeSMSBean> fetchSMS(Context context) {
        List<RechargeSMSBean> smsList = new ArrayList<RechargeSMSBean>();
        Uri uri = Uri.parse("content://sms/inbox");
        long timeInMilliseconds = 0;
        String where = null;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (!preferences.getBoolean(UploadRechargeSMSTask.RECHARGE_SMS_UPLOADED, false)) {
            timeInMilliseconds = DateUtils.getBeforeDays(30);
        } else {
            timeInMilliseconds = DateUtils.getBeforeDays(7);
        }
        where = "date" + ">?";
        Cursor c = context.getContentResolver().query(uri, null, where, new String[]{"" + timeInMilliseconds}, "date DESC");
        // Read the sms data and store it in the list
        if (c != null) {
            if (c.moveToFirst()) {
                for (int i = 0; i < c.getCount(); i++) {
                    RechargeSMSBean sms = new RechargeSMSBean();

                    if (c.getString(c.getColumnIndexOrThrow("address")).length() <= 10) {
                        sms.setBody(c.getString(c.getColumnIndexOrThrow("body")).toString());
                        sms.setNumber(c.getString(c.getColumnIndexOrThrow("address")).toString());
                        String date = c.getString(c.getColumnIndexOrThrow("date")).toString();
                        date = DateUtils.convertIntoDesiredDateFormat(Long.parseLong(date));
                        sms.setCapture_date(date);
                        smsList.add(sms);
                        c.moveToNext();
                    }
                }
            }
            c.close();
        }
        return smsList;
    }
}
