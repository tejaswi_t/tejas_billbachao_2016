package com.app.Billbachao.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.WebViewActivity;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.common.ConnectionManager;
import com.app.Billbachao.dashboard.DashboardActivity;
import com.app.Billbachao.deals.activity.DealsAddressActivity;
import com.app.Billbachao.deals.activity.DealsBookingWebview;
import com.app.Billbachao.deals.activity.DealsFlowActivity;
import com.app.Billbachao.deals.activity.DealsDetailActivity;
import com.app.Billbachao.deals.activity.DealsListActivity;
import com.app.Billbachao.deals.activity.DealsPaymentReceiptActivity;
import com.app.Billbachao.deals.utils.DealsConstant;
import com.app.Billbachao.deals.models.DealsAddressModel;
import com.app.Billbachao.deals.models.RelianceTvDetailsModel;
import com.app.Billbachao.notifications.gcm.activity.NotificationInboxActivity;
import com.app.Billbachao.payment.contents.activities.PaymentIntermediaryActivity;
import com.app.Billbachao.recharge.cart.CartActivity;
import com.app.Billbachao.recharge.plans.contents.BrowsePlansActivity;
import com.app.Billbachao.recharge.quick.contents.RechargeActivity;
import com.app.Billbachao.recommendations.app.AppRecommendActivity;
import com.app.Billbachao.recommendations.network.contents.NetworkActivity;
import com.app.Billbachao.recommendations.network.contents.RateOperatorActivity;
import com.app.Billbachao.registration.contents.activities.EditProfileActivity;
import com.app.Billbachao.selfhelp.contents.SettingsActivity;
import com.app.Billbachao.tutorial.InitWalkthroughActivity;
import com.app.Billbachao.usage.activities.InternalUsageActivity;
import com.app.Billbachao.usagelog.AccessibilityHelper;
import com.app.Billbachao.usagelog.alerts.contents.PreferenceActivity;
import com.app.Billbachao.usagelog.contents.UsageLogActivity;
import com.app.Billbachao.usagelog.contents.dialogs.UssdDialogFragment;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;

/**
 * Created by mihir on 18-08-2015.
 */
public class UiUtils {

    public static void launchDashboard(Context context) {
        Intent intent = new Intent(context, DashboardActivity.class);
        context.startActivity(intent);
    }

    public static void launchEditProfile(Context context) {
        Intent intent = new Intent(context, EditProfileActivity.class);
        context.startActivity(intent);
    }

    /**
     * Launches usage bifurcation screen (last 30 days usage)
     *
     * @param context
     */
    public static void launchInternalUsage(Context context) {
        Intent intent = new Intent(context, InternalUsageActivity.class);
        context.startActivity(intent);
    }

    /**
     * Picks Summary screen or Settings screen.
     *
     * @param context
     * @param forceAccessibilityLaunch Setting this will prompt to turn on USSD when not running.
     * @return
     */
    public static boolean launchUsageLogOrSettings(Context context, boolean
            forceAccessibilityLaunch) {
        AccessibilityHelper helper = new AccessibilityHelper(context);
        if (helper.isEnabledAndRunning()) {
            return launchUsageSummary(context);
        } else if (context instanceof AppCompatActivity && forceAccessibilityLaunch) {
            showUssdDialog((AppCompatActivity) context, false);
            return false;
        } else {
            return false;
        }
    }

    /**
     * Will launch usage log summary activity
     *
     * @param context
     * @return
     */
    public static boolean launchUsageSummary(Context context) {
        Intent intent = new Intent(context, UsageLogActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
        return true;
    }

    /**
     * Show USSD dialog for guiding user to the settings screen
     *
     * @param activity
     */

    public static void showUssdDialog(AppCompatActivity activity, boolean isFromNotification) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();

        UssdDialogFragment ussdDialogFragment = UssdDialogFragment.newInstance(isFromNotification);

        ussdDialogFragment.show(fragmentManager, UssdDialogFragment.TAG);

    }


    /**
     * Launches app recommendation screen
     *
     * @param context
     */
    public static void launchAppRecommendation(Context context) {
        Intent intent = new Intent(context, AppRecommendActivity.class);
        context.startActivity(intent);
    }

    /**
     * Launches self help screen
     *
     * @param context
     */
    public static void launchSelfHelp(Context context) {
        Intent intent = new Intent(context, SettingsActivity.class);
        context.startActivity(intent);
    }

    public static void launchNotificationInBox(Context context) {
        Intent intent = new Intent(context, NotificationInboxActivity.class);
        context.startActivity(intent);
    }

    /**
     * Launch browse plans for self, used for launching internally, default mode
     *
     * @param context
     */
    public static void launchSelfBrowsePlans(Context context) {
        launchSelfBrowsePlans(context, 0);
    }

    /**
     * Launch browse plans for self
     *
     * @param context
     * @param preferredPlanType Parameter while launching from external, eg. notification ->
     *                          launching data pack plans or roaming etc
     */
    public static void launchSelfBrowsePlans(Context context, int preferredPlanType) {
        if (ConnectionManager.isConnected(context)) {
            Intent intent = new Intent(context, BrowsePlansActivity.class);
            if (preferredPlanType != 0) {
                intent.putExtra(ConstantUtils.PREFERRED_RECHARGE_TYPE, preferredPlanType);
            }
            intent.putExtra(ConstantUtils.LAUNCH_SELF_PLANS, true);
            context.startActivity(intent);
        } else {
            ((BaseActivity) context).showSnack(NetworkErrorHelper.MSG_NETWORK_ERROR);
        }
    }

    /**
     * Payment dialog screen, which will handle code like promo and cashback
     *
     * @param context
     */
    public static void launchPaymentIntermediary(Context context) {
        Intent intent = new Intent(context, PaymentIntermediaryActivity.class);
        context.startActivity(intent);
    }

    /**
     * Cart screen launch
     *
     * @param context
     */
    public static void launchCart(Context context) {
        Intent intent = new Intent(context, CartActivity.class);
        context.startActivity(intent);
    }

    /**
     * Recharge screen launch
     *
     * @param context
     */
    public static void launchRecharge(Context context) {
        Intent intent = new Intent(context, RechargeActivity.class);
        context.startActivity(intent);
    }

    /**
     * Launch browse plans with some params
     *
     * @param circleId
     * @param operatorId
     * @param maxAmount
     */
    public static void launchBrowsePlans(Activity activity, String circleId, String operatorId,
                                         String maxAmount) {
        Intent intent = new Intent(activity, BrowsePlansActivity.class);
        intent.putExtra(PreferenceUtils.CIRCLE, circleId);
        intent.putExtra(PreferenceUtils.OPERATOR, operatorId);
        intent.putExtra(PreferenceUtils.AMOUNT, maxAmount);
        activity.startActivityForResult(intent, ConstantUtils.PLAN_PICKER);
    }

    public static void launchBrowsePlans(Activity activity, String circleId, String operatorId) {
        launchBrowsePlans(activity, circleId, operatorId, ApiUtils.NA);
    }

    /**
     * Launches network screen
     *
     * @param context
     */
    public static void launchNetworkScreen(Context context) {
        Intent intent = new Intent(context, NetworkActivity.class);
        context.startActivity(intent);
    }

    /**
     * Launches preference screen
     *
     * @param context
     */
    public static void launchAlerts(Context context) {
        Intent intent = new Intent(context, PreferenceActivity.class);
        context.startActivity(intent);
    }

    /**
     * Launches rating screen
     * @param context
     */
    public static void launchRateOperator(Context context) {
        Intent intent = new Intent(context, RateOperatorActivity.class);
        context.startActivity(intent);
    }

    /**
     * Launches preference screen
     * @param context
     */
    public static void launchWalkthrough(Context context) {
        Intent intent = new Intent(context, InitWalkthroughActivity.class);
        context.startActivity(intent);
    }

    /**
     * Launch Reliance Deals Detail Activity
     * @param mContext
     * @param bundle
     */
    public static void launchDealsDetailsActivity(Context mContext, Bundle bundle) {
        Intent intent = new Intent(mContext, DealsDetailActivity.class);

        if (bundle != null)
            intent.putExtras(bundle);

        mContext.startActivity(intent);
    }

    /**
     * Launch Deals Screen
     * @param mContext
     * @param from
     */
    public static void launchDealsScreen(Context mContext, String from) {
        Bundle bundle = new Bundle();
        bundle.putString(DealsConstant.KEY_FROM, from);
        Intent intent = new Intent(mContext, DealsListActivity.class);
        intent.putExtras(bundle);
        mContext.startActivity(intent);
    }

    public static void launchDealsFlowActivity(Context mContext, RelianceTvDetailsModel relianceTvDetailsModel, String from, boolean isBookNow) {
        Intent intent = new Intent(mContext, DealsFlowActivity.class);

        Bundle bundle = new Bundle();
        if (relianceTvDetailsModel != null) {
            bundle.putString(DealsConstant.KEY_FROM, from);
            bundle.putParcelable(DealsConstant.KEY_RELIANCE_DETAILS, relianceTvDetailsModel);
            bundle.putBoolean(DealsConstant.KEY_IS_USE_BOOKING, isBookNow);
        }

        intent.putExtras(bundle);

        mContext.startActivity(intent);
    }

    /**
     * Launch WebView Activity
     * @param mContext
     * @param url
     * @param buyOptions
     */
    public static void launchWebViewActivity(Context mContext, String url, String buyOptions) {

        Intent intent = new Intent(mContext, WebViewActivity.class);

        Bundle bundle = new Bundle();
        bundle.putString(WebViewActivity.CONSTANTS.WEB_VIEW_URL, url.trim());
        bundle.putString(WebViewActivity.CONSTANTS.WEB_VIEW_TITLE, buyOptions);

        intent.putExtras(bundle);

        mContext.startActivity(intent);
    }


    /**
     * Launch Deals PaymentReceipt
     * @param mContext
     * @param bundle
     * @param code
     */
    public static void launchDealsPaymentReceiptActivity(FragmentActivity mContext, Bundle bundle, int code){
        Intent intent = new Intent(mContext, DealsPaymentReceiptActivity.class);

        if (bundle != null)
            intent.putExtras(bundle);

        mContext.startActivityForResult(intent, code);
    }


    /**
     * Launch Deals Booking Activity
     * @param mContext
     * @param bundle
     */
    public static void launchDealsBookingActivity(FragmentActivity mContext, Bundle bundle) {
        Intent intent = new Intent(mContext, DealsBookingWebview.class);

        if (bundle != null)
            intent.putExtras(bundle);

        mContext.startActivityForResult(intent, DealsConstant.REQUEST_CODE_BOOKING);
    }

    /**
     * Launch Deal Address Activity
     * @param mContext
     * @param keyFrom
     * @param position
     * @param dealsAddressModel
     */
    public static void launchDealsAddAddressActivity(FragmentActivity mContext, String keyFrom, int position, DealsAddressModel dealsAddressModel) {
        Intent intent = new Intent(mContext, DealsAddressActivity.class);

        Bundle bundle = new Bundle();
        bundle.putString(DealsConstant.KEY_FROM, keyFrom);
        bundle.putParcelable(DealsConstant.KEY_ADDRESS_MODEL, dealsAddressModel);
        bundle.putInt(DealsConstant.KEY_POSITION, position);

        intent.putExtras(bundle);
        mContext.startActivityForResult(intent, DealsConstant.REQUEST_CODE_DEALS_ADDRESS);
    }
}