package com.app.Billbachao.utils;

import android.content.Context;

import java.util.Arrays;

/**
 * Created by mihir on 17-08-2015.
 */
public class CircleUtils {

    public static String[] sCircleId = {"1", "2", "3", "4", "5", "6", "7", "8",
            "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
            "20", "21", "22", "23"};

    public static String[] sCircleNames = {"Andhra Pradesh", "Assam",
            "Bihar and Jharkhand", "Chennai", "Delhi and NCR", "Gujarat",
            "Himachal Pradesh", "Haryana", "Jammu and Kashmir", "Kerala",
            "Karnataka", "Kolkata", "Maharashtra and Goa",
            "Madhya Pradesh and Chhattisgarh", "Mumbai", "North East",
            "Odisha", "Punjab", "Rajasthan", "Tamil Nadu",
            "Uttar Pradesh(East)", "Uttar Pradesh(West) and Uttarakhand",
            "West Bengal"};

    public static String[] sCircleNames_shortCut = {"Andhra Pradesh", "Assam",
            "Bihar", "Chennai", "Delhi", "Gujarat",
            "Himachal Pradesh", "Haryana", "Jammu & Kashmir", "Kerala",
            "Karnataka", "Kolkata", "Maharashtra",
            "Madhya Pradesh", "Mumbai", "North East",
            "Odisha", "Punjab", "Rajasthan", "Tamil Nadu",
            "UP East", "UP West",
            "West Bengal"};

    public static String getCircleNameFromId(String id) {
        int index = Arrays.asList(sCircleId).indexOf(id);
        if (index == -1) {
            return PreferenceUtils.NA;
        }
        return sCircleNames_shortCut[index];
    }

    public static String getCircleCompleteNameFromId(String id) {
        int index = Arrays.asList(sCircleId).indexOf(id);
        return sCircleNames[index];
    }

    public static String getCircleName(Context context) {
        String id = PreferenceUtils.getCircleId(context);
        return getCircleNameFromId(id);
    }

    public static int getCircleIndex(String id) {
        return Arrays.asList(sCircleId).indexOf(id);
    }

    public static String getCircleIdFromName(String name) {
        try {
            int index = Arrays.asList(sCircleNames).indexOf(name);
            return sCircleId[index];
        } catch (ArrayIndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }
        return PreferenceUtils.NA_ID;
    }

    public static boolean isCircleIdMatchFound(String id) {
        return (Arrays.asList(sCircleId).indexOf(id) >= 0);
    }

    // Requirement for considering same circles. Eg. Mumbai - Maharashtra
    public static final int PAIRED_CIRCLES[][] = new int[][]{{4, 20}, {12, 23}, {13, 15}, {15, 13}, {20, 4}, {21, 22}, {22, 21}, {23, 12}};

    /**
     * Returns pair circle
     * @param circle
     * @return Paired circle, if found else same
     */
    public static final int getPairedCircle(int circle) {
        for (int[] pair : PAIRED_CIRCLES) {
            if (circle == pair[0]) return pair[1];
        }
        return circle;
    }
}
