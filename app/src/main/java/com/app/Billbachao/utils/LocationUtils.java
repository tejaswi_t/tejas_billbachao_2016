package com.app.Billbachao.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by mihir on 09-12-2015.
 */
public class LocationUtils {

    private static final String TAG = LocationUtils.class.getName();

    public static String getAddressText(Address address) {
        return getAddressText(address, 0);
    }

    public static String getAddressText(Address address, int from) {
        return getAddressText(address, from, -1);
    }

    public static String getAddressText(Address address, int from, int to) {
        ArrayList<String> addressFragments = new ArrayList<>();

        // Fetch the address lines using getAddressLine, join them, and send them to the thread.
        int end = address.getMaxAddressLineIndex();
        if (to > -1) {
            end = Math.min(end, to);
        }
        for (int i = from; i < end; i++) {
            addressFragments.add(address.getAddressLine(i));
        }
        return TextUtils.join(System.getProperty("line.separator"), addressFragments);
    }

    public static Address getAddress(Context context, double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(context);

        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException | IllegalArgumentException ex) {
            ex.printStackTrace();
            return null;
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size() == 0) {
            return null;
        } else {
            return addresses.get(0);
        }
    }

    public static final String LAST_LATITUDE = "last_latitude", LAST_LONGITUDE =
            "last_longitude", LAST_TIME = "last_location_time", LATITUDE = "latitude", LONGITUDE
            = "longitude", USER_REG_CITY = "user_reg_city";

    public static void storeLocation(Context context, Location location) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context)
                .edit();
        editor.putFloat(LAST_LATITUDE, (float) location.getLatitude());
        editor.putFloat(LAST_LONGITUDE, (float) location.getLongitude());
        editor.putLong(LAST_TIME, location.getTime());
        editor.apply();
    }

    public static CustomLocation getLocation(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        CustomLocation location = new CustomLocation(preferences.getFloat(LAST_LATITUDE, 0),
                preferences.getFloat(LAST_LONGITUDE, 0), preferences.getLong(LAST_TIME, 0));
        return location;
    }

    public static class CustomLocation {

        private float mLatitude, mLongitude;

        private long mTime;

        public CustomLocation(float latitude, float longitude, long time) {
            mLatitude = latitude;
            mLongitude = longitude;
            mTime = time;
        }

        public float getLatitude() {
            return mLatitude;
        }

        public float getLongitude() {
            return mLongitude;
        }

        public long getTime() {
            return mTime;
        }
    }


    /**
     * Get city using lalitude and longitude.
     *
     * @param mContext
     * @param latitude
     * @param longitude
     * @param locale
     * @return
     */
    public static String getAddressFromLatLng(Context mContext, double latitude, double
            longitude, Locale locale) {

        if (locale == null)
            locale = Locale.getDefault();

        Geocoder gcd = new Geocoder(mContext, locale);

        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(latitude, longitude, 1);

            if (addresses.size() > 0) {
                ILog.d(TAG, " current city  | " + addresses.get(0).getLocality() + " |");
                return addresses.get(0).getLocality();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
