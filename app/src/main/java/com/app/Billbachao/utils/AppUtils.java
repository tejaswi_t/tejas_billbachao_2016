package com.app.Billbachao.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by mihir on 10-09-2015.
 */
public class AppUtils {

    public static int getDaysSinceInstalled(Context context) {
        int MILLI_SECONDS_DAY_CONVERTER = 1000 * 60 * 60 * 24;

        long diff = System.currentTimeMillis() - getInstalledTime(context);
        return (int) (diff / MILLI_SECONDS_DAY_CONVERTER);
    }

    static long getInstalledTime(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName
                    (), 0);
            return info.firstInstallTime;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return Long.MAX_VALUE;
    }

}
