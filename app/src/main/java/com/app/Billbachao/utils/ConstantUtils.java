package com.app.Billbachao.utils;

/**
 * Created by mihir on 25-04-2016.
 */
public class ConstantUtils {

    // Plan list launch mode -> Launch from Browse plans/ Quick recharge
    public static final String LAUNCH_SELF_PLANS = "launch_self", LAUNCH_FROM_EXTERNAL =
            "external_launch", PREFERRED_RECHARGE_TYPE = "recharge_type_preferred";

    // Payment screen
    public static final String PLANS = "plans", VALIDATION_RESULT = "validation_data",
            PAYMENT_STATUS = "payment_status";

    // Launch intent ids
    // Plan picker launch from recharge screen
    public static final int PLAN_PICKER = 0x101, CONTACT_PICKER = 0x102;
}
