package com.app.Billbachao.utils.view;

import android.graphics.Color;

/**
 * Created by mihir on 29-03-2016.
 */
public class ColorUtils {

    static final int LIGHTER_ALPHA = (int) (255 * 0.7);

    public static int getColorShade(int alpha, int color) {
        return Color.argb(alpha, Color.red(color), Color.green(color), Color.blue(color));
    }

    public static int getLighterShade(int color) {
        return getColorShade(LIGHTER_ALPHA, color);
    }
}
