package com.app.Billbachao.utils;

import android.content.Context;
import android.telephony.TelephonyManager;

/**
 * Created by mihir on 04-03-2016.
 */
public class TelephonyUtils {

    public static String getIMEINumber(Context context) {

        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context
                .TELEPHONY_SERVICE);
        return manager.getDeviceId();
    }
}
