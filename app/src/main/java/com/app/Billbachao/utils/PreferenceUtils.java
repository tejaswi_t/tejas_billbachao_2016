package com.app.Billbachao.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by mihir on 13-08-2015.
 */
public class PreferenceUtils {

    public static final String PREPAID_POSTPAID = "MODE";

    public static final String PREPAID = "PREPAID", POSTPAID = "POSTPAID";

    // Mobile number value entered by user in info screen
    public static final String MOBILE_NUMBER = "mobileno";

    public static final String SMS_STATUS = "SmsStatus";

    public static final String USER_MODE = "mode";

    public static final String CIRCLE = "planOperatorCircleId", OPERATOR = "planOperatorId", MAIL
            = "user_email", AMOUNT = "amount", MOBVERIFIED = "mobVerified", SMSRECEIVED =
            "SMSRECEIVED";

    public static final String NETWORK_TYPE = "NWtypeM", NETWORK_3G = "3G", NETWORK_2G = "2G", NETWORK_4G = "4G";

    public static final String APP_LANGUAGE = "appLang";

    public final static String KEY = "sKey", PWD_KEY = "pKey";

    public static final String NA = "NA", NA_ID = "0";

    public static final String DATA_THRESHOLD = "data_threshold", TALK_THRESHOLD =
            "talk_threshold", DATA_PERCENT_THRESHOLD = "data_threshold_percent", PLAN_EXPIRY =
            "plan_expiry", BILL_DUE_DAYS_REMAIN = "bill_due_remain", SCHEDULER = "Scheduler", MYBESTPLAN = "MyBestPlan";

    // Preexisting separate preference file created unnecessarily.
    public static final String ACTIVITY_PREFERENCE = "Preference";

    // Plan alerts preferences
    public static final String PLAN_ALERTS = "plan_alerts";

    // Data Usage User choice
    public static final String DATA_USAGE = "data_usage_month";

    // Night call timings
    public static final String NIGHT_TIME_FROM = "nightCall_frm", NIGHT_TIME_TO = "nightCall_to";

    // Coupon preferences
    public static final String COUPON_MODE = "mode", COUPON_CATEGORY = "category";

    // Payment gateway mode
    public static final String BILLDESK_PG_MODE = "1";


    // HandsetDetails preference
    public static final String BASIC_HANDSET_DATA_SYNC = "basicHandsetDataSync";

    public static boolean isPrepaid(Context context) {
        if (context == null) {
            return true;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return (preferences.getString(PREPAID_POSTPAID, PREPAID).equalsIgnoreCase(PREPAID));
    }

    public static String getCType(Context context) {
        if (context == null) {
            return PREPAID;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(PREPAID_POSTPAID, PREPAID).toUpperCase();
    }

    public static String getNetworkType(Context context) {
        if (context == null) {
            return NETWORK_3G;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(NETWORK_TYPE, NETWORK_3G);
    }

    public static String getKey(Context context) {
        if (context == null) {
            return NA;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(KEY, NA);
    }

    public static String getOperatorId(Context context) {
        if (context == null) {
            return NA_ID;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(OPERATOR, NA_ID);
    }

    public static String getCircleId(Context context) {
        if (context == null) {
            return NA_ID;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(CIRCLE, NA_ID);
    }

    public static String getMobileNumber(Context context) {
        if (context == null) {
            return NA;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(MOBILE_NUMBER, NA);
    }

    public static String getMailId(Context context) {
        if (context == null) {
            return NA;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(MAIL, NA);
    }

    public static boolean isSMSReceived(Context context) {
        if (context == null) {
            return false;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(SMSRECEIVED, false);
    }

    public static boolean isMobVerified(Context context) {
        if (context == null) {
            return false;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(MOBVERIFIED, false);
    }

    public static void setMobVerified(Context context, boolean isVerified) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putBoolean(MOBVERIFIED, isVerified).apply();
    }

    public static boolean isRegistered(Context context) {
        if (context == null) {
            return false;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(PreferenceUtils.REGISTRATION_COMPLETE, "false").equalsIgnoreCase("true");
    }

    public static String getUserMode(Context context) {
        if (context == null) {
            return NA;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(USER_MODE, NA);
    }

    public static boolean isNewAlertsOn(Context context) {
        if (context == null) {
            return true;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(PreferenceUtils.PLAN_ALERTS, true);
    }

    public static boolean isBasicHandsetDataSynced(Context context) {
        if (context == null) {
            return false;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(BASIC_HANDSET_DATA_SYNC, false);
    }

    // Additional preferences
    // used to display users maximum data usage calculation
    public static final String MAX_LIMIT_DATA_MB = "maxLimitDataMB";
    // used to show while displaying reco progress
    public static final String NUMBER_OF_PLANS = "numberOfPlans";

    // Preferences for various user status
    public static final String REGISTRATION_COMPLETE = "chkAuth";

    // Preference for plan categories list
    public static final String PLAN_CATEGORY_LIST = "planCategories";

    public static final String getPlanList(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(PLAN_CATEGORY_LIST, "");
    }

    // Preferences for DB_VERSION - Club with DB name
    public static final String DB_VERSION = "db_version";

    // Install info fields
    public static final String MEDIUM = "media_source", CAMPAIGN = "campaign", SOURCE = "source",
            INSTALL_INFO_SAVED = "install_info_saved", INSTALL_INFO_PUSHED = "install_info_pushed";

    //GCM
    public static final String GCM_REGISTRATION_ID = "gcm_registration_id";

    public static void saveInstallInfo(Context context, String medium, String campaign, String
            source) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(MEDIUM, medium).putString(CAMPAIGN, campaign).putString(SOURCE, source)
                .putBoolean(INSTALL_INFO_SAVED, true).apply();
    }

    public static boolean isInstallInfoSaved(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean
                (INSTALL_INFO_SAVED, false);
    }

    public static boolean isInstallInfoPushed(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean
                (INSTALL_INFO_PUSHED, false);
    }

    public static void saveInstallInfoPushed(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean
                (INSTALL_INFO_PUSHED, true).apply();
    }

    public static void saveProfileInfo(Context context, String mobile, String operatorId, String circleId, String mail, boolean isPrepaid, String dataType) {
        String connection = isPrepaid ? PREPAID : POSTPAID;
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(MOBILE_NUMBER, mobile).putString(OPERATOR, operatorId).putString(CIRCLE, circleId).putString(MAIL, mail).putString(PREPAID_POSTPAID, connection).putString(NETWORK_TYPE, dataType);
        // Saving boolean as a string due to bad coding practice in legacy code :(
        editor.putString(REGISTRATION_COMPLETE, "true").apply();
    }

    public static void saveKey(Context context, String key) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(KEY, key).commit();
    }

    public static void setGcmRegistrationId(Context context, String registration){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(GCM_REGISTRATION_ID, registration);
        editor.apply();
    }

    public static String getGcmRegistrationId(Context context){

        if (context == null)
            return NA;

        SharedPreferences editor = PreferenceManager.getDefaultSharedPreferences(context);
        return editor.getString(GCM_REGISTRATION_ID, NA);
    }
}
