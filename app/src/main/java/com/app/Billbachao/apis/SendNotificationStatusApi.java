package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.BuildConfig;
import com.app.Billbachao.notifications.gcm.model.NotificationModel;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by nitesh on 02-05-2016.
 */
public class SendNotificationStatusApi extends ApiUtils {

    public static final String URL = BASE_URL_GCM + "gcmNotificationOpenServlet";

    public static Map<String, String> getParams(Context context, NotificationModel model) {
        Map<String, String> map = new LinkedHashMap<>();

        map.put(GCM_ID, PreferenceUtils.getGcmRegistrationId(context));
        map.put(GCM_NOT_ID, model.gcm_not_id);
        map.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        return getParamsWithCheckSum(context, map);
    }
}
