package com.app.Billbachao.apis.deals;

import android.content.Context;
import android.text.TextUtils;

import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.deals.utils.DealsUtils;
import com.app.Billbachao.deals.models.DealsPaymentOptions;
import com.app.Billbachao.deals.models.GenericTvModel;
import com.app.Billbachao.deals.models.RelianceTvDetailsModel;
import com.app.Billbachao.deals.models.RelianceTvModel;
import com.app.Billbachao.deals.models.TvProductInformation;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.LocationUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.google.myjson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Reliance Deals Ai
 * Created by nitesh on 24/02/2016.
 */
public class DealsListApiUtils extends ApiUtils {

    public static final String PRODUCT_DEALS = BASE_URL + "GetRelianceTelevisionDetails";
    public static final String MODE_ALL = "ALL";
    public static final String MODE_BY_PRODUCT = "BYPRODUCT";
    public static final String PRODUCTID = "productId";

    public static Map<String, String> getDealsForTvModeAll(Context context, String mode, String productId) {

        HashMap<String, String> params = new LinkedHashMap<>();
        params.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        params.put(REQUEST_FROM, APP);
        params.put(REQUEST_DATE, getDate());
        params.put(EMAIL, PreferenceUtils.getMailId(context));

        addLocationInfo(context, params);

        params.put(MODE, mode);
        params.put(PRODUCTID, productId);
        return getParamsWithCheckSum(context, params);
    }

    /**
     * Add latitude, longitude and city if found.
     *
     * @param mContext
     * @param params
     */
    private static void addLocationInfo(Context mContext, HashMap<String, String> params) {

        LocationUtils.CustomLocation customLocation = LocationUtils.getLocation(mContext);

        if (customLocation != null) {
            float latitude = customLocation.getLatitude();
            float longitude = customLocation.getLongitude();

            String address = LocationUtils.getAddressFromLatLng(mContext, customLocation.getLatitude(), customLocation.getLongitude(), Locale.getDefault());

            if (TextUtils.isEmpty(address))
                address = ApiUtils.NA;

            params.put(LATITUDE, String.valueOf(latitude));
            params.put(LONGITUDE, String.valueOf(longitude));
            params.put(CITY, address);

        } else {
            params.put(LATITUDE, String.valueOf(0.0f));
            params.put(LONGITUDE, String.valueOf(0.0f));
            params.put(CITY, NA);
        }
    }

    /**
     * Replace all _with " "
     *
     * @param productName
     * @return
     */
    public static String checkFromUnderScore(String productName) {
        return productName.replaceAll("_", " ");
    }


    /**
     * Parse the JSON Response of the deals
     * @param responseJSON
     * @return
     */
    public static List<RelianceTvDetailsModel> getParseJsonTvDetails(JSONObject responseJSON) {

        if (ApiUtils.isValidResponse(responseJSON.toString())) {

            JSONArray tvDetails = responseJSON.optJSONArray(RelianceTvModel.TV_DETAILS);

            if (tvDetails != null && tvDetails.length() > 0) {

                int len = tvDetails.length();

                List<RelianceTvDetailsModel> relianceTvModelList = new ArrayList<>();

                for (int m = 0; m < len; m++) {

                    JSONObject itemObject = tvDetails.optJSONObject(m);

                    if (itemObject != null) {

                        //check all the keys
                        String key = itemObject.optString("key");

                        ILog.d(TAG, "|key|" + key);

                        //check is its not empty
                        if (!TextUtils.isEmpty(key)) {

                            //spilt the keys
                            List<String> items = DealsUtils.getListByComma(key);

                            if (items != null && items.size() > 0) {

                                RelianceTvDetailsModel relianceTvDetailsModel = new RelianceTvDetailsModel();

                                List<String> titleList = new ArrayList<>();
                                HashMap<String, ArrayList<GenericTvModel>> listHashMap = new HashMap<>();

                                for (String itemsDetails : items) {

                                    ILog.d(TAG, "|itemKey|" + itemsDetails);

                                    if (!TextUtils.isEmpty(itemsDetails)) {

                                        //get object from the keys
                                        JSONObject productObject = itemObject.optJSONObject(itemsDetails);

                                        if (productObject != null) {

                                            //DONOT add the title and its content if its payment options
                                            if (!itemsDetails.equalsIgnoreCase(RelianceTvDetailsModel.SPEC_PAYEMNT_OPTIONS)) {

                                                //add the content always for product info.
                                                if (itemsDetails.equalsIgnoreCase(RelianceTvDetailsModel.SPEC_PRODUCT_INFO)) {
                                                    relianceTvDetailsModel.productInformation = new Gson().fromJson(productObject.toString(), TvProductInformation.class);
                                                }


                                                String keyDetails = productObject.optString("key");
                                                List<String> itemsDetailsKeyList = DealsUtils.getListByComma(keyDetails);

                                                ArrayList<GenericTvModel> genericTvModelList = new ArrayList<>();
                                                for (String itemDetailsKey : itemsDetailsKeyList) {
                                                    genericTvModelList.add(new GenericTvModel(itemsDetails, itemDetailsKey, productObject.optString(itemDetailsKey)));
                                                }

                                                titleList.add(itemsDetails);
                                                listHashMap.put(itemsDetails, genericTvModelList);
                                            } else {
                                                relianceTvDetailsModel.dealPaymentOptions = new Gson().fromJson(productObject.toString(), DealsPaymentOptions.class);
                                            }
                                        }

                                        relianceTvDetailsModel.titleList = titleList;
                                        relianceTvDetailsModel.genericTvModelHashMap = listHashMap;
                                    }

                                }
                                relianceTvModelList.add(relianceTvDetailsModel);
                            }
                        }
                    }
                }
                return relianceTvModelList;
            }
        }

        return null;
    }

}
