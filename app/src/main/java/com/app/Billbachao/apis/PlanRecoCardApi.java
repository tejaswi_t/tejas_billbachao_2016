package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.utils.AppUtils;
import com.app.Billbachao.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mihir on 26-04-2016.
 */
public class PlanRecoCardApi extends BestPlanApi {

    public static final String URL = BASE_URL + "MyBestPlanRecoServlet", AGE_DAYS = "ageDays";

    public static Map<String, String> getParams(Context context, String mode) {
        Map<String, String> map = new LinkedHashMap<>();

        map.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        map.put(USER_USAGE, getUserUsageWithDays(context).toString());
        map.put(MODE, mode);
        map.put(REQUEST_DATE, getDate());
        return getParamsWithCheckSum(context, map);
    }

    public static JSONObject getUserUsageWithDays(Context context) {
        JSONObject jsonObject = getUserUsage(context);
        try {
            // +1 as index starts from 1 on backend
            return jsonObject.put(AGE_DAYS, AppUtils.getDaysSinceInstalled(context) + 1);
        } catch (JSONException e) {
            e.printStackTrace();
            return jsonObject;
        }
    }
}
