package com.app.Billbachao.apis;

import android.content.Context;
import android.text.TextUtils;

import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mihir on 26-04-2016.
 */
public class NetworkCardApi extends ApiUtils {

    public static final String URL = BASE_URL + "MyBestNetwork";

    static final String LATITUDE = "latitude", LONGITUDE = "longitude", MODE = "mode";

    static final String HOME = "home", WORK = "work", CURRENT = "current";

    public static final int MODE_HOME = 1, MODE_WORK = 2, MODE_CURRENT = 3;

    public static Map<String, String> getParams(Context context, int[] modeArray) {
        return getParams(context, modeArray, 0, 0);
    }

    public static Map<String, String> getParams(Context context, double latitude, double
            longitude) {
        return getParams(context, new int[]{NetworkCardApi.MODE_CURRENT, NetworkCardApi.MODE_HOME,
                NetworkCardApi.MODE_WORK}, latitude, longitude);
    }

    public static Map<String, String> getParams(Context context, int[] modeArray, double
            latitude, double longitude) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        map.put(CIRCLE_ID, PreferenceUtils.getCircleId(context));
        map.put(OPERATOR_ID, PreferenceUtils.getOperatorId(context));
        map.put(MODE, getModeString(modeArray));
        map.put(LATITUDE, String.valueOf(latitude));
        map.put(LONGITUDE, String.valueOf(longitude));
        map.put(REQUEST_TYPE, APP);
        map.put(REQUEST_DATE, getDate());
        return getParamsWithCheckSum(context, map);
    }

    private static String getModeString(int[] modeArray) {
        String modeString[] = new String[modeArray.length];
        int index = 0;
        for (int mode : modeArray) {
            modeString[index++] = getMode(mode);
        }
        return TextUtils.join(", ", modeString);
    }

    private static String getMode(int mode) {
        switch (mode) {
            case MODE_HOME:
                return HOME;
            case MODE_WORK:
                return WORK;
            case MODE_CURRENT:
            default:
                return CURRENT;
        }
    }
}
