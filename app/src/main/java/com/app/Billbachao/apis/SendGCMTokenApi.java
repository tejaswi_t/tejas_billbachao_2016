package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Send GCMTokenApi to upate GcmId.
 * Created by nitesh on 02-05-2016.
 */
public class SendGCMTokenApi extends ApiUtils {

    public static final String URL = BASE_URL + "UpdateGCMRegistrationID";

    public static Map<String, String> getParams(Context context, String token) {
        Map<String, String> map = new LinkedHashMap<>();

        map.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        map.put(GCM_ID, token);
        map.put(MOBILE_VERIFIED, PreferenceUtils.isMobVerified(context) ? Y : N);
        map.put(REQUEST_FROM, APP);
        map.put(REQUEST_DATE, getDate());

        return getParamsWithCheckSum(context, map);
    }
}
