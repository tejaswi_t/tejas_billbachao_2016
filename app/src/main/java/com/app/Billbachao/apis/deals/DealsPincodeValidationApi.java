package com.app.Billbachao.apis.deals;

import android.content.Context;

import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.utils.PreferenceUtils;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nitesh on 24/02/2016.
 */
public class DealsPincodeValidationApi extends ApiUtils {

    public static final String PINCODE_URL = BASE_URL + "GetDetailsByPincode";
    private static final String GET_BY_PINCODE = "GET";
    /**
     * get deals address by pincode
     * @param context
     * @param pincode
     * @return
     */

    //(mobile| reqFrom|mode|pincode| requestDate| key)
    public static Map<String, String> getDealsByPincode(Context context, String pincode) {
        HashMap<String, String> params = new LinkedHashMap<>();

        params.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        params.put(REQUEST_FROM, APP);
        params.put(MODE , GET_BY_PINCODE);
        params.put(PIN_CODE, pincode);
        params.put(REQUEST_DATE, getDate());
        return getParamsWithCheckSum(context,params);
    }

}
