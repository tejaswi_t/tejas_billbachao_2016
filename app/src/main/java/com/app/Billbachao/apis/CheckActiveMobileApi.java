package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.BuildConfig;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by BB-001 on 5/3/2016.
 */
public class CheckActiveMobileApi extends ApiUtils {

    public static final String URL = BASE_URL + "CheckActiveMobileUserServlet";

    static final String VERSION_NO = "versionNo", OLD_MOB_NO = "oldMobNo";

    public static Map<String, String> getParams(Context context, String mobileNumber) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE_NUMBER, mobileNumber);
        map.put(REQUEST_DATE, getDate());
        map.put(VERSION_NO, BuildConfig.VERSION_NAME);
        map.put(OLD_MOB_NO, PreferenceUtils.getMobileNumber(context));
        return getParamsWithCheckSum(context, map);
    }
}
