package com.app.Billbachao.apis;

import android.content.Context;

import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mihir on 04-12-2015.
 */
public class FeedbackApi extends ApiUtils {

    public static final String URL = ApiUtils.BASE_URL + "SaveUserFeedbackServlet";

    static final String FEEDBACK = "feedbackText", RATING = "rating";

    public static Map<String, String> getParams(Context context, int rating, String feedback) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        map.put(FEEDBACK, feedback);
        map.put(RATING, String.valueOf(rating));
        map.put(REQUEST_DATE, getDate());

        return getParamsWithCheckSum(context, map);
    }
}
