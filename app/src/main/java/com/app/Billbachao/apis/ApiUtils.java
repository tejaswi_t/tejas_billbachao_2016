package com.app.Billbachao.apis;

import android.content.Context;
import android.support.annotation.NonNull;

import com.app.Billbachao.BuildConfig;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by mihir on 11-03-2016.
 */
public class ApiUtils {

    protected static final String TAG = ApiUtils.class.getSimpleName();

    protected static final String MOBILE_NUMBER = "mobNo", CHECKSUM = "checkSum", CIRCLE_NAME =
            "cName", TYPE = "cType", REQUEST_FROM = "reqFrom", OPERATOR_NAME = "opName", AMOUNT =
            "amt", REQ_DATE = "reqDate", MSG = "msg", REQUEST_DATE = "requestDate", REQUEST_TYPE
            = "reqType", MOBILE = "mobile", CIRCLE_ID = "cId", OPERATOR_ID = "opId", UEMAIL = "uEmail", EMAIL = "email",
            USSD_DETAILS = "ussdDetails", LANGUAGE = "language", NETWORK_TYPE = "dType", GCM_ID =
            "gcmid", GCM_NOT_ID = "notId", MOBILE_VERIFIED = "mobileVerified";

    //deals
    protected static final String LATITUDE = "latitude", LONGITUDE = "longitude", CITY = "city", MODE = "mode",
            PIN_CODE = "pincode", ADDRESS = "address", STATE = "state", LANDMARK = "landmark", FIRST_NAME = "firstname",
            LAST_NAME = "lastname", ADDRESS_TYPE = "addressType";

    public static final String INSERT = "INSERT", DELETE = "DELETE", UPDATE = "UPDATE", ERROR = "Error";;

    protected static final String APP = "APP", PREPAID = "PREPAID", POSTPAID = "POSTPAID", PIPE = "|", MOB = "MOB";

    public static final String NA = "NA";

    // Result tags
    public static final String STATUS = "status", SUCCESS = "success", STATUS_DESC = "statusDesc", Y = "Y", N = "N", FAILURE = "failure";

    protected static final String BASE_DEBUG_URL = "http://devfj.billbachao.com/CroptRecoEngine/";

    protected static final String BASE_RELEASE_URL = "https://fj.billbachao.com/CroptRecoEngine/";

    protected static final String BASE_URL = BuildConfig.DEBUG ? BASE_DEBUG_URL : BASE_RELEASE_URL;

    //GCM Different Project.
    protected static final String BASE_DEBUG_URL_GCM = "http://devfj.billbachao.com/BBGCMNotificationScheduler/";

    protected static final String BASE_RELEASE_URL_GCM = "https://fj.billbachao.com/BBGCMNotificationScheduler/";

    protected static final String BASE_URL_GCM = BuildConfig.DEBUG ? BASE_DEBUG_URL_GCM : BASE_RELEASE_URL_GCM;


    /**
     * Call this api without passing checksum, this will calculate checksum and add it to params
     *
     * @param context
     * @param map
     * @return
     */
    protected static Map<String, String> getParamsWithCheckSum(Context context, @NonNull
    Map<String, String> map) {
        String checkSum = ApiUtils.getCheckSum(context, map);
        map.put(CHECKSUM, checkSum);
        return map;
    }

    public static String getCheckSum(Context context, @NonNull Map<String, String> params) {
        ILog.d(TAG, "Key : " + PreferenceUtils.getKey(context) + " found");
        String md5String = generateMD5String(params, PreferenceUtils.getKey(context));
        ILog.d(TAG, "MD5String : " + md5String);
        return generateCheckSum(md5String);
    }

    protected static String generateMD5String(Map<String, String> params, String key) {
        StringBuilder builder = new StringBuilder();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            String value = entry.getValue();
            if (value == null || value.equals("")) {
                value = NA;
            }
            builder.append(value + PIPE);
        }
        builder.append(key);

        return builder.toString();
    }

    protected static String generateCheckSum(String string) {
        StringBuilder sb = new StringBuilder();
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(string.getBytes());
            byte[] digest = md.digest();

            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String checkSum = sb.toString().toUpperCase();
        ILog.d(TAG, "Checksum " + checkSum);
        return checkSum;
    }

    protected static String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHH24mm");
        return formatter.format(new Date());
    }

    public static boolean isValidResponse(String response) {
        ILog.d(TAG, "Response " + response);

        try {
            JSONObject jsonObject = new JSONObject(response);
            return jsonObject.getString(STATUS).equalsIgnoreCase(SUCCESS);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }
}
