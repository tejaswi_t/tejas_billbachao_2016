package com.app.Billbachao.apis;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.app.Billbachao.usage.helper.UsageHelper;
import com.app.Billbachao.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by mihir on 20-04-2016.
 */
public class BestPlanApi extends ApiUtils {

    public static final String URL = BASE_URL + "MyBestPlanServlet", MODE = "mode", USER_USAGE = "userUsage", SCHEDULER = "Scheduler", MYBESTPLAN = "MyBestPlan";

    public static Map<String, String> getParams(Context context, String mode) {
        Map<String, String> map = new LinkedHashMap<>();

        map.put(MOBILE, PreferenceUtils.getMobileNumber(context));
        map.put(USER_USAGE, getUserUsage(context).toString());
        map.put(MODE, mode);
        map.put(REQUEST_DATE, getDate());
        return getParamsWithCheckSum(context, map);
    }

    public static JSONObject getUserUsage(Context context) {
        JSONObject usageObj = new JSONObject();
        try {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            long estimatedDataUsage = UsageHelper.getEstimatedDataUsageMB(context);
            long userDataUsage = preferences.getLong(PreferenceUtils.DATA_USAGE, estimatedDataUsage);

            usageObj.put("opName", PreferenceUtils.getOperatorId(context));
            usageObj.put("cName", PreferenceUtils.getCircleId(context));
            usageObj.put("cType", PreferenceUtils.getCType(context));
            usageObj.put("dType", PreferenceUtils.getNetworkType(context));
            usageObj.put("lastAmt", "0");
            usageObj.put("intCallMin", UsageHelper.getISDOutGoingCall());
            usageObj.put("intSMSCnt", UsageHelper.getISDOutSms());
            usageObj.put("dInMB", String.valueOf(userDataUsage));
            usageObj.put("loOnNet", UsageHelper.getLocalOnnetCall());
            usageObj.put("loOffNet", UsageHelper.getLocalOffnetCall());
            usageObj.put("lolline", UsageHelper.getLocalLandline());
            usageObj.put("sOnNet", UsageHelper.getNationalOnnetCall());
            usageObj.put("sOffNet", UsageHelper.getNationalOffnetCall());
            usageObj.put("slline", UsageHelper.getNationalLandline());
            usageObj.put("loSMS", UsageHelper.getLocalOutSms());
            usageObj.put("natSMS", UsageHelper.getNationalOutSms());
            usageObj.put("otherCall", UsageHelper.getOtherOutGoingCall());
            usageObj.put("otherSMS", UsageHelper.getOtherSms());
            usageObj.put("nightCallMin", UsageHelper.getNightCall());
            usageObj.put("nightSMSCnt", UsageHelper.getNightSms());

            usageObj.put("uDataInMB", userDataUsage);  //User Edited Data In MB
            usageObj.put("bbDataInMB", estimatedDataUsage);  //BB Estimated Usage
            usageObj.put("wifiTraffic", UsageHelper.getWifiTrafficStats());
            usageObj.put("mobileTraffic", UsageHelper.getMobileDataTrafficStats());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return usageObj;
    }
}
