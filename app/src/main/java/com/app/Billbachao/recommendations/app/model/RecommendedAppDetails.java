package com.app.Billbachao.recommendations.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.Billbachao.utils.DateUtils;
import com.google.myjson.annotations.SerializedName;

/**
 * Created by mihir on 16-12-2015.
 */
public class RecommendedAppDetails implements Parcelable {

    @SerializedName("appName")
    private String mTitle;

    @SerializedName("googlePlayStoreURL")
    private String mAppUrl;

    @SerializedName("smallImage")
    private String mImageUrl;

    @SerializedName("feature")
    private String mDetails;

    @SerializedName("appRating")
    private float mRating;

    @SerializedName("appDate")
    private String mDate;

    protected RecommendedAppDetails(Parcel in) {
        mTitle = in.readString();
        mAppUrl = in.readString();
        mImageUrl = in.readString();
        mDetails = in.readString();
        mRating = in.readFloat();
        mDate = in.readString();
    }

    public RecommendedAppDetails(String title, String appUrl, String imageUrl, String details, float rating, String date) {
        mTitle = title;
        mAppUrl = appUrl;
        mImageUrl = imageUrl;
        mDetails = details;
        mRating = rating;
        mDate = date;
    }

    public static final Creator<RecommendedAppDetails> CREATOR = new
            Creator<RecommendedAppDetails>() {
                @Override
                public RecommendedAppDetails createFromParcel(Parcel in) {
                    return new RecommendedAppDetails(in);
                }

                @Override
                public RecommendedAppDetails[] newArray(int size) {
                    return new RecommendedAppDetails[size];
                }
            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mAppUrl);
        dest.writeString(mImageUrl);
        dest.writeString(mDetails);
        dest.writeFloat(mRating);
        dest.writeString(mDate);
    }

    public float getRating() {
        return mRating;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getAppUrl() {
        return mAppUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getDetails() {
        return mDetails;
    }

    public long getDateMillis() {
        return DateUtils.parseDateFormat(mDate, DateUtils.FORMAT_YYYY_MM_DD).getTime();
    }
}
