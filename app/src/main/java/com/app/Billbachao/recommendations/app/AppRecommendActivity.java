package com.app.Billbachao.recommendations.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.AppsRecommendationApi;
import com.app.Billbachao.externalsdk.AppsFlyerUtils;
import com.app.Billbachao.model.AppRecommendationGsonResponse;
import com.app.Billbachao.recommendations.app.model.RecommendedAppDetails;
import com.app.Billbachao.utils.DateUtils;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by mihir on 16-12-2015.
 */
public class AppRecommendActivity extends BaseActivity {

    public static final String TAG = AppRecommendActivity.class.getSimpleName();

    ViewPager mPager;

    PagerTabStrip mPagerStrip;

    ArrayList<RecommendedAppDetails> mAppsList;

    boolean[] mEventTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apps_recommend);
        initView();
        showProgressDialog(getString(R.string.fetching_apps_recommendation));
        fetchAppData();
    }

    void initView() {
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerStrip = (PagerTabStrip) findViewById(R.id.pager_title_strip);
        mPagerStrip.setTabIndicatorColorResource(R.color.colorAccent);
    }

    void fetchAppData() {
        String appsString = AppsRecommendationApi.getSavedAppString(this);

        if (!appsString.isEmpty()) {
            onAppDataReceived(AppsRecommendationApi.parseAppsJsonArray(appsString));
            return;
        }

        NetworkGsonRequest<AppRecommendationGsonResponse> request = new NetworkGsonRequest<AppRecommendationGsonResponse>(Request.Method.POST, AppsRecommendationApi.URL, AppRecommendationGsonResponse.class, AppsRecommendationApi.getParams(AppRecommendActivity.this), new Response.Listener<AppRecommendationGsonResponse>() {
            @Override
            public void onResponse(AppRecommendationGsonResponse response) {
                onAppDataReceived(response.appList);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dismissProgressDialog();
                showSnack(NetworkErrorHelper.getErrorStatus(volleyError).getErrorMessage());
                finish();
            }
        });
        VolleySingleTon.getInstance(this).addToRequestQueue(request, TAG);
    }

    public void onAppDataReceived(ArrayList<RecommendedAppDetails> appsList) {
        dismissProgressDialog();
        if (appsList.isEmpty()) {
            Toast.makeText(AppRecommendActivity.this, R.string.no_apps_found, Toast.LENGTH_SHORT)
                    .show();
            finish();
            return;
        }
        AppsFlyerUtils.trackEvent(AppRecommendActivity.this, AppsFlyerUtils.APP_OF_THE_DAY);
        Collections.sort(appsList, new Comparator<RecommendedAppDetails>() {
            @Override
            public int compare(RecommendedAppDetails lhs, RecommendedAppDetails rhs) {
                return Double.compare(lhs.getDateMillis(), rhs.getDateMillis());
            }
        });
        mAppsList = appsList;
        mPager.setAdapter(new CustomPagerAdapter(getSupportFragmentManager()));
        mPager.setCurrentItem(mAppsList.size() - 1);
        mPagerStrip.setVisibility(View.VISIBLE);
        mEventTracker = new boolean[mAppsList.size()];
        mPager.addOnPageChangeListener(mPageChangeListener);
    }

    /*void showProgressDialog() {
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    void dismissProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }*/

    class CustomPagerAdapter extends FragmentPagerAdapter {

        public CustomPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return AppRecommendedFragment.getInstance(mAppsList.get(position));
        }

        @Override
        public int getCount() {
            return mAppsList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return DateUtils.convertTimeToDate(mAppsList.get(position).getDateMillis(), DateUtils
                    .FORMAT_DD_MMM);
        }
    }

    ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            // Avoid repeated events
            if ((position == mAppsList.size() - 1) || mEventTracker[position]) {
                return;
            }
            int index = mAppsList.size() - 1 - position;
            mEventTracker[position] = true;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    protected void onDestroy() {
        VolleySingleTon.getInstance(this).cancelPendingRequests(TAG);
        super.onDestroy();
    }
}
