package com.app.Billbachao.recommendations.network.model;

import com.app.Billbachao.model.BaseGsonResponse;
import com.google.myjson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mihir on 26-04-2016.
 */
public class NetworkRecoCardGsonResponse extends BaseGsonResponse {

    @SerializedName("HOME")
    ArrayList<NetworkOperatorInfo> homeNetworkData;

    @SerializedName("WORK")
    ArrayList<NetworkOperatorInfo> workNetworkData;

    @SerializedName("CURRENT")
    ArrayList<NetworkOperatorInfo> currentNetworkData;

    @SerializedName("workLatitude")
    float workLatitude;

    @SerializedName("workLongitude")
    float workLongitude;

    @SerializedName("homeLatitude")
    float homeLatitude;

    @SerializedName("homeLongitude")
    float homeLongitude;

    public ArrayList<NetworkOperatorInfo> getHomeNetworkData() {
        return homeNetworkData;
    }

    public ArrayList<NetworkOperatorInfo> getWorkNetworkData() {
        return workNetworkData;
    }

    public ArrayList<NetworkOperatorInfo> getCurrentNetworkData() {
        return currentNetworkData;
    }

    public NetworkOperatorInfo getCurrentHomeNetwork() {
        return findCurrentOperatorData(homeNetworkData);
    }

    public NetworkOperatorInfo getCurrentWorkNetwork() {
        return findCurrentOperatorData(workNetworkData);
    }

    private static NetworkOperatorInfo findCurrentOperatorData(ArrayList<NetworkOperatorInfo>
                                                                       networkData) {
        if (networkData == null) return null;
        for (NetworkOperatorInfo info : networkData) {
            if (info.isSameNetwork()) return info;
        }
        return null;
    }

    @Override
    public boolean isSuccess() {
        return super.isSuccess() && ((currentNetworkData != null && !currentNetworkData.isEmpty())
                || (homeNetworkData != null && !homeNetworkData.isEmpty()) || (workNetworkData != null &&
                !workNetworkData.isEmpty()));
    }
}
