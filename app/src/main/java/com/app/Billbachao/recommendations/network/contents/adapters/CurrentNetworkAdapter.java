package com.app.Billbachao.recommendations.network.contents.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.recommendations.network.model.NetworkOperatorInfo;

import java.util.ArrayList;

/**
 * Created by mihir on 15-12-2015.
 */
public class CurrentNetworkAdapter extends BestNetworkAdapter {
    public CurrentNetworkAdapter(Context context, ArrayList<NetworkOperatorInfo> networkObjects,
                                 CallBack callBack) {
        super(context, networkObjects, callBack);
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        convertView = super.getGroupView(groupPosition, isExpanded, convertView, parent);
        GroupViewHolder holder = (GroupViewHolder) convertView.getTag();
        NetworkOperatorInfo networkObject = mNetworkObjects.get(groupPosition);

        if (groupPosition == 0 && !networkObject.isSameNetwork()) {
            holder.requestMnp.setVisibility(View.VISIBLE);
            holder.requestMnp.setOnClickListener(mClickListener);
        } else {
            holder.requestMnp.setVisibility(View.GONE);
        }
        if (networkObject.isSameNetwork()) {
            holder.rateOperator.setVisibility(View.VISIBLE);
            holder.rateOperator.setOnClickListener(mClickListener);
            holder.testSpeed.setVisibility(View.VISIBLE);
            holder.testSpeed.setOnClickListener(mClickListener);
        } else {
            holder.rateOperator.setVisibility(View.GONE);
            holder.testSpeed.setVisibility(View.INVISIBLE);
        }

        // TODO
        String savingsPercent = "";
        if (!(savingsPercent.isEmpty() || savingsPercent.equals("0") || savingsPercent.equals("0%"))) {
            holder.savingsPercent.setText(mContext.getString(R.string.save_percent,
                    savingsPercent));
        } else {
            holder.savingsPercent.setText("");
        }

        holder.dropDown.setVisibility(View.VISIBLE);
        holder.dropDown.setImageResource(isExpanded ? R.drawable.ic_expand_less: R.drawable.ic_expand_more);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View
            convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.network_child_view, null);
            ChildViewHolder holder = new ChildViewHolder(convertView);
            convertView.setTag(holder);
        }
        ChildViewHolder holder = (ChildViewHolder) convertView.getTag();
        NetworkOperatorInfo networkObject = mNetworkObjects.get(groupPosition);

        for (int index = 0; index < RATINGS_COUNT; index++) {
            ChildViewHolder.RatingHolder ratingHolder = holder.ratingHolders[index];
            int ratingTag = NetworkOperatorInfo.RATING_TAGS[index];
            ratingHolder.ratingText.setText(NetworkOperatorInfo.getTitleResId(ratingTag));
            ratingHolder.ratingBar.setProgress(networkObject.getRatingValue(ratingTag));
        }

        return convertView;
    }

    class ChildViewHolder {

        RatingHolder[] ratingHolders = new RatingHolder[RATINGS_COUNT];
        int ratingResIds[] = new int[]{R.id.rating1, R.id.rating2, R.id.rating3, R.id.rating4, R.id.rating5};

        public ChildViewHolder(View view) {
            for (int index = 0; index < RATINGS_COUNT; index++) {
                RatingHolder holder = new RatingHolder();
                View parentView = view.findViewById(ratingResIds[index]);
                parentView.setVisibility(View.VISIBLE);
                holder.ratingText = (TextView) parentView.findViewById(R.id.rating_title);
                holder.ratingBar = (RatingBar) parentView.findViewById(R.id.rating_bar);
                ratingHolders[index] = holder;
            }
        }

        class RatingHolder {
            TextView ratingText;
            RatingBar ratingBar;
        }

    }

}
