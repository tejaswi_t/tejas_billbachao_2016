package com.app.Billbachao.recommendations.network.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by mihir on 09-05-2016.
 */
public class NetworkLocationFragmentInfo implements Parcelable {

    ArrayList<NetworkOperatorInfo> locationObjectList;

    int type;

    String address;

    String title;

    double mLatitude;

    double mLongitude;

    public NetworkLocationFragmentInfo(ArrayList<NetworkOperatorInfo> locationObjectList, int
            type, String title, double latitude, double longitude) {
        this.locationObjectList = locationObjectList;
        this.type = type;
        this.title = title;
        mLatitude = latitude;
        mLongitude = longitude;
    }

    protected NetworkLocationFragmentInfo(Parcel in) {
        type = in.readInt();
        address = in.readString();
        title = in.readString();
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(type);
        dest.writeString(address);
        dest.writeString(title);
        dest.writeDouble(mLatitude);
        dest.writeDouble(mLongitude);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NetworkLocationFragmentInfo> CREATOR = new Creator<NetworkLocationFragmentInfo>() {
        @Override
        public NetworkLocationFragmentInfo createFromParcel(Parcel in) {
            return new NetworkLocationFragmentInfo(in);
        }

        @Override
        public NetworkLocationFragmentInfo[] newArray(int size) {
            return new NetworkLocationFragmentInfo[size];
        }
    };

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTitle() {
        return title;
    }

    public int getType() {
        return type;
    }

    public ArrayList<NetworkOperatorInfo> getNetworkList() {
        return locationObjectList;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public String getAddress() {
        return address;
    }
}
