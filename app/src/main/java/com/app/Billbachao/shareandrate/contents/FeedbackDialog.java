package com.app.Billbachao.shareandrate.contents;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

import com.app.Billbachao.R;

/**
 * Created by mihir on 02-12-2015.
 */
public class FeedbackDialog extends DialogFragment {

    public static final String TAG = FeedbackDialog.class.getSimpleName(), RATING = "rating";

    OnFeedbackListener mListener;

    int mRating;

    public static FeedbackDialog getInstance(int ratingValue) {
        FeedbackDialog dialog = new FeedbackDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(RATING, ratingValue);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AnimatedDialogTheme_EnterLeftExitLeft);
        builder.setTitle(R.string.feedback);
        View view = View.inflate(getActivity(), R.layout.dialog_feedback, null);
        final EditText feedbackText = (EditText) view.findViewById(R.id.feedback);
        builder.setView(view);

        final int rating = getArguments().getInt(RATING);

        builder.setPositiveButton(R.string.send_feedback, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendFeedback(rating, feedbackText.getText().toString().trim());
            }
        });

        builder.setNeutralButton(R.string.contact_us, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mListener != null) {
                    mListener.onLaunchContactInfo();
                }
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnFeedbackListener)
            mListener = (OnFeedbackListener) activity;
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    void sendFeedback(int rating, String feedbackText) {
        // TODO api for feedback
        if (feedbackText.isEmpty()) {
            return;
        }
        if (mListener != null) {
            mListener.onFeedbackSent(rating, feedbackText);
        }
    }

    public interface OnFeedbackListener {
        void onFeedbackSent(int rating, String feedback);

        void onLaunchContactInfo();
    }
}
