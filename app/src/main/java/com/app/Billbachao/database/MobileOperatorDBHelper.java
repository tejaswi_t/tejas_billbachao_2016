package com.app.Billbachao.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.app.Billbachao.model.OperatorCircleInfo;

/**
 * Created by mihir on 14-08-2015.
 */
public class MobileOperatorDBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "MobileOperatorDB.db";

    public static final int DB_VERSION = 67;

    private SQLiteDatabase myDataBase;

    private static MobileOperatorDBHelper sInstance;

    private static final String TABLE_NAME = "MobileOperatorCircles";
    private static final String MOBILE_SERIES = "mobileSeries";
    private static final String OPERATOR_NAME = "operatorName";
    private static final String OPERATOR_CIRCLE = "operatorCircle";

    public static MobileOperatorDBHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new MobileOperatorDBHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    MobileOperatorDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public void openDataBase() throws SQLException {
        String myPath = DatabaseUtils.DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    public synchronized void close() {
        if (myDataBase != null)
            myDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public OperatorCircleInfo getOperatorCircleInfo(String number) {
        Cursor cursor = myDataBase.query(TABLE_NAME, null, MOBILE_SERIES + "=?", new
                String[]{number.substring(0, 5)}, null, null, null, null);

        int circleId = -1, operatorId = -1;
        String circle = "", operator = "";

        if (cursor.moveToFirst()) {
            circle = cursor.getString(cursor.getColumnIndex(OPERATOR_CIRCLE));
            operator = cursor.getString(cursor.getColumnIndex(OPERATOR_NAME));
        } else {
            cursor.close();
            cursor = myDataBase.query(TABLE_NAME, null, MOBILE_SERIES + "=?", new
                    String[]{number.substring(0, 4)}, null, null, null, null);
            if (cursor.moveToFirst()) {
                circle = cursor.getString(cursor.getColumnIndex(OPERATOR_CIRCLE));
                operator = cursor.getString(cursor.getColumnIndex(OPERATOR_NAME));
            }
        }
        cursor.close();

        if (!circle.isEmpty()) {
            circleId = Integer.parseInt(circle);
        }
        if (!operator.isEmpty()) {
            operatorId = Integer.parseInt(operator);
        }
        return new OperatorCircleInfo(operatorId, circleId);
    }

}