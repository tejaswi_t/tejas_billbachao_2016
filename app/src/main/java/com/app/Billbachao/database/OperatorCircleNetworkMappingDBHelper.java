package com.app.Billbachao.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by sushil on 1/15/2016.
 */
public class OperatorCircleNetworkMappingDBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "OperatorCircleNetworkMapping.db";

    public static final int DB_VERSION = 3;

    private SQLiteDatabase myDataBase;

    private static OperatorCircleNetworkMappingDBHelper sInstance;

    private static final String TABLE_NAME = "OperatorCircleNetworkType";

    public static final String OPERATOR_ID = "operatorId", CIRCLE_ID = "circleId", KEY_COL_NETWORK_TYPE = "networkType";

    public static OperatorCircleNetworkMappingDBHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new OperatorCircleNetworkMappingDBHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    public OperatorCircleNetworkMappingDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public void openDataBase() throws SQLException {
        String myPath = DatabaseUtils.DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    public synchronized void close() {
        if (myDataBase != null)
            myDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public ArrayList<String> getNetworkList(String operatorId, String circleId) {
        ArrayList<String> networkMappingList = new ArrayList<String>();
        Cursor cursor = myDataBase.query(TABLE_NAME, new String[]{KEY_COL_NETWORK_TYPE}, OPERATOR_ID + "=? AND " + CIRCLE_ID + "=?", new
                String[]{operatorId, circleId}, null, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int networkIndex = cursor.getColumnIndex(KEY_COL_NETWORK_TYPE);
                do {
                    networkMappingList.add(cursor.getString(networkIndex));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return networkMappingList;
    }

}

