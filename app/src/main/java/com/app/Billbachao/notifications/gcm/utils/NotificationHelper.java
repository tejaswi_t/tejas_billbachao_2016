package com.app.Billbachao.notifications.gcm.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.SendNotificationStatusApi;
import com.app.Billbachao.model.BaseGsonResponse;
import com.app.Billbachao.notifications.gcm.db.NotificationDataSource;
import com.app.Billbachao.notifications.gcm.loaders.FetchNotificationImages;
import com.app.Billbachao.notifications.gcm.loaders.NotificationUpdateAsynctask;
import com.app.Billbachao.notifications.gcm.model.NotificationModel;
import com.app.Billbachao.notifications.gcm.task.PrepareNotificationModel;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;
import com.clevertap.android.sdk.CleverTapAPI;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


/**
 * GCM Helper to create notification.
 * Created by nitesh on 26-04-2016.
 */
public class NotificationHelper implements FetchNotificationImages.INotifyBitmap {

    private static final String TAG = NotificationHelper.class.getSimpleName();

    private final Context context;

    public NotificationHelper(Context context) {
        this.context = context;
    }

    /**
     * Create pendingIntent to launch the launcher Activity
     *
     * @param context
     * @param bundle
     * @return
     */
    private static PendingIntent createPendingIntent(Context context, NotificationModel model, Bundle bundle) {

        Intent intent;

        if (!TextUtils.isEmpty(model.gcm_deep_link)) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(model.gcm_deep_link));
        } else {
            intent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        }

        bundle.putParcelable(NotificationConstant.KEY_NOTIFICATION_MODEL, model);
        intent.putExtras(bundle);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        return PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    /**
     * Create Notification
     *
     * @param bitmap
     * @param extras
     */
    public void sendBBNotification(Bitmap bitmap, NotificationModel model, Bundle extras) {

        //insert into Db
        String _id = new NotificationDataSource(context).insertDb(model);

        if (_id == null)
            return;

        //add the _id to the model
        model._id = _id;

        NotificationCompat.Style style;

        if (bitmap != null) {
            style = new NotificationCompat.BigPictureStyle()
                    .setSummaryText(model.gcm_description)
                    .bigPicture(bitmap);
        } else {
            style = new NotificationCompat.BigTextStyle()
                    .bigText(model.gcm_description);
        }

        //create pending intent
        PendingIntent pIntent = createPendingIntent(context, model, extras);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(model.gcm_title)
                .setContentText(model.gcm_description)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(context.getApplicationInfo().icon)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .setStyle(style);

        Notification n = builder.build();

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify((int) System.currentTimeMillis(), n);
    }


    /**
     * Handle the bundle and create notification
     *
     * @param extras
     */
    public void createBBNotification(Bundle extras) {

        NotificationModel model = new PrepareNotificationModel(context).getBBNotificationModel(extras);
        if (model == null)
            return;

        if (model.gcm_is_silent.equalsIgnoreCase(NotificationConstant.NOT_SILENT))
            new FetchNotificationImages(model, extras, this).execute(model.gcm_image_url);
        else {
            String _id = new NotificationDataSource(context).insertDb(model);
            ILog.d(TAG, "|silently inserted| " + _id);
        }
    }


    /**
     * Create clevertap notification in database
     *
     * @param extras
     */
    public void createCleverTapNotification(Bundle extras) {

        NotificationModel model = new PrepareNotificationModel(context).getCleverTapNotificationModel(extras);
        if (model != null) {
            new NotificationDataSource(context).insertDb(model);
            CleverTapAPI.createNotification(context, extras);
        }

    }


    /**
     * Send the status of if the notification is read by the user.
     *
     * @param model
     */
    public void sendNotificationResponse(Context context, NotificationModel model) {
        NetworkGsonRequest<BaseGsonResponse> request = new NetworkGsonRequest<>(Request.Method.POST, SendNotificationStatusApi.URL, BaseGsonResponse.class, SendNotificationStatusApi.getParams(context, model), new Response.Listener<BaseGsonResponse>() {

            @Override
            public void onResponse(BaseGsonResponse response) {

                if (response != null) {
                    ILog.d(TAG, "|Notification Status |" + response.status);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ILog.d(TAG, "|Notification Status |" + NetworkErrorHelper.getErrorStatus(error).getErrorMessage());
            }
        }, true);

        VolleySingleTon.getInstance(context).addToRequestQueue(request, NotificationConstant.TAG_GCM_STATUS);

    }

    public void cancelNotificationStatus() {
        VolleySingleTon.getInstance(context).cancelPendingRequests(NotificationConstant.TAG_GCM_STATUS);
    }


    /**
     * get CurrentDate in yyyy-MM-dd format
     *
     * @return
     */
    public static String getCurrentDate() {
        DateFormat df = new SimpleDateFormat(NotificationConstant.NOTIFICATION_DATE);
        String data = df.format(new Date());
        ILog.d(TAG, "|Notification-Date|" + data);
        return data;
    }


    /**
     * get CurrentGMTDate in yyyy-MM-dd HH:mm:ss
     *
     * @return
     */
    public static String getCurrentGMTDate() {

        DateFormat df = new SimpleDateFormat(NotificationConstant.NOTIFICATION_EXPIRY_DATE);
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        String data = df.format(new Date());

        ILog.d(TAG, "|UTC-Date|" + data + "|Current date|" + getCurrentDate());

        return data;
    }


    /**
     * Upate the notification status on server and app.
     *
     * @param notificationModel
     */
    public void updateNotificationStatus(NotificationModel notificationModel) {
        if (notificationModel != null) {
            String is_send_response = notificationModel.gcm_response_callback;
            if (!TextUtils.isEmpty(is_send_response) && is_send_response.equals(NotificationConstant.SEND_RESPONSE_CALLBACK)) {
                sendNotificationResponse(context, notificationModel);
            }

            //update the notification.
            new NotificationUpdateAsynctask(context).execute(notificationModel._id);
        }
    }

    @Override
    public void notifyBitmap(Bitmap bitmap, Bundle extras, NotificationModel model) {
        sendBBNotification(bitmap, model, extras);
    }
}

