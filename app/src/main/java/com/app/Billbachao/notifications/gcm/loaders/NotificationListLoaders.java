package com.app.Billbachao.notifications.gcm.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.app.Billbachao.notifications.gcm.db.NotificationDataSource;
import com.app.Billbachao.notifications.gcm.model.NotificationModel;

import java.util.List;

/**
 * NotificationLoader to fetch the list of notifications.
 * Created by nitesh 27-04-2016.
 */
public class NotificationListLoaders extends AsyncTaskLoader<List<NotificationModel>> {
    public NotificationListLoaders(Context context) {
        super(context);
    }

    @Override
    public List<NotificationModel> loadInBackground() {
        return new NotificationDataSource(getContext()).read();
    }
}
