package com.app.Billbachao.notifications.inapp;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.app.Billbachao.dashboard.DashboardActivity;

/**
 * Simple text Notification
 * Created by mihir.shah on 20-11-2015.
 */
public class SimpleNotification extends BaseNotification {

    protected String mTitle;

    protected String mText;

    public SimpleNotification(Context context, String title) {
        this(context, title, null);
    }

    public SimpleNotification(Context context, String title, String text) {
        super(context);
        mTitle = title;
        mText = text;
    }

    @Override
    protected NotificationCompat.Builder getNotificationBuilder() {

        NotificationCompat.Builder builder = new BaseNotificationBuilder(mContext);
        builder.setContentTitle(mTitle);
        if (mText != null) {
            builder.setContentText(mText).setStyle(new NotificationCompat.BigTextStyle().bigText
                    (mText));
        }

        Intent intent = new Intent(mContext, DashboardActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        return builder;
    }
}
