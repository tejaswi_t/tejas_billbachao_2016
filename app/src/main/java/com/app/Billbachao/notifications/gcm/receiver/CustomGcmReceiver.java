package com.app.Billbachao.notifications.gcm.receiver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.app.Billbachao.notifications.gcm.utils.NotificationConstant;
import com.app.Billbachao.notifications.gcm.utils.NotificationHelper;
import com.app.Billbachao.utils.ILog;

/**
 * Custom Gcm Receiver for notification.
 * Created by nitesh on 26-04-2016.
 */
public class CustomGcmReceiver extends WakefulBroadcastReceiver {

    private static final String TAG = CustomGcmReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle bundle = intent.getExtras();
        if (null == bundle)
            return;

        NotificationHelper gcmHelper = new NotificationHelper(context);

        if (null != bundle.get(NotificationConstant.IS_CLEVERTAP_SOURCE)) {
            gcmHelper.createCleverTapNotification(bundle);
            ILog.d(TAG, "|Gcm Received| - clevertap");
        } else if (null != bundle.get(NotificationConstant.IS_BB_SOURCE) && NotificationConstant.BB_SOURCE.equals(bundle.get(NotificationConstant.IS_BB_SOURCE))) {
            gcmHelper.createBBNotification(bundle);
            ILog.d(TAG, "|Gcm Received| - billbcachao");
        }

    }
}
