package com.app.Billbachao.notifications.gcm.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.app.Billbachao.utils.ILog;

/**
 * ContentProvider for notification
 * Created by nitesh on 27-04-2016.
 */
public class NotificationContentProvider extends ContentProvider {

    private static final String AUTHORIRY = "com.app.Billbachao.provider.inbox";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORIRY);

    private static final String TAG = NotificationContentProvider.class.getSimpleName();

    private SQLiteDatabase mDatabase;

    private Context mContext;

    @Override
    public boolean onCreate() {

        ILog.d(TAG, "|onCreate|");
        mContext = getContext();

        NotificationDbHelper dbHelper = new NotificationDbHelper(mContext);
        mDatabase = dbHelper.getWritableDatabase();
        return mDatabase != null;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Cursor cursor = mDatabase.query(NotificationDbHelper.TABLE_NAME, projection,
                selection, selectionArgs, null, null, sortOrder);

        //Registering the content.
        if (cursor != null) {
            ILog.d(TAG, "|Query|" + cursor.getCount());
            cursor.setNotificationUri(mContext.getContentResolver(), uri);
        }

        return cursor;

    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {

        Uri content_uri = null;

        long rowId = mDatabase.insert(NotificationDbHelper.TABLE_NAME, "", values);

        if (rowId != -1) {
            content_uri = ContentUris.withAppendedId(CONTENT_URI, rowId);
            ILog.d(TAG, "|Insert | " + rowId + " |uri| " + uri.getAuthority());
            mContext.getContentResolver().notifyChange(CONTENT_URI, null);
        }

        return content_uri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        int deletedCount = mDatabase.delete(NotificationDbHelper.TABLE_NAME, selection, selectionArgs);

        if (deletedCount > 0) {
            ILog.d(TAG, "|Deleted |" + deletedCount);
            mContext.getContentResolver().notifyChange(CONTENT_URI, null);
        }
        return deletedCount;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        int updatedCount = mDatabase.update(NotificationDbHelper.TABLE_NAME, values, selection, selectionArgs);

        if (updatedCount > 0) {
            ILog.d(TAG, "|Updated |" + updatedCount);
            mContext.getContentResolver().notifyChange(CONTENT_URI, null);
        }

        return updatedCount;
    }
}
