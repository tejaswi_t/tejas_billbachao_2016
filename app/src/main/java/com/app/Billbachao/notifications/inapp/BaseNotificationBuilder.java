package com.app.Billbachao.notifications.inapp;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.app.Billbachao.R;

/**
 * NotificationCompat.Builder extension for setting common notification properties across the app
 * Created by mihir.shah on 20-11-2015.
 */
public class BaseNotificationBuilder extends NotificationCompat.Builder {

    public BaseNotificationBuilder(Context context) {
        super(context);

        setDefaults(NotificationCompat.DEFAULT_ALL);
        setAutoCancel(true);
        setSmallIcon(R.drawable.ic_small_icon);
        setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher));
    }
}
