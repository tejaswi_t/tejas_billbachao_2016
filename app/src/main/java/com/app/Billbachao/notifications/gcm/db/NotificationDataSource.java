package com.app.Billbachao.notifications.gcm.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.app.Billbachao.notifications.gcm.model.NotificationModel;
import com.app.Billbachao.notifications.gcm.utils.NotificationConstant;
import com.app.Billbachao.notifications.gcm.utils.NotificationHelper;
import com.app.Billbachao.utils.ILog;

import java.util.ArrayList;
import java.util.List;

/**
 * Base source to query notification
 * Created by nitesh on 27-04-2016.
 */
public class NotificationDataSource {

    private static final String TAG = NotificationDataSource.class.getSimpleName();
    private Context mContext;

    public NotificationDataSource(Context context) {
        mContext = context;
    }

    private Uri insert(NotificationModel notificationModel) {
        return mContext.getContentResolver().insert(NotificationContentProvider.CONTENT_URI, NotificationDbHelper.getContentValues(notificationModel));
    }

    public int update(String _id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NotificationModel.COLUMN_NAMES.gcm_is_read, NotificationConstant.READ);

        String selection = NotificationModel.COLUMN_NAMES._id + " =? AND (" + NotificationModel.COLUMN_NAMES.gcm_expiry + " >=? OR " + NotificationModel.COLUMN_NAMES.gcm_expiry + " =? )";
        String selectionArgs[] = new String[]{_id, NotificationHelper.getCurrentGMTDate(), NotificationConstant.BB_DEFAULT_EXPIRY};

        return mContext.getContentResolver().update(NotificationContentProvider.CONTENT_URI, contentValues, selection, selectionArgs);
    }

    public List<NotificationModel> read() {
        String orderBy = NotificationModel.COLUMN_NAMES._id + " DESC ";

        String selection = NotificationModel.COLUMN_NAMES.gcm_is_silent + " =? AND ("+NotificationModel.COLUMN_NAMES.gcm_expiry + " >=? OR " + NotificationModel.COLUMN_NAMES.gcm_expiry + " =? )";
        String selectionArgs[] = new String[]{NotificationConstant.NOT_SILENT, NotificationHelper.getCurrentGMTDate(), NotificationConstant.BB_DEFAULT_EXPIRY};
        Cursor cursor = mContext.getContentResolver().query(NotificationContentProvider.CONTENT_URI, null, selection, selectionArgs, orderBy);

        if (null == cursor)
            return null;

        List<NotificationModel> list = new ArrayList<>();

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

            String title = cursor.getString(cursor.getColumnIndex(NotificationModel.COLUMN_NAMES.gcm_title));

            String gcm_description = cursor.getString(cursor.getColumnIndex(NotificationModel.COLUMN_NAMES.gcm_description));

            String gcm_deep_link = cursor.getString(cursor.getColumnIndex(NotificationModel.COLUMN_NAMES.gcm_deep_link));

            String gcm_image_url = cursor.getString(cursor.getColumnIndex(NotificationModel.COLUMN_NAMES.gcm_image_url));

            String gcm_response_callback = cursor.getString(cursor.getColumnIndex(NotificationModel.COLUMN_NAMES.gcm_response_callback));

            String gcm_expiry = cursor.getString(cursor.getColumnIndex(NotificationModel.COLUMN_NAMES.gcm_expiry));

            String gcm_is_read = cursor.getString(cursor.getColumnIndex(NotificationModel.COLUMN_NAMES.gcm_is_read));

            String gcm_time_ms = cursor.getString(cursor.getColumnIndex(NotificationModel.COLUMN_NAMES.gcm_time_ms));

            String gcm_notId = cursor.getString(cursor.getColumnIndex(NotificationModel.COLUMN_NAMES.gcm_not_id));

            String gcm_category = cursor.getString(cursor.getColumnIndex(NotificationModel.COLUMN_NAMES.gcm_category));

            String gcm_source = cursor.getString(cursor.getColumnIndex(NotificationModel.COLUMN_NAMES.gcm_source));

            String gcm_api_url = cursor.getString(cursor.getColumnIndex(NotificationModel.COLUMN_NAMES.gcm_api_url));

            String gcm_is_silent = cursor.getString(cursor.getColumnIndex(NotificationModel.COLUMN_NAMES.gcm_is_silent));

            NotificationModel model = new NotificationModel(gcm_notId, title, gcm_description, gcm_deep_link, gcm_image_url, gcm_source, gcm_response_callback, gcm_expiry, gcm_is_read, gcm_time_ms, gcm_category, gcm_api_url, gcm_is_silent);
            model._id = "" + cursor.getInt(cursor.getColumnIndex(NotificationModel.COLUMN_NAMES._id));
            list.add(model);
        }

        cursor.close();

        return list;

    }


    public int readUnReadCount() {

        String selection = NotificationModel.COLUMN_NAMES.gcm_is_silent + " =? AND "+ NotificationModel.COLUMN_NAMES.gcm_is_read + " =? AND (" + NotificationModel.COLUMN_NAMES.gcm_expiry + " >=? OR " + NotificationModel.COLUMN_NAMES.gcm_expiry + " =? )";
        String selectionArgs[] = new String[]{NotificationConstant.NOT_SILENT, String.valueOf(NotificationConstant.UNREAD), NotificationHelper.getCurrentGMTDate(), NotificationConstant.BB_DEFAULT_EXPIRY};

        Cursor cursor = mContext.getContentResolver().query(NotificationContentProvider.CONTENT_URI, null, selection, selectionArgs, null);

        if (cursor == null)
            return 0;

        int unreadCount = cursor.getCount();

        ILog.d(TAG, "|unreadCount|" + unreadCount);
        cursor.close();

        return unreadCount;
    }

    /**
     * Insert the notification into database
     *
     * @param model
     * @return _id of the inserted row.
     */
    public String insertDb(NotificationModel model) {
        NotificationDataSource notificationDataSource = new NotificationDataSource(mContext);
        Uri uri = notificationDataSource.insert(model);

        String _id = null;

        if (uri != null) {
            _id = uri.getLastPathSegment();
            ILog.d(TAG, "|inserted Id|" + _id);
        }

        return _id;

    }

    /**
     * Delete the notification by id.
     * @param id
     * @return
     */
    public int delete(String id) {
        String selection = NotificationModel.COLUMN_NAMES._id + " =?";
        String selectionArgs[] = new String[]{id};
        return mContext.getContentResolver().delete(NotificationContentProvider.CONTENT_URI, selection, selectionArgs);
    }
}
