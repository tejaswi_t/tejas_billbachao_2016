/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.app.Billbachao.notifications.gcm.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.SendGCMTokenApi;
import com.app.Billbachao.model.BaseGsonResponse;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;


/**
 * RegistrationIntentService for Registering in GCM.
 * Created by nitesh 01/05/2016
 */
public class RegistrationIntentService extends IntentService {

    private static final String TAG = RegistrationIntentService.class.getSimpleName();

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        try {

            InstanceID instanceID = InstanceID.getInstance(this);
            String registrationId = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);


            if (TextUtils.isEmpty(registrationId))
                return;

            PreferenceUtils.setGcmRegistrationId(this, registrationId);

            ILog.d(TAG, "GCM Registration Token: " + registrationId);

            //send token to server
            sendRegistrationToServer(registrationId);

        } catch (Exception e) {
            ILog.d(TAG, "Failed to complete token refresh" + e.getMessage());
        }
    }

    /**
     * Send gcmId to backend
     * @param token
     */
    private void sendRegistrationToServer(String token) {

        NetworkGsonRequest<BaseGsonResponse> request = new NetworkGsonRequest<>(Request.Method.POST, SendGCMTokenApi.URL, BaseGsonResponse.class, SendGCMTokenApi.getParams(this, token), new Response.Listener<BaseGsonResponse>() {
            @Override
            public void onResponse(BaseGsonResponse response) {
                if (response != null) {
                    ILog.d(TAG, "|Gcm Registration |" + response.status);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ILog.d(TAG, "|Gcm Registration failed|" + NetworkErrorHelper.getErrorStatus(error));
            }
        }, true);

        VolleySingleTon.getInstance(this).addToRequestQueue(request, TAG);
    }

}
