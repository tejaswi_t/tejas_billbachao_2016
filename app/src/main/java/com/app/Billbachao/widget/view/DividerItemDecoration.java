package com.app.Billbachao.widget.view;

import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Divider for Vertical and Horizontal RecyclerView
 * Created by mihir on 07-01-2016.
 */
public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    private int space;

    public DividerItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State
            state) {

        int orientation = getOrientation(parent);

        if (orientation == LinearLayoutManager.VERTICAL) {
            if (parent.getChildAdapterPosition(view) != 0)
                outRect.top = space;
        } else {
            if (parent.getChildAdapterPosition(view) != 0)
                outRect.left = space;
        }

    }

    private int getOrientation(RecyclerView parent) {
        if (parent.getLayoutManager() instanceof LinearLayoutManager) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) parent.getLayoutManager();
            return layoutManager.getOrientation();
        } else {
            throw new IllegalStateException("Dividers are only for LinearLayoutManager");
        }
    }
}
