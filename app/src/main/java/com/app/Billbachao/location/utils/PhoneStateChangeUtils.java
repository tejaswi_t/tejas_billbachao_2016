package com.app.Billbachao.location.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;

/**
 * This class is responsible for providing utilities to phone sate changes
 * <p>
 * Created by rajkumar on 4/25/2016.
 */
public class PhoneStateChangeUtils {

    public static final String CDMA = "CDMA", GSM = "GSM", SIP = "SIP", NONE = "NONE";

    public static String getPhoneType(int phoneType) {

        switch (phoneType) {
            case (TelephonyManager.PHONE_TYPE_CDMA):
                return CDMA;

            case (TelephonyManager.PHONE_TYPE_GSM):
                return GSM;

            case (TelephonyManager.PHONE_TYPE_SIP):
                return SIP;

            case (TelephonyManager.PHONE_TYPE_NONE):
                return NONE;

            default:
                return NONE;
        }
    }

    public static final String ROAMING_LAST_NOTIFIED = "roaming_last_notified", SIGNAL_STRENGTH = "signal_strength";

    public static final long ROAMING_NOTIFICATION_INTERVAL = 86400 * 1000; // 24 hours

    public static boolean canShowRoaming(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences
                (context);
        return (System.currentTimeMillis() - preferences.getLong(ROAMING_LAST_NOTIFIED, 0) > ROAMING_NOTIFICATION_INTERVAL);
    }

    public static void updateRoamingNotified(Context context) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences
                (context).edit();
        editor.putLong(ROAMING_LAST_NOTIFIED, System.currentTimeMillis()).apply();
    }

    public static void StoreSignalStrength(Context context, int signalStrength) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences
                (context).edit();
        editor.putInt(SIGNAL_STRENGTH, signalStrength).apply();
    }

    public static int getSignalStrength(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences
                (context);
        return preferences.getInt(SIGNAL_STRENGTH, 0);
    }

    public static boolean isLowerOSVersion() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB ? true : false;
    }

    public static boolean isAirplaneModeOn(Context context) {
        if (context == null) {
            return false;
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }
}
