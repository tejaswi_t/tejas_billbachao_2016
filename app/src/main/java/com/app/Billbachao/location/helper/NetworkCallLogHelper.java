package com.app.Billbachao.location.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.app.Billbachao.location.model.CallLogBean;
import com.app.Billbachao.location.model.NetworkBean;
import com.app.Billbachao.location.provider.NetworkProvider;
import com.app.Billbachao.utils.DateUtils;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.PreferenceUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by BB-001 on 4/16/2016.
 */
public class NetworkCallLogHelper {

    public static final String TAG = NetworkCallLogHelper.class.getSimpleName();

    public static final String CALL_DATA = "1", NETWORK_DATA = "2";

    public static boolean insertNetworkDetails(Context context, NetworkBean networkbean) {
        if (networkbean.isInvalid()) {
            return false;
        }
        String date = DateUtils.customDateFormat(DateUtils.FORMAT_YYYY_MM_DD_HH_MM_SS);

        ContentValues values = new ContentValues();
        values.put(NetworkProvider.NetworkColoumns.OPERATOR_NAME, networkbean.getOperatorName());
        values.put(NetworkProvider.NetworkColoumns.NETWORK_TYPE, networkbean.getNetworkType());
        values.put(NetworkProvider.NetworkColoumns.SIGNAL_STRENGTH, networkbean.getSignalStrength());
        values.put(NetworkProvider.NetworkColoumns.ROAMING_STATUS, networkbean.getRoamingStatus());
        values.put(NetworkProvider.NetworkColoumns.TIME, date);
        values.put(NetworkProvider.NetworkColoumns.MOBILE_NO, networkbean.getMobileno());
        values.put(NetworkProvider.NetworkColoumns.CIRCLE, networkbean.getCircle());
        values.put(NetworkProvider.NetworkColoumns.MCC, networkbean.getMCC());
        values.put(NetworkProvider.NetworkColoumns.LAC, networkbean.getLAC());
        values.put(NetworkProvider.NetworkColoumns.LAT, networkbean.getLatitude());
        values.put(NetworkProvider.NetworkColoumns.LANG, networkbean.getLongtitude());
        values.put(NetworkProvider.NetworkColoumns.PROTOCOL, networkbean.getNetworkProtocol());
        values.put(NetworkProvider.NetworkColoumns.CELL_ID, networkbean.getCellID());
        values.put(NetworkProvider.NetworkColoumns.BATTERY_STATUS, networkbean.getBattery());
        values.put(NetworkProvider.NetworkColoumns.INTERNET_TYPE, networkbean.getInternetType());

        Uri uri = context.getContentResolver().insert(NetworkProvider.NetworkColoumns.CONTENT_URI, values);
        if (uri != null) {
            ILog.d(TAG, "Insert Network Details");
            return true;
        }
        return false;
    }

    public static ArrayList<NetworkBean> getNetworkDetails(Context context) {

        Cursor cursor = context.getContentResolver().query(NetworkProvider.NetworkColoumns.CONTENT_URI, null, null, null, null);
        ArrayList<NetworkBean> networkList = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
            networkList = networkCursorToList(cursor);
        }
        return networkList;
    }

    public static boolean isNetworkDetailsAvailable(Context context) {
        Cursor cursor = context.getContentResolver().query(NetworkProvider.NetworkColoumns.CONTENT_URI, null, null, null, null);
        if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
            cursor.close();
            return true;
        }
        return false;
    }

    private static ArrayList<NetworkBean> networkCursorToList(Cursor cursor) {
        ArrayList<NetworkBean> networkbeanList = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                NetworkBean networkbean = new NetworkBean();
                networkbean.setOperatorName(cursor.getString(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.OPERATOR_NAME)));
                networkbean.setNetworkType(cursor.getString(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.NETWORK_TYPE)));
                networkbean.setSignalStrength(cursor.getInt(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.SIGNAL_STRENGTH)));
                networkbean.setRoamingStatus(cursor.getString(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.ROAMING_STATUS)));
                networkbean.setMobileno(cursor.getString(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.MOBILE_NO)));
                networkbean.setTime(cursor.getString(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.TIME)));
                networkbean.setCircle(cursor.getString(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.CIRCLE)));
                networkbean.setMCC(cursor.getInt(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.MCC)));
                networkbean.setLAC(cursor.getInt(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.LAC)));
                networkbean.setLatitude(cursor.getDouble(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.LAT)));
                networkbean.setLongtitude(cursor.getDouble(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.LANG)));
                networkbean.setNetworkProtocol(cursor.getString(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.PROTOCOL)));
                networkbean.setCellID(cursor.getInt(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.CELL_ID)));
                networkbean.setBattery(cursor.getFloat(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.BATTERY_STATUS)));
                networkbean.setInternetType(cursor.getString(cursor.getColumnIndex(NetworkProvider.NetworkColoumns.INTERNET_TYPE)));
                networkbeanList.add(networkbean);
            } while (cursor.moveToNext());
            cursor.close();
        }
        return networkbeanList;
    }

    public static void insertDropCalls(Context context, long dropCallsCount, long zeroSignalStrength) {

        String date = DateUtils.convertTimeToDate(System.currentTimeMillis(), DateUtils.FORMAT_YYYY_MM_DD_HH_MM_SS);

        ContentValues values = new ContentValues();

        values.put(NetworkProvider.StatisticsColoumns.MOBILE_NO, PreferenceUtils.getMobileNumber(context));
        values.put(NetworkProvider.StatisticsColoumns.TIME, date);
        values.put(NetworkProvider.StatisticsColoumns.DROPPEDCALLS, dropCallsCount);
        values.put(NetworkProvider.StatisticsColoumns.ZEROSIGNAL, zeroSignalStrength);

        Uri uri = context.getContentResolver().insert(NetworkProvider.StatisticsColoumns.CONTENT_URI, values);
    }

    public static ArrayList<CallLogBean> getCallDetails(Context context) {
        ArrayList<CallLogBean> callLogBeanList = new ArrayList<CallLogBean>();
        Cursor cursor = context.getContentResolver().query(NetworkProvider.StatisticsColoumns.CONTENT_URI, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                CallLogBean callLogBean = new CallLogBean();


                callLogBean.setMobileno(cursor.getString(cursor.getColumnIndex("mobileno")));
                callLogBean.setTime(cursor.getString(cursor.getColumnIndex("time")));
                callLogBean.setDroppedcallscount(cursor.getInt(cursor.getColumnIndex("droppedcalls")));

                callLogBean.setZerosignalduration(cursor.getLong(cursor.getColumnIndex("zerosignal")));

                callLogBeanList.add(callLogBean);
            } while (cursor.moveToNext());
        }
        return callLogBeanList;
    }

    public static boolean isCallsDataAvailable(Context context) {
        Cursor cursor = context.getContentResolver().query(NetworkProvider.StatisticsColoumns.CONTENT_URI, null, null, null, null);
        if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
            cursor.close();
            ILog.d(TAG, "Call Data Available");
            return true;
        }
        return false;
    }

    public static void removeData(Context context, String call_or_network) {
        if (call_or_network == CALL_DATA) {
            context.getContentResolver().delete(NetworkProvider.NetworkColoumns.CONTENT_URI, null, null);
        } else if (call_or_network == NETWORK_DATA) {
            context.getContentResolver().delete(NetworkProvider.StatisticsColoumns.CONTENT_URI, null, null);
        }
    }
}
