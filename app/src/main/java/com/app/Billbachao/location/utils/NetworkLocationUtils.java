package com.app.Billbachao.location.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;

import com.app.Billbachao.common.ConnectionManager;
import com.app.Billbachao.location.api.MobDataUsagePushAPI;
import com.app.Billbachao.location.api.NetworkDataCallLogPushAPI;
import com.app.Billbachao.location.helper.NetworkCallLogHelper;
import com.app.Billbachao.location.model.LocationParameter;
import com.app.Billbachao.location.model.NetworkBean;
import com.app.Billbachao.utils.DateUtils;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.OperatorUtils;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * This class is responsible for providing utility for tasks related to networks.
 * <p>
 * Created by rajkumar on 4/23/2016.
 */
public class NetworkLocationUtils {

    public static final String TAG = NetworkLocationUtils.class.getSimpleName();

    /**
     * This method inserts networks data to table
     *
     * @param context     caller's context
     * @param phoneNumber phone number of user
     * @param latitude    location latitude
     * @param longitude   location longitude
     */
    public static void insertNetworkBeanDtls(Context context, String phoneNumber, double latitude, double longitude) {
        NetworkBean networkbean = new NetworkBean();
        networkbean.setRoamingStatus("");
        networkbean.setNetworkType("Unknown");
        networkbean.setMobileno(phoneNumber);
        networkbean.setBattery(0);
        networkbean.setCellID(0);
        networkbean.setLAC(0);
        networkbean.setMCC(0);
        networkbean.setCircle("");
        networkbean.setLatitude(latitude);
        networkbean.setLongtitude(longitude);
        networkbean.setSignalStrength(0);
        networkbean.setNetworkProtocol("NONE");
        networkbean.setOperatorName(OperatorUtils.getOperatorName(context));
        networkbean.setInternetType(ConnectionManager.getNetConnectionType(context));
        NetworkCallLogHelper.insertNetworkDetails(context, networkbean);
    }

    /**
     * This method inserts networks data to table
     *
     * @param context          caller's context
     * @param telephonyManager phone number of user
     * @param gsmCellLocation  location latitude
     * @param parameter        location longitude
     * @return NetworkBean object is retured.
     */
    public static NetworkBean getNetworkBean(Context context, TelephonyManager telephonyManager, GsmCellLocation gsmCellLocation, LocationParameter parameter) {
        NetworkBean networkbean = new NetworkBean();
        networkbean.setNetworkProtocol(PhoneStateChangeUtils.getPhoneType(telephonyManager.getPhoneType()));
        networkbean.setCellID(gsmCellLocation.getCid());
        networkbean.setLAC(gsmCellLocation.getLac());
        networkbean.setLatitude(parameter.getsLatitude());
        networkbean.setLongtitude(parameter.getsLongitude());
        networkbean.setSignalStrength(PhoneStateChangeUtils.getSignalStrength(context));
        networkbean.setOperatorName(OperatorUtils.getOperatorName(context));
        String roamingStatus = telephonyManager.isNetworkRoaming() ? "Y" : "N";
        networkbean.setRoamingStatus(roamingStatus);
        networkbean.setNetworkType(ConnectionManager.getNetworkType(context));
        networkbean.setMobileno(PreferenceUtils.getMobileNumber(context));
        networkbean.setBattery(0);
        networkbean.setInternetType(ConnectionManager.getNetConnectionType(context));
        return networkbean;
    }

    public static final String NETWORK_POINT_COUNT = "networkPointsCount", NETWORK_POINT_PUSH_DATE = "NetworkPointsPushedDate",
            DATA_PUSH_TIME = "DataPushTime", YYYY_MM_DD_KK_MM_SS = "yyyy/MM/dd kk:mm:ss";


    public static void pushNetworkPoint(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (ConnectionManager.isConnected(context)) {
            if (isValidToPushNetworkData(context, preferences.getInt(DATA_PUSH_TIME, 24))) {
                int count = preferences.getInt(NETWORK_POINT_COUNT, 0);
                if (count == 0) {
                    pushNetworkCallLogData(context, preferences);
                    preferences.edit().putInt(NETWORK_POINT_COUNT, 1).apply();
                    if (TextUtils.isEmpty(preferences.getString(NETWORK_POINT_PUSH_DATE, ""))) {
                        String currentDate = DateUtils.convertTimeToDate(System.currentTimeMillis(), YYYY_MM_DD_KK_MM_SS);
                        preferences.edit().putString(NETWORK_POINT_PUSH_DATE, currentDate).apply();
                    }
                }
            } else {
                preferences.edit().putInt(NETWORK_POINT_COUNT, 0).apply();
            }
        }
    }

    /**
     * This method is called to check eligibility for network data push
     *
     * @param context caller's context
     * @param hours   this hours is to compare with last sync data hours.
     * @return return true if last data sync hours passed more than @param hours or return false.
     */
    public static boolean isValidToPushNetworkData(Context context, int hours) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String networkPushDate = preferences.getString(NETWORK_POINT_PUSH_DATE, "2014/01/01 00:00:00");
        Date feedExecutedTime = DateUtils.parseDateFormat(networkPushDate, YYYY_MM_DD_KK_MM_SS);
        long diffInMilliSeconds = System.currentTimeMillis() - feedExecutedTime.getTime();
        if (TimeUnit.MILLISECONDS.toHours(diffInMilliSeconds) >= (hours)) {
            ILog.d(TAG, "pushData weekly true");
            return true;
        }
        return false;
    }

    public static long getTimeDiffInSecond(long startDate, long endDate) {
        return (endDate - startDate) / 1000;
    }

    /**
     * This method called to push network and call log data.
     *
     * @param context
     * @param preferences DefaultShared preference obj
     */
    public static void pushNetworkCallLogData(Context context, SharedPreferences preferences) {
        if (context != null && preferences != null) {
            if ((NetworkCallLogHelper.isNetworkDetailsAvailable(context) || NetworkCallLogHelper.isCallsDataAvailable(context))) {
                SharedPreferences.Editor editor = preferences.edit();
                String pushDate = DateUtils.customDateFormat(YYYY_MM_DD_KK_MM_SS);
                editor.putString(NETWORK_POINT_PUSH_DATE, pushDate).apply();
                // Send network and call log data to server
                NetworkDataCallLogPushAPI.pushNetworkCallLogData(context, NetworkCallLogHelper.getNetworkDetails(context), NetworkCallLogHelper.getCallDetails(context));
                // send Mob data usage to server
                MobDataUsagePushAPI.pushMobDataUsage(context);
            }
        }
    }
}
