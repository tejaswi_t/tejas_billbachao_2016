package com.app.Billbachao.location.api;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.location.utils.MobBillSMSUtils;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.volley.request.NetworkStringRequest;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * This class is responsible to push Mobile bills due sms details to server.
 * <p>
 * Created by rajkumar on 4/27/2016.
 */
public class MobBillSMSPushAPI {

    public static final String TAG = MobBillSMSPushAPI.class.getSimpleName();

    /**
     * @param context    context of caller
     * @param smsDetails bill due sms
     */
    public static void pushMobBillSms(Context context, String smsDetails) {
        NetworkStringRequest stringRequest = new NetworkStringRequest(Request.Method.POST, MobBillSMSUtils.URL,
                MobBillSMSUtils.getMobBillSMSHeader(context, smsDetails), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ILog.d(TAG, "Mob Bill sms push response: " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ILog.d(TAG, error.toString());
            }
        });
        VolleySingleTon.getInstance(context).addToRequestQueue(stringRequest, TAG);
    }

}
