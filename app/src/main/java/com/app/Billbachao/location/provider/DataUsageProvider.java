package com.app.Billbachao.location.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.app.Billbachao.utils.ILog;


/**
 * Created by BB-001 on 4/18/2016.
 */
public class DataUsageProvider extends ContentProvider {

    public static final String TAG = DataUsageProvider.class.getSimpleName();

    private Context mContext;

    static String DATABASE_NAME = "dataUsage.db";

    static int DATA_BASE_VERSION = 1;

    private DataUsageSQLiteHelper mHelper;

    public static final String AUTHORITY = "com.app.Billbachao.data.datausageprovider", RAW_QUERY_PATH = "/rawQuery";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final int DATAUSAGE = 1, DATAUSAGECOUNTER = 2, GROUP_BY = 3, RAW_QUERY = 4;

    private static final UriMatcher URI_MATCHER;

    public SQLiteDatabase mDatabase;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(AUTHORITY, DataUsageColoumns.TABLE_NAME, DATAUSAGE);
        URI_MATCHER.addURI(AUTHORITY, DataUsageCounterColumns.TABLE_NAME, DATAUSAGECOUNTER);
        URI_MATCHER.addURI(AUTHORITY, DataUsageCounterColumns.TABLE_NAME + RAW_QUERY_PATH, RAW_QUERY);
    }

    @Override
    public boolean onCreate() {
        mContext = getContext();
        mHelper = new DataUsageSQLiteHelper(mContext, DATABASE_NAME, null,
                DATA_BASE_VERSION);
        try {
            mDatabase = mHelper.getWritableDatabase();
        } catch (Exception ex) {
            mDatabase = mHelper.getReadableDatabase();
        }
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor = null;
        switch (URI_MATCHER.match(uri)) {
            case DATAUSAGE:
                cursor = mDatabase.query(DataUsageColoumns.TABLE_NAME, projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            case DATAUSAGECOUNTER:
                cursor = mDatabase.query(DataUsageCounterColumns.TABLE_NAME, projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            case GROUP_BY:
                cursor = mDatabase.query(DataUsageColoumns.TABLE_NAME, null, null, null, sortOrder, null, null);
                break;
            case RAW_QUERY:
                String query = "select " + "sum(" + DataUsageCounterColumns.TOTAL_DATA_IN_DAY + ") " + "as " + DataUsageCounterColumns.DATA_USED_IN_MONTH +
                        "," + "count(" + DataUsageCounterColumns.TOTAL_DATA_IN_DAY + ") " + "as " + DataUsageCounterColumns.CAPTURED_DAYS +
                        " from " + DataUsageCounterColumns.TABLE_NAME + " where date(" + DataUsageCounterColumns.RECORDED_DATE + ") >= date(?) and " + DataUsageCounterColumns.RECHARGED + " = ?";
                cursor = mDatabase.rawQuery(query, null);
                break;
            default:
                break;
        }
        if (cursor != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowId = -1;
        Uri insertUri = null;
        switch (URI_MATCHER.match(uri)) {
            case DATAUSAGE:
                rowId = mDatabase.insert(DataUsageColoumns.TABLE_NAME, null, values);
                insertUri = Uri.withAppendedPath(DataUsageColoumns.CONTENT_URI,
                        Long.toString(rowId));
                break;
            case DATAUSAGECOUNTER:
                rowId = mDatabase.insert(DataUsageCounterColumns.TABLE_NAME, null, values);
                insertUri = Uri.withAppendedPath(DataUsageCounterColumns.CONTENT_URI,
                        Long.toString(rowId));
                break;
        }
        if (rowId != -1) {
            ILog.d(TAG, "Inserted rowId: " + rowId +values);
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return insertUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int numRows = 0;
        switch (URI_MATCHER.match(uri)) {
            case DATAUSAGE:
                numRows = mDatabase.delete(DataUsageColoumns.TABLE_NAME, selection,
                        selectionArgs);
                break;
            case DATAUSAGECOUNTER:
                numRows = mDatabase.delete(DataUsageCounterColumns.TABLE_NAME, selection,
                        selectionArgs);
                break;
        }
        return numRows;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int numRows = 0;
        switch (URI_MATCHER.match(uri)) {
            case DATAUSAGE:
                numRows = mDatabase.update(DataUsageColoumns.TABLE_NAME, values,
                        selection, selectionArgs);
                break;
            case DATAUSAGECOUNTER:
                numRows = mDatabase.update(DataUsageCounterColumns.TABLE_NAME, values,
                        selection, selectionArgs);
                break;
        }
        ILog.d(TAG, "Updated rows: " + numRows);
        return numRows;
    }

    public static class DataUsageSQLiteHelper extends SQLiteOpenHelper {

        static DataUsageSQLiteHelper sInstance;

        public static DataUsageSQLiteHelper getInstance(Context context) {
            if (sInstance == null) {
                sInstance = new DataUsageSQLiteHelper(context.getApplicationContext());
            }
            return sInstance;
        }

        public DataUsageSQLiteHelper(Context context) {
            super(context, DATABASE_NAME, null,
                    DATA_BASE_VERSION);
        }

        public DataUsageSQLiteHelper(Context context, String name,
                                     SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            createDataUsageTable(db);
            createDataUsageCounterTable(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

        void createDataUsageTable(SQLiteDatabase db) {
            db.execSQL(DataUsageColoumns.CREATE_TABLE);
        }

        void createDataUsageCounterTable(SQLiteDatabase db) {
            db.execSQL(DataUsageCounterColumns.CREATE_TABLE);
        }
    }

    public static class DataUsageColoumns {

        public static String TABLE_NAME = "DataUsageTable";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                DataUsageProvider.CONTENT_URI, TABLE_NAME);

        public static final String ID = "id", WIFI_DATAUSAGE = "wifi_datausage", WIFI_COUNTER = "wifi_counter", MOBILE_DATAUSAGE = "mobile_datausage",
                MOBILE_COUNTER = "mobile_counter", RECORDED_DATE = "recorded_date";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME + " ( "
                + ID + " integer primary key autoincrement" + ", "
                + WIFI_DATAUSAGE + " integer" + ", "
                + WIFI_COUNTER + " integer" + ", "
                + MOBILE_DATAUSAGE + " integer" + ", "
                + MOBILE_COUNTER + " integer" + ", "
                + RECORDED_DATE + " text"
                + " ) ";

    }

    public static class DataUsageCounterColumns {
        public static String TABLE_NAME = "DataUsageCounterTable";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                DataUsageProvider.CONTENT_URI, TABLE_NAME);

        public static final Uri RAW_QUERY_URI = Uri.withAppendedPath(
                DataUsageProvider.CONTENT_URI, TABLE_NAME + RAW_QUERY_PATH);

        public static final String ID = "id", TOTAL_TX_BYTES = "total_tc_bytes",
                TOTAL_DATA_IN_DAY = "total_data_in_day", RECHARGED = "recharged",
                RECORDED_DATE = "recorded_date", DATA_USED_IN_MONTH = "dataUsedInMonth", CAPTURED_DAYS = "capturedDays";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME + " ( "
                + ID + " integer primary key autoincrement" + ", "
                + TOTAL_TX_BYTES + " integer" + ", "
                + TOTAL_DATA_IN_DAY + " integer" + ", "
                + RECHARGED + " text" + ", "
                + RECORDED_DATE + " text"
                + " ) ";
    }
}
