package com.app.Billbachao.location.listener;

import android.content.Context;
import android.location.Criteria;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.app.Billbachao.location.model.LocationParameter;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.LocationUtils;


/**
 * This class is responsible for updating customer's device location lat and long
 * <p>
 * Created by rajkumar on 4/23/2016.
 */
public class CellLocationListener implements LocationListener {

    private static final String TAG = CellLocationListener.class.getSimpleName();

    Context mContext;

    LocationParameter parameter;

    LocationManager locationManager;

    private static CellLocationListener sLocationUpdater;

    private CellLocationListener(Context context, LocationParameter locationParameter) {
        mContext = context;
        parameter = locationParameter;
    }

    public static CellLocationListener getInstance(Context context, LocationParameter locationParameter) {
        if (sLocationUpdater == null) {
            sLocationUpdater = new CellLocationListener(context, locationParameter);
        }
        return sLocationUpdater;
    }

    /**
     * This method updates location parameters(i.e. Lat and Long) of phone.
     */
    public void updateLocation() {

        try {
            locationManager = (LocationManager) mContext.getSystemService(Context
                    .LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_COARSE);
            criteria.setPowerRequirement(Criteria.POWER_MEDIUM);
            criteria.setCostAllowed(true);

            String provider = locationManager.getBestProvider(criteria, true);
            Location lastLocation = locationManager.getLastKnownLocation(provider);

            if (lastLocation != null) {
                LocationUtils.storeLocation(mContext, lastLocation);
                parameter.setsLatitude(lastLocation.getLatitude());
                parameter.setsLongitude(lastLocation.getLongitude());
            } else {
                parameter.setsLatitude(LocationUtils.getLocation(mContext).getLatitude());
                parameter.setsLongitude(LocationUtils.getLocation(mContext).getLatitude());
            }
            locationManager.requestLocationUpdates(provider, 1000, (float) 1.0, this);

        } catch (IllegalArgumentException ex) {
            ILog.d(TAG, "Location provider is null or doesn\'t exist");
            ex.printStackTrace();
        } catch (SecurityException ex) {
            ex.printStackTrace();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        if (location != null && parameter != null) {
            LocationUtils.storeLocation(mContext, location);
            parameter.setsLatitude(location.getLatitude());
            parameter.setsLongitude(location.getLongitude());
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

}
