package com.app.Billbachao.location.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.TrafficStats;
import android.net.Uri;

import com.app.Billbachao.common.ConnectionManager;
import com.app.Billbachao.location.model.DataUsagesCounterBean;
import com.app.Billbachao.location.provider.DataUsageProvider;
import com.app.Billbachao.utils.DateUtils;
import com.app.Billbachao.utils.ILog;

import java.util.Date;

/**
 * Created by BB-001 on 4/18/2016.
 */
public class DataUsageHelper {

    public static final String TAG = DataUsageHelper.class.getSimpleName();

    public static DataUsagesCounterBean getDataUsageByDate(Context context, String date) {
        DataUsagesCounterBean dCounterBean = null;
        SQLiteDatabase usageDB = DataUsageProvider.DataUsageSQLiteHelper.getInstance(context)
                .getReadableDatabase();
        String queryString = "select * from " + DataUsageProvider.DataUsageCounterColumns.TABLE_NAME + " where date(recorded_date) = date('" + date + "')";
        Cursor cursor = usageDB.rawQuery(queryString, null);
        if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
            dCounterBean = new DataUsagesCounterBean();
            dCounterBean.setTxBytes(cursor.getLong(cursor.getColumnIndex(DataUsageProvider.DataUsageCounterColumns.TOTAL_TX_BYTES)));
            long dataUsedInDay = cursor.getLong(cursor.getColumnIndex(DataUsageProvider.DataUsageCounterColumns.TOTAL_DATA_IN_DAY));
            dCounterBean.setDataInDay(dataUsedInDay);

            double dataUsedInMonth = 0.0;
            dataUsedInMonth = ((double) dataUsedInDay) / (1024 * 1024);
            dataUsedInMonth = Double.parseDouble(String.format("%.2f", dataUsedInMonth));

            dCounterBean.setIsRecharged(cursor.getString(cursor.getColumnIndex(DataUsageProvider.DataUsageCounterColumns.RECHARGED)));
            dCounterBean.setRecordedDate(cursor.getString(cursor.getColumnIndex(DataUsageProvider.DataUsageCounterColumns.RECORDED_DATE)));
            dCounterBean.setTotalDataMBUsedInMonth(dataUsedInMonth);
            cursor.close();
        }
        return dCounterBean;
    }

    public static boolean deleteOldData(Context context) {
        String prevDate = DateUtils.convertTimeToDate(DateUtils.getBeforeDays(30), "dd-MM-yy hh:mm:ss.SSS a");
        int rowId = context.getContentResolver().delete(DataUsageProvider.DataUsageCounterColumns.CONTENT_URI,
                DataUsageProvider.DataUsageCounterColumns.RECORDED_DATE + "<= ?", new String[]{prevDate});
        return rowId > 0;
    }

    public static DataUsagesCounterBean getDataUsageByOrder(Context context, String orderBy) {
        DataUsagesCounterBean dCounterBean = null;
        Cursor cursor = context.getContentResolver().query(DataUsageProvider.DataUsageCounterColumns.CONTENT_URI, null, null, null, orderBy);
        if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
            dCounterBean = new DataUsagesCounterBean();
            dCounterBean.setTxBytes(cursor.getLong(cursor.getColumnIndex(DataUsageProvider.DataUsageCounterColumns.TOTAL_TX_BYTES)));
            dCounterBean.setDataInDay(cursor.getLong(cursor.getColumnIndex(DataUsageProvider.DataUsageCounterColumns.TOTAL_DATA_IN_DAY)));
            dCounterBean.setIsRecharged(cursor.getString(cursor.getColumnIndex(DataUsageProvider.DataUsageCounterColumns.RECHARGED)));
            dCounterBean.setRecordedDate(cursor.getString(cursor.getColumnIndex(DataUsageProvider.DataUsageCounterColumns.RECORDED_DATE)));
            cursor.close();
        }
        return dCounterBean;
    }

    public static void updatetDataUsagedInCurrentDate(Context context, DataUsagesCounterBean dataUsagesCounterBean, boolean isUpdate) {
        int rowid = 0;
        ContentValues values = new ContentValues();
        values.put(DataUsageProvider.DataUsageCounterColumns.TOTAL_TX_BYTES, dataUsagesCounterBean.getTxBytes());
        values.put(DataUsageProvider.DataUsageCounterColumns.TOTAL_DATA_IN_DAY, dataUsagesCounterBean.getDataInDay());
        values.put(DataUsageProvider.DataUsageCounterColumns.RECHARGED, dataUsagesCounterBean.getIsRecharged());
        values.put(DataUsageProvider.DataUsageCounterColumns.RECORDED_DATE, dataUsagesCounterBean.getRecordedDate());

        if (isUpdate) {
            rowid = context.getContentResolver().update(DataUsageProvider.DataUsageCounterColumns.CONTENT_URI, values, DataUsageProvider.DataUsageCounterColumns.RECORDED_DATE + "<= ?", new String[]{dataUsagesCounterBean.getRecordedDate()});
            if (rowid > 0) {
                ILog.d(TAG, "DataUsage Updated");
            }
        } else {
            Uri uri = context.getContentResolver().insert(DataUsageProvider.DataUsageCounterColumns.CONTENT_URI, values);
            if (uri != null) {
                ILog.d(TAG, "DataUsage Inserted");
            }
        }
    }

    public static boolean setDataUsageCounter(Context context, String isRecharged, boolean isFreshInstallation) {
        String netType = ConnectionManager.getNetConnectionType(context);
        if (netType.equalsIgnoreCase(ConnectionManager.TYPE_MOBILE)) {
            long currentDataUsedinBytes = getCurrentDataUsageInBytes(); // get txBytes
            String currentDate = DateUtils.customDateFormat(DateUtils.FORMAT_YYYY_MM_DD_HH_MM_SS);
            String prevDate = DateUtils.convertTimeToDate(DateUtils.getBeforeDays(1), "dd-MM-yy hh:mm:ss.SSS a");
            DataUsagesCounterBean currentCounterBean = getDataUsageByDate(context, currentDate);

            if (null == currentCounterBean) {
                // insert record for a day
                DataUsagesCounterBean prevCounterBean = getDataUsageByDate(context, prevDate);
                DataUsagesCounterBean counterBean = new DataUsagesCounterBean();

                long dataUsedInDay = 0L;
                if (null != prevCounterBean) {
                    if (currentDataUsedinBytes < prevCounterBean.getTxBytes())   // if current txbytes is less than previous one
                        currentDataUsedinBytes = currentDataUsedinBytes + prevCounterBean.getTxBytes();   // add current txbytes to prev one
                    dataUsedInDay = (currentDataUsedinBytes - prevCounterBean.getTxBytes());//+prevCounterBean.getDataInDay();
                } else {
                    DataUsagesCounterBean lastUsedCounterBean = DataUsageHelper.getDataUsageByOrder(context, DataUsageProvider.DataUsageCounterColumns.ID); // if prev day record not in local db then we are taking last recorded data in db
                    if (null != lastUsedCounterBean) {
                        if (currentDataUsedinBytes < lastUsedCounterBean.getTxBytes())
                            currentDataUsedinBytes = currentDataUsedinBytes + lastUsedCounterBean.getTxBytes();
                        dataUsedInDay = (currentDataUsedinBytes - lastUsedCounterBean.getTxBytes());
                    } else {
                        dataUsedInDay = currentDataUsedinBytes;
                    }
                }
                if (dataUsedInDay < 0)
                    dataUsedInDay = 0;
                if (dataUsedInDay == 0)
                    dataUsedInDay = currentDataUsedinBytes;

                counterBean.setDataInDay(dataUsedInDay);
                counterBean.setTxBytes(currentDataUsedinBytes);
                counterBean.setRecordedDate(currentDate);
                counterBean.setIsRecharged(isRecharged); // -- to do
                updatetDataUsagedInCurrentDate(context, counterBean, false);
                DataUsageHelper.deleteOldData(context);
            } else {
                // update record for a day
                DataUsagesCounterBean updatedCounterBean = new DataUsagesCounterBean();

                if (currentDataUsedinBytes < currentCounterBean.getTxBytes())
                    currentDataUsedinBytes = currentCounterBean.getTxBytes();// currentDataUsedinBytes + currentCounterBean.getTxBytes();

                long dataUsedInDay = (currentDataUsedinBytes - currentCounterBean.getTxBytes()) + currentCounterBean.getDataInDay();
                dataUsedInDay = dataUsedInDay <= 0 ? currentCounterBean.getDataInDay() : dataUsedInDay;
                updatedCounterBean.setDataInDay(dataUsedInDay);

                updatedCounterBean.setTxBytes(currentDataUsedinBytes);
                updatedCounterBean.setRecordedDate(currentCounterBean.getRecordedDate());
                updatedCounterBean.setIsRecharged(isRecharged);// -- to do
                updatetDataUsagedInCurrentDate(context, updatedCounterBean, true);
            }
        } else {
            ILog.d(TAG, "Connected to WIFI");
            if (isFreshInstallation) // first time installation time only its true otherwise should be pass as false
            {
                long currentDataUsedinBytes = getCurrentDataUsageInBytes(); // get txBytes
                Date currentdate = new Date();
                String currentDate = DateUtils.customDateFormat(DateUtils.FORMAT_YYYY_MM_DD_HH_MM_SS);
                String prevDate = DateUtils.convertTimeToDate(DateUtils.getBeforeDays(1), "dd-MM-yy hh:mm:ss.SSS a");
                DataUsagesCounterBean currentCounterBean = getDataUsageByDate(context, currentDate);
                if (null == currentCounterBean) {
                    // insert record for a day
                    DataUsagesCounterBean prevCounterBean = getDataUsageByDate(context, prevDate);
                    DataUsagesCounterBean counterBean = new DataUsagesCounterBean();

                    long dataUsedInDay = 0L;
                    if (null != prevCounterBean)
                        dataUsedInDay = (currentDataUsedinBytes - prevCounterBean.getTxBytes());//+prevCounterBean.getDataInDay();
                    if (dataUsedInDay < 0)
                        dataUsedInDay = 0;
                    if (dataUsedInDay == 0)
                        dataUsedInDay = currentDataUsedinBytes;

                    counterBean.setDataInDay(dataUsedInDay);
                    counterBean.setTxBytes(currentDataUsedinBytes);
                    counterBean.setRecordedDate(currentDate);
                    counterBean.setIsRecharged(isRecharged); // -- to do
                    updatetDataUsagedInCurrentDate(context, counterBean, false);
                }
            }
        }
        return false;
    }

    public static DataUsagesCounterBean getUpliftedDataMBUsedInMonth(Context context) {
        DataUsagesCounterBean counterBean = new DataUsagesCounterBean();

        String currentDate = DateUtils.customDateFormat(DateUtils.FORMAT_YYYY_MM_DD_HH_MM_SS);
        String prevDate = DateUtils.convertTimeToDate(DateUtils.getBeforeDays(30), DateUtils.FORMAT_YYYY_MM_DD_HH_MM_SS);// -30 from a month record
        DataUsagesCounterBean rechargedCounterBean = getMonthlyDataUsaged(context, prevDate, "Y"); // give all record from last 30 days including current date
        DataUsagesCounterBean nonRechargeCounterBean = getMonthlyDataUsaged(context, prevDate, "N"); // give all record from last 30 days including current date

        double totalDataUsed = rechargedCounterBean.getTotalDataMBUsedInMonth() + nonRechargeCounterBean.getTotalDataMBUsedInMonth();
        long totalDays = rechargedCounterBean.getTotalDaysDataUsed() + nonRechargeCounterBean.getTotalDaysDataUsed();

        DataUsagesCounterBean currentDayCounterBean = getDataUsageByDate(context, currentDate);

        if (totalDays > 1) {
            // if its more thatn 1 day record id there so we have to skip current day record from calculation
            // else we have to uplift current record only
            if (currentDayCounterBean != null) {
                totalDataUsed = totalDataUsed - currentDayCounterBean.getTotalDataMBUsedInMonth();
                totalDays = totalDays - 1; // remove 1 (current day) from total days
            }
        }

        if (totalDays == 0) // fresh install day one
            totalDays = 1;

        if (totalDataUsed >= 0 && totalDays <= 30) {
            long upliftedDays = 30;//30 - totalDays;
            totalDataUsed = ((totalDataUsed / totalDays) * upliftedDays);
        } else {
            totalDataUsed = getDataUsageInMB();
        }

        counterBean.setTotalDataMBUsedInMonth(totalDataUsed);
        counterBean.setTotalDaysDataUsed(30);
        return counterBean;
    }

    private static DataUsagesCounterBean getMonthlyDataUsaged(Context context, String date, String isRecharged) {
        DataUsagesCounterBean dCounterBean = new DataUsagesCounterBean();
        Cursor cursor = null;
        double dataUsedInMonth = 0.0;
        long capturedDays = 0L;

        SQLiteDatabase usageDB = DataUsageProvider.DataUsageSQLiteHelper.getInstance(context)
                .getReadableDatabase();
        String query = "select  sum(total_data_in_day) as dataUsedInMonth,count(total_data_in_day) as capturedDays from " + DataUsageProvider.DataUsageCounterColumns.TABLE_NAME + " where date(recorded_date) >= date('" + date + "') and recharged = '" + isRecharged + "' ";
        cursor = usageDB.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
            dataUsedInMonth = cursor.getLong(cursor.getColumnIndex(DataUsageProvider.DataUsageCounterColumns.DATA_USED_IN_MONTH));
            dataUsedInMonth = ((double) dataUsedInMonth) / (1024 * 1024);
            dataUsedInMonth = Double.parseDouble(String.format("%.2f", dataUsedInMonth));
            capturedDays = cursor.getLong(cursor.getColumnIndex(DataUsageProvider.DataUsageCounterColumns.CAPTURED_DAYS));
        }

        dCounterBean.setTotalDataMBUsedInMonth(dataUsedInMonth);
        dCounterBean.setTotalDaysDataUsed(capturedDays);

        return dCounterBean;
    }

    public static double getDataUsageInMB() {
        double totalData = 0.0;
        double received = Math.ceil((double) TrafficStats.getMobileRxBytes() //getUidRxBytes(UID)
                / (1024 * 1024));
        double send = Math.ceil((double) TrafficStats.getMobileTxBytes() //getUidTxBytes(UID)
                / (1024 * 1024));
        double total = received + send;
        if (total != 0)
            totalData = totalData + Double.parseDouble(String.format("%.2f", total));
        //  else totalData = 1.0;
        return Math.floor(totalData * 30);
    }

    public static long getCurrentDataUsageInBytes() {
        long totalData = 0;
        long received = TrafficStats.getMobileRxBytes();

        long send = TrafficStats.getMobileTxBytes();
        totalData = received + send;

        return totalData;
    }

}
