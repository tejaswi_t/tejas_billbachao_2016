package com.app.Billbachao.location.utils;

import android.content.Context;
import android.net.TrafficStats;


import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.utils.DateUtils;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by rajkumar on 4/26/2016.
 */
public class MobDataUsageUtils extends ApiUtils {

    public static final String URL = BASE_URL + "PushMobileDataServlet";

    public static final String MOB_DATA = "mdata", MOB_DATA_USER = "mdatauser", DATA_TIME = "dTime";

    public static Map<String, String> getMobDataUsageHeader(Context context) {

        Map<String, String> header = new LinkedHashMap<>();
        header.put(MOBILE_NUMBER, PreferenceUtils.getMobileNumber(context));
        header.put(MOB_DATA, String.valueOf(getMobDataTraffic()));
        header.put(MOB_DATA_USER, "0");
        header.put(DATA_TIME, DateUtils.convertTimeToDate(System.currentTimeMillis(), DateUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
        header.put(REQUEST_DATE, getDate());

        return getParamsWithCheckSum(context, header);

    }

    public static long getMobDataTraffic() {
        return (TrafficStats.getMobileRxBytes() + TrafficStats.getMobileTxBytes()) / (long) Math.pow(2, 20);
    }

}
