package com.app.Billbachao.payment.cashback.model;

import com.app.Billbachao.model.BaseGsonResponse;
import com.google.myjson.annotations.SerializedName;

/**
 * Created by mihir on 23-04-2016.
 */
public class CashbackInfo extends BaseGsonResponse {

    @SerializedName("cashbackAmount")
    double cashbackAmount;

    @SerializedName("cashbackExpireDate")
    String expiryDate;

    public double getCashbackAmount() {
        return cashbackAmount;
    }

    public String getExpiryDate() {
        return expiryDate;
    }
}
