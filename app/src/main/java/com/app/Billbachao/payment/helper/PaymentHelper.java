package com.app.Billbachao.payment.helper;

import android.content.Context;
import android.content.Intent;

import com.app.Billbachao.payment.contents.activities.PaymentStatusActivity;
import com.app.Billbachao.payment.contents.activities.RechargeBilldeskPaymentActivity;
import com.app.Billbachao.payment.model.PaymentStatusInfo;
import com.app.Billbachao.payment.model.ValidationResult;
import com.app.Billbachao.recharge.plans.model.PlanValidationItem;
import com.app.Billbachao.utils.ConstantUtils;

import java.util.ArrayList;

/**
 * Created by mihir on 02-05-2016.
 */
public class PaymentHelper {

    public static void launchPayment(Context context, ArrayList<PlanValidationItem> planList,
                                     ValidationResult result) {
        if (result.isBillDesk()) {
            launchBillDeskPayment(context, planList, result);
        }
    }

    private static void launchBillDeskPayment(Context context, ArrayList<PlanValidationItem> planList,
                                             ValidationResult result) {
        Intent intent = new Intent(context, RechargeBilldeskPaymentActivity.class);
        intent.putExtra(ConstantUtils.PLANS, planList);
        intent.putExtra(ConstantUtils.VALIDATION_RESULT, result);
        context.startActivity(intent);
    }

    public static void launchPaymentStatus(Context context, PaymentStatusInfo info, ArrayList<PlanValidationItem> planList) {
        Intent intent = new Intent(context, PaymentStatusActivity.class);
        intent.putExtra(ConstantUtils.PLANS, planList);
        intent.putExtra(ConstantUtils.PAYMENT_STATUS, info);
        context.startActivity(intent);
    }

}
