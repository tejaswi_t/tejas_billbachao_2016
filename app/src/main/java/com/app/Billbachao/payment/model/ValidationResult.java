package com.app.Billbachao.payment.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.Billbachao.model.BaseGsonResponse;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.google.myjson.annotations.SerializedName;

/**
 * Created by mihir on 02-05-2016.
 */
public class ValidationResult extends BaseGsonResponse implements Parcelable {

    public final static String BD = "bd", BILLDESK_PG_MODE = "1";

    @SerializedName("bbPgOrderId")
    String bbPgOrderId;

    @SerializedName("msg")
    String msg;

    @SerializedName("pgmode")
    String pgMode;

    protected ValidationResult(Parcel in) {
        bbPgOrderId = in.readString();
        msg = in.readString();
        pgMode = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bbPgOrderId);
        dest.writeString(msg);
        dest.writeString(pgMode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ValidationResult> CREATOR = new Creator<ValidationResult>() {
        @Override
        public ValidationResult createFromParcel(Parcel in) {
            return new ValidationResult(in);
        }

        @Override
        public ValidationResult[] newArray(int size) {
            return new ValidationResult[size];
        }
    };

    public boolean isBillDesk() {
        return BILLDESK_PG_MODE.equals(pgMode);
    }

    public String getMsg() {
        return msg;
    }

    public String getBbPgOrderId() {
        return bbPgOrderId;
    }

    @Override
    public String getStatusDesc() {
        return statusDesc != null ? statusDesc : msg != null ? msg : NetworkErrorHelper.MSG_UNKNOWN;
    }
}
