package com.app.Billbachao.payment.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.Billbachao.model.BaseGsonResponse;
import com.google.myjson.annotations.SerializedName;

/**
 * Created by mihir on 02-05-2016.
 */
public class PaymentStatusInfo extends BaseGsonResponse implements Parcelable {

    @SerializedName("paymentDtls")
    public PaymentDetails paymentDetails;

    public String bbPgOrderId;

    public double paymentAmount, rechargeAmount, cashbackAmount;

    protected PaymentStatusInfo(Parcel in) {
        paymentDetails = in.readParcelable(PaymentDetails.class.getClassLoader());
        bbPgOrderId = in.readString();
        paymentAmount = in.readDouble();
        rechargeAmount = in.readDouble();
        cashbackAmount = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(paymentDetails, flags);
        dest.writeString(bbPgOrderId);
        dest.writeDouble(paymentAmount);
        dest.writeDouble(rechargeAmount);
        dest.writeDouble(cashbackAmount);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PaymentStatusInfo> CREATOR = new Creator<PaymentStatusInfo>() {
        @Override
        public PaymentStatusInfo createFromParcel(Parcel in) {
            return new PaymentStatusInfo(in);
        }

        @Override
        public PaymentStatusInfo[] newArray(int size) {
            return new PaymentStatusInfo[size];
        }
    };

    public void setPgOrderId(String pgOrderId) {
        bbPgOrderId = pgOrderId;
    }

    public String getPgOrderId() {
        return bbPgOrderId;
    }

    public boolean isPaymentSuccess() {
        return paymentDetails.transactionStatus.equalsIgnoreCase("300");
    }

    static class PaymentDetails implements Parcelable {

        @SerializedName("payTxnStatus")
        String transactionStatus;

        @SerializedName("payOrderId")
        String paymentId;

        @SerializedName("payTxnAmount")
        double transactionAmount;

        protected PaymentDetails(Parcel in) {
            transactionStatus = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(transactionStatus);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<PaymentDetails> CREATOR = new Creator<PaymentDetails>() {
            @Override
            public PaymentDetails createFromParcel(Parcel in) {
                return new PaymentDetails(in);
            }

            @Override
            public PaymentDetails[] newArray(int size) {
                return new PaymentDetails[size];
            }
        };
    }
}
