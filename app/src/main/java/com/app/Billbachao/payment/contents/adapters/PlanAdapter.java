package com.app.Billbachao.payment.contents.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.recharge.plans.model.PlanValidationItem;
import com.app.Billbachao.utils.CircleUtils;
import com.app.Billbachao.utils.OperatorUtils;

import java.util.ArrayList;

/**
 * Created by mihir on 29-04-2016.
 */
public class PlanAdapter<T extends PlanValidationItem> extends RecyclerView.Adapter<PlanAdapter.PlanViewHolder> {

    Context mContext;

    ArrayList<T> mPlanList;

    ArrayList<Integer> mSectionHeaderIndices;

    public PlanAdapter(Context context, ArrayList<T> planList) {
        mContext = context;
        mPlanList = planList;
        initSectionHeaders();
    }

    void initSectionHeaders() {
        mSectionHeaderIndices = new ArrayList<>();
        if (mPlanList.isEmpty()) return;

        String saveNumber = "";
        int index = 0;
        for (T planItem : mPlanList) {
            String number = planItem.getMobileNo();
            if (number != null && !number.equalsIgnoreCase(saveNumber)) {
                mSectionHeaderIndices.add(index);
                saveNumber = number;
            }
            index++;
        }
    }

    @Override
    public PlanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(mContext, R.layout.plan_list_row_payment, null);
        return new PlanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PlanViewHolder holder, int position) {
        PlanValidationItem item = mPlanList.get(position);
        holder.nametext.setText(item.getTitle());
        holder.costText.setText(mContext.getString(R.string.inr_d, item.getValue()));
        bindSectionHeader(holder, position, item);
    }

    private void bindSectionHeader(PlanViewHolder holder, int position, PlanValidationItem item) {
        if (mSectionHeaderIndices.contains(position)) {
            holder.sectionHeader.setVisibility(View.VISIBLE);
            holder.mobileText.setText(item.getMobileNo());
            holder.prepaidText.setText(item.getType());
            holder.operatorCircleText.setText(mContext.getString(R.string.operator_circle,
                    OperatorUtils.getOperatorNameFromId(item.getOperatorId()),
                    CircleUtils.getCircleNameFromId(item.getCircleId())));
        } else {
            holder.sectionHeader.setVisibility(View.GONE);
        }
        //Divider logic
        holder.divider.setVisibility(mSectionHeaderIndices.contains(position + 1) ? View.VISIBLE
                : View.GONE);
    }

    @Override
    public int getItemCount() {
        return mPlanList.size();
    }

    static class PlanViewHolder extends RecyclerView.ViewHolder {

        TextView costText, nametext, mobileText, operatorCircleText, prepaidText;

        View sectionHeader, divider;

        public PlanViewHolder(View itemView) {
            super(itemView);
            nametext = (TextView) itemView.findViewById(R.id.plan_name);
            costText = (TextView) itemView.findViewById(R.id.plan_cost);
            mobileText = (TextView) itemView.findViewById(R.id.mobile_number);
            operatorCircleText = (TextView) itemView.findViewById(R.id.operator_circle);
            prepaidText = (TextView) itemView.findViewById(R.id.prepaid);
            sectionHeader = itemView.findViewById(R.id.plan_item_header);
            divider = itemView.findViewById(R.id.divider);
        }
    }
}
