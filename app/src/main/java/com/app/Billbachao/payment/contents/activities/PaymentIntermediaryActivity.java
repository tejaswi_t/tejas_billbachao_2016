package com.app.Billbachao.payment.contents.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.payment.background.PaymentValidatorTask;
import com.app.Billbachao.payment.cashback.CashbackFetcher;
import com.app.Billbachao.payment.cashback.model.CashbackInfo;
import com.app.Billbachao.payment.cashback.utils.CashbackUtils;
import com.app.Billbachao.payment.contents.adapters.PlanAdapter;
import com.app.Billbachao.payment.helper.PaymentHelper;
import com.app.Billbachao.payment.model.ValidationResult;
import com.app.Billbachao.recharge.plans.model.PlanValidationItem;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import java.util.ArrayList;

/**
 * Created by mihir on 28-04-2016.
 */
public class PaymentIntermediaryActivity extends BaseActivity {

    public static final String TAG = PaymentIntermediaryActivity.class.getSimpleName();

    ArrayList<PlanValidationItem> plansList;

    RecyclerView mRecyclerView;

    TextView mCashbackText, mPromoText;

    Button mGotoPayment;

    public static final String PLANS = "plans";

    boolean mIsSingleNumber, mIsSingleRecharge, mCashbackFetched, mPendingPayment;

    double mTransactionAmount, mPaymentAmount, mCashback;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_intermediary);
        initView();
        getData();
        loadDataOnUI();
    }

    @Override
    protected boolean isCartItemSupported() {
        return false;
    }

    void initView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager
                .VERTICAL, false));
        mGotoPayment = (Button) findViewById(R.id.goto_payment);
        mCashbackText = (TextView) findViewById(R.id.cashback);
        mPromoText = (TextView) findViewById(R.id.promo_code);
        mGotoPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceedToPayment();
            }
        });
        //setTitle(R.string.recharge);
    }

    void getData() {
        plansList = getIntent().getParcelableArrayListExtra(PLANS);
        if (plansList == null || plansList.isEmpty()) {
            finish();
            return;
        }
        // Calculate recharge modes for setting title etc.
        if (plansList.size() <= 1) {
            mIsSingleNumber = true;
            mIsSingleRecharge = true;
        } else {
            mIsSingleRecharge = false;
            mIsSingleNumber = true;
            String mobile = plansList.get(0).getMobileNo();
            for (PlanValidationItem planValidationItem : plansList) {
                if (!planValidationItem.getMobileNo().equalsIgnoreCase(mobile)) {
                    mIsSingleNumber = false;
                    break;
                }
            }
        }
        mTransactionAmount = 0;
        for (PlanValidationItem planValidationItem : plansList)
            mTransactionAmount += planValidationItem.getValue();
        mPaymentAmount = mTransactionAmount - mCashback;
        fetchCashback();
    }

    void loadDataOnUI() {
        PlanAdapter adapter = new PlanAdapter(this, plansList);
        mRecyclerView.setAdapter(adapter);
        ((TextView)findViewById(R.id.total_recharge)).setText(getString(R.string
                .total_recharge_n2, mTransactionAmount));
        updatePaymentUI();
    }

    void updatePaymentUI() {
        if (mCashbackFetched) {
            mCashbackText.setText(mCashback > 0 ? getString(R.string.cashback_n2, mCashback) :
                    getString(R.string.no_cashback));
            mGotoPayment.setText(getString(R.string.proceed_to_pay_n2, mPaymentAmount));
        }
        mCashbackText.setEnabled(mTransactionAmount >= CashbackUtils.MIN_TRANSACTION_CASHBACK);
    }

    void fetchCashback() {
        CashbackFetcher cashbackFetcher = new CashbackFetcher(this, infoListener);
        cashbackFetcher.trigger(TAG);
    }

    private void validateAndPayment() {
        double effectiveCashback = mTransactionAmount < CashbackUtils.MIN_TRANSACTION_CASHBACK ? 0
                : mCashback;
        PaymentValidatorTask paymentValidatorTask = new PaymentValidatorTask(this, mPaymentValidatorListener);
        paymentValidatorTask.trigger(TAG, plansList, mTransactionAmount, effectiveCashback, mPaymentAmount);
        showProgressDialog(getString(R.string.validating_recharge));
    }

    void runPaymentCalculation() {
        mPaymentAmount = mTransactionAmount < CashbackUtils.MIN_TRANSACTION_CASHBACK ?
                mTransactionAmount : Math.max(0, mTransactionAmount - mCashback);
    }

    void proceedToPayment() {
        if (mCashbackFetched) {
            validateAndPayment();
        } else {
            mPendingPayment = true;
            showProgressDialog(getString(R.string.fetching_cashback));
            fetchCashback();
        }
    }

    @Override
    protected void onDestroy() {
        VolleySingleTon.getInstance(this).cancelPendingRequests(TAG);
        super.onDestroy();
    }

    CashbackFetcher.CashbackInfoListener infoListener = new CashbackFetcher.CashbackInfoListener() {
        @Override
        public void onCashbackUpdated(CashbackInfo info) {
            dismissProgressDialog();
            mCashback = info.getCashbackAmount();
            mCashbackFetched = true;
            runPaymentCalculation();
            updatePaymentUI();
            if (mPendingPayment) {
                validateAndPayment();
                mPendingPayment = false;
            }
        }

        @Override
        public void onCashbackError(String networkErrorMessage) {
            dismissProgressDialog();
            mCashback = 0;
        }
    };

    PaymentValidatorTask.PaymentValidatorListener mPaymentValidatorListener = new PaymentValidatorTask.PaymentValidatorListener() {

        @Override
        public void onValidated(ArrayList<PlanValidationItem> planList, ValidationResult result) {
            dismissProgressDialog();
            PaymentHelper.launchPayment(PaymentIntermediaryActivity.this, planList, result);
            finish();
        }

        @Override
        public void onError(String networkErrorMessage) {
            dismissProgressDialog();
            showToast(networkErrorMessage);
        }
    };
}
