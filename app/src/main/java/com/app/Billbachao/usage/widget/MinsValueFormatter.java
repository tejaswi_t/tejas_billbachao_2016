package com.app.Billbachao.usage.widget;

import android.content.Context;

import com.app.Billbachao.R;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

/**
 * Created by mihir on 05-04-2016.
 */
public class MinsValueFormatter implements ValueFormatter {

    Context mContext;

    public MinsValueFormatter(Context context) {
        mContext = context;
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        if ((int) value <= 0)
            return "";
        return mContext.getString(R.string.n_min, (int) value);
    }
}
