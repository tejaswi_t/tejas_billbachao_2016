package com.app.Billbachao.usage.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.TrafficStats;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.CallLog;
import android.provider.Telephony;

import com.app.Billbachao.common.permissions.PermissionWrapper;
import com.app.Billbachao.database.MobileOperatorDBHelper;
import com.app.Billbachao.database.STDNumberSeriesDBHelper;
import com.app.Billbachao.location.helper.DataUsageHelper;
import com.app.Billbachao.model.OperatorCircleInfo;
import com.app.Billbachao.utils.CircleUtils;
import com.app.Billbachao.utils.DataUsageUtils;
import com.app.Billbachao.utils.DateUtils;
import com.app.Billbachao.utils.NumberUtils;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by mihir on 13-08-2015.
 * <p/>
 * Class created to assist calculations for UsageInfo
 */
public class UsageHelper {

    static int sTotalOutGoingCall, sLocalOutGoingCall, sNationalOutGoingCall,
            sISDOutGoingCall, sOnnetOutGoingcall, sOffnetOutGoingcall, sOtherOutGoingCall,
            sNightCall,
            sLocalOnnetCall, sLocalOffnetCall, sNationalOnnetCall, sNationalOffnetCall,
            sLocalLandline, sNationalLandline;

    static int sTotalOutboxSms, sLocalOutSms, sNationalOutSms, sISDOutSms, sOnnetSms, sOffnetSms,
            sOtherSms, sNightSms;

    static long sDataUsageMb;

    public static final String DEFAULT_NIGHT_TIME_START = "22", DEFAULT_NIGHT_TIME_END = "08";

    static boolean sIsInitialized = false;

    public static void init(Context context) {
        if (sIsInitialized) return;
        calculateOutgoingCallDuration(context);
        calculateOutboxSms(context);
        sDataUsageMb = calculateDataMb(context);
        sIsInitialized = true;
    }

    public static void reset() {
        sIsInitialized = false;
    }

    static void calculateOutgoingCallDuration(Context context) {
        int totalCallDuration = 0, isdOutGoingCall = 0;
        int localOutGoingCall = 0, nationalOutGoingCall = 0, onnetOutGoingcall = 0,
                offnetOutGoingcall = 0, otherOutGoingCall = 0, localOnnetCall = 0,
                localOffnetCall = 0, nationalOnnetCall = 0, nationalOffnetCall = 0, localLandline
                = 0, nationalLandline = 0, nightCall = 0;
        if (!PermissionWrapper.hasCallLogsPermissions(context)) {
            return;
        }
        Cursor cursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null,
                CallLog.Calls.TYPE + "=?", new String[]{String.valueOf(CallLog.Calls
                        .OUTGOING_TYPE)}, null);
        long time = getStartDateTime();

        if (cursor != null) {
            if (cursor.moveToFirst()) {

                int homeCircle = Integer.parseInt(PreferenceUtils.getCircleId(context));
                int pairedCircle = CircleUtils.getPairedCircle(homeCircle);
                int homeOperator = Integer.parseInt(PreferenceUtils.getOperatorId(context));

                MobileOperatorDBHelper mobileDbHelper = MobileOperatorDBHelper.getInstance(context);
                STDNumberSeriesDBHelper stdDbHelper = STDNumberSeriesDBHelper.getInstance(context);

                int numberIndex = cursor.getColumnIndex(CallLog.Calls.NUMBER);
                int durationIndex = cursor.getColumnIndex(CallLog.Calls.DURATION);
                int dateIndex = cursor.getColumnIndex(CallLog.Calls.DATE);

                Date nightDurationStart = getNightCallDate(context, PreferenceUtils.NIGHT_TIME_FROM,
                        DEFAULT_NIGHT_TIME_START);
                Date nightDurationEnd = getNightCallDate(context, PreferenceUtils.NIGHT_TIME_TO,
                        DEFAULT_NIGHT_TIME_END);

                do {
                    long callTime = cursor.getLong(dateIndex);
                    if (callTime >= time) {
                        int duration = cursor.getInt(durationIndex);
                        totalCallDuration += duration;

                        String number = cursor.getString(numberIndex);
                        // Add various filters for different cases
                        number = number.replace(" ", "");

                        // ISD number
                        if (NumberUtils.isNumberInternational(number)) {
                            isdOutGoingCall += duration;
                        } else {
                            number = NumberUtils.processNumber(number);

                            int isMobileNumber = NumberUtils.checkIfMobileNumber(number);
                            if (isMobileNumber != NumberUtils.UNKNOWN) {
                                OperatorCircleInfo info = (isMobileNumber == NumberUtils.YES) ?
                                        mobileDbHelper.getOperatorCircleInfo(number) : stdDbHelper
                                        .getOperatorCircleInfo(number);

                                boolean isLocal = false;
                                if (info.getCircleId() == homeCircle || info.getCircleId() == pairedCircle) {
                                    localOutGoingCall += duration;
                                    isLocal = true;
                                    if (isMobileNumber == NumberUtils.NO) {
                                        localLandline += duration;
                                    }
                                } else {
                                    nationalOutGoingCall += duration;
                                    if (isMobileNumber == NumberUtils.NO) {
                                        nationalLandline += duration;
                                    }
                                }

                                if (info.getOperatorId() == homeOperator) {
                                    onnetOutGoingcall += duration;
                                    if (isMobileNumber == NumberUtils.YES) {
                                        if (isLocal) {
                                            localOnnetCall += duration;
                                        } else {
                                            nationalOnnetCall += duration;
                                        }
                                    }
                                } else {
                                    offnetOutGoingcall += duration;
                                    if (isMobileNumber == NumberUtils.YES) {
                                        if (isLocal) {
                                            localOffnetCall += duration;
                                        } else {
                                            nationalOffnetCall += duration;
                                        }
                                    }
                                }

                                // Night calls
                                if (isMobileNumber == NumberUtils.YES) {
                                    Date truncatedTime = DateUtils.extractTimeFromDate(callTime);
                                    if (truncatedTime.before(nightDurationEnd) || truncatedTime
                                            .after
                                                    (nightDurationStart)) {
                                        nightCall += duration;
                                    }
                                }
                            } else {
                                otherOutGoingCall += duration;
                            }

                        }

                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        sISDOutGoingCall = isdOutGoingCall / 60;
        sTotalOutGoingCall = Math.max((totalCallDuration - otherOutGoingCall) / 60, 0);
        sOnnetOutGoingcall = onnetOutGoingcall / 60;
        sOffnetOutGoingcall = offnetOutGoingcall / 60;
        sLocalOutGoingCall = localOutGoingCall / 60;
        sNationalOutGoingCall = nationalOutGoingCall / 60;
        sOtherOutGoingCall = otherOutGoingCall / 60;

        sLocalOnnetCall = localOnnetCall / 60;
        sLocalOffnetCall = localOffnetCall / 60;
        sNationalOnnetCall = nationalOnnetCall / 60;
        sNationalOffnetCall = nationalOffnetCall / 60;
        sLocalLandline = localLandline / 60;
        sNationalLandline = nationalLandline / 60;

        sNightCall = nightCall / 60;

        // Code for rounding off in cases, when rounding off causes difference in total by 1 minute
        sOffnetOutGoingcall = Math.max(sTotalOutGoingCall - sOnnetOutGoingcall, 0);
        sNationalOutGoingCall = Math.max(sTotalOutGoingCall - sLocalOutGoingCall, 0);
    }

    private static Date getNightCallDate(Context context, String nightTimePreference, String
            defaultTime) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String time = preferences.getString(nightTimePreference, defaultTime);
        if (time.length() <= 2) {
            return DateUtils.parseDateFromHour(time);
        } else {
            return DateUtils.parseDateFromHourMinutes(time);
        }
    }

    static long getStartDateTime() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -30);
        return cal.getTimeInMillis();
    }

    public static int getTotalOutGoingCall() {
        return sTotalOutGoingCall;
    }

    public static int getLocalOutGoingCall() {
        return sLocalOutGoingCall;
    }

    public static int getNationalOutGoingCall() {
        return sNationalOutGoingCall;
    }

    public static int getISDOutGoingCall() {
        return sISDOutGoingCall;
    }

    public static int getOnnetOutGoingcall() {
        return sOnnetOutGoingcall;
    }

    public static int getOffnetOutGoingcall() {
        return sOffnetOutGoingcall;
    }

    public static int getOtherOutGoingCall() {
        return sOtherOutGoingCall;
    }

    public static int getLocalOnnetCall() {
        return sLocalOnnetCall;
    }

    public static int getLocalOffnetCall() {
        return sLocalOffnetCall;
    }

    public static int getNationalOnnetCall() {
        return sNationalOnnetCall;
    }

    public static int getNationalOffnetCall() {
        return sNationalOffnetCall;
    }

    public static int getLocalLandline() {
        return sLocalLandline;
    }

    public static int getNationalLandline() {
        return sNationalLandline;
    }

    public static int getNightCall() {
        return sNightCall;
    }

    public static int getNightSms() {
        return sNightSms;
    }

    static void calculateOutboxSms(Context context) {
        if (!PermissionWrapper.hasSmsReadPermissions(context)) return;
        Cursor cursor = context.getContentResolver().query(Uri.parse("content://sms/sent"), null,
                null, null, /*Telephony.Sms.TYPE + "=?", new String[]{String.valueOf(Telephony
                .Sms.MESSAGE_TYPE_SENT)},*/ null);
        long time = getStartDateTime();

        int totalSms = 0;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int dateIndex = cursor.getColumnIndex(Telephony.Sms.DATE);
                int numberIndex = cursor.getColumnIndex(Telephony.Sms.ADDRESS);

                int isdSMS = 0;
                int localSms = 0, nationalSms = 0, onnetSms = 0, offnetSms = 0, otherSms = 0;
                int nightSMS = 0;

                int homeCircle = Integer.parseInt(PreferenceUtils.getCircleId(context));
                int pairedCircle = CircleUtils.getPairedCircle(homeCircle);
                int homeOperator = Integer.parseInt(PreferenceUtils.getOperatorId(context));

                MobileOperatorDBHelper mobileDbHelper = MobileOperatorDBHelper.getInstance(context);
                STDNumberSeriesDBHelper stdDbHelper = STDNumberSeriesDBHelper.getInstance(context);

                Date nightDurationStart = getNightCallDate(context, PreferenceUtils.NIGHT_TIME_FROM,
                        DEFAULT_NIGHT_TIME_START);
                Date nightDurationEnd = getNightCallDate(context, PreferenceUtils.NIGHT_TIME_TO,
                        DEFAULT_NIGHT_TIME_END);

                do {
                    long smsTime = cursor.getLong(dateIndex);
                    if (smsTime >= time) {

                        String number = cursor.getString(numberIndex);
                        // Add various filters for different cases
                        if (number == null) {
                            continue;
                        }
                        number = number.replace(" ", "");
                        totalSms++;
                        // ISD number
                        if (NumberUtils.isNumberInternational(number)) {
                            isdSMS++;
                        } else {
                            number = NumberUtils.processNumber(number);

                            int isMobileNumber = NumberUtils.checkIfMobileNumber(number);
                            if (isMobileNumber != NumberUtils.UNKNOWN) {
                                OperatorCircleInfo info = (isMobileNumber == NumberUtils.YES) ?
                                        mobileDbHelper.getOperatorCircleInfo(number) : stdDbHelper
                                        .getOperatorCircleInfo(number);

                                if (info.getCircleId() == homeCircle || info.getCircleId() == pairedCircle) {
                                    localSms++;
                                } else {
                                    nationalSms++;
                                }

                                if (info.getOperatorId() == homeOperator) {
                                    onnetSms++;
                                } else {
                                    offnetSms++;
                                }

                                // Night SMS
                                if (isMobileNumber == NumberUtils.YES) {
                                    Date truncatedTime = DateUtils.extractTimeFromDate(smsTime);
                                    if (truncatedTime.before(nightDurationEnd) || truncatedTime
                                            .after
                                                    (nightDurationStart)) {
                                        nightSMS++;
                                    }
                                }
                            } else {
                                otherSms++;
                            }
                        }
                    }
                } while (cursor.moveToNext());
                sTotalOutboxSms = totalSms - otherSms;
                sLocalOutSms = localSms;
                sNationalOutSms = nationalSms;
                sISDOutSms = isdSMS;
                sOnnetSms = onnetSms;
                sOffnetSms = offnetSms;
                sOtherSms = otherSms;

                sNightSms = nightSMS;
            }
            cursor.close();
        }
    }

    public static int getTotalOutboxSms() {
        return sTotalOutboxSms;
    }

    public static int getLocalOutSms() {
        return sLocalOutSms;
    }

    public static int getNationalOutSms() {
        return sNationalOutSms;
    }

    public static int getISDOutSms() {
        return sISDOutSms;
    }

    public static int getOnnetSms() {
        return sOnnetSms;
    }

    public static int getOffnetSms() {
        return sOffnetSms;
    }

    public static int getOtherSms() {
        return sOtherSms;
    }

    public static long getMobileDataTrafficStats() {
        return (TrafficStats.getMobileRxBytes() + TrafficStats.getMobileTxBytes()) / (long) Math
                .pow(2, 20);
    }

    public static long getWifiTrafficStats() {
        return (TrafficStats.getTotalRxBytes() + TrafficStats.getTotalTxBytes() - TrafficStats
                .getMobileRxBytes() - TrafficStats.getMobileTxBytes()) / (long) Math
                .pow(2, 20);
    }

    public static long getEstimatedDataUsageMB(Context context) {
        if (context == null) {
            return 0;
        }

        double dataUsed = DataUsageHelper.getUpliftedDataMBUsedInMonth(context).getTotalDataMBUsedInMonth();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        int defaultDataCap = PreferenceUtils.isPrepaid(context) ? DataUsageUtils
                .DATA_LIMIT_PREPAID_MB : DataUsageUtils.DATA_LIMIT_POSTPAID_MB;
        long dataLimitCap = preferences.getInt(PreferenceUtils.MAX_LIMIT_DATA_MB, defaultDataCap);
        return Math.min(Math.max((long) dataUsed, getMobileDataTrafficStats()), dataLimitCap);
    }

    public static long calculateDataMb(Context context) {
        if (context == null) {
            return 0;
        }
        long estimatedDataUsage = getEstimatedDataUsageMB(context);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getLong(PreferenceUtils.DATA_USAGE, estimatedDataUsage);
    }

    public static long getDataMb() {
        return sDataUsageMb;
    }

    /**
     * Calculates only total outgoing calls
     *
     * @param context
     * @return
     */
    public static int calculateOutgoingCalls(Context context) {
        if (!PermissionWrapper.hasCallLogsPermissions(context)) {
            return 0;
        }
        Cursor cursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null,
                CallLog.Calls.TYPE + "=?", new String[]{String.valueOf(CallLog.Calls
                        .OUTGOING_TYPE)}, null);
        long time = getStartDateTime();
        long totalCallDuration = 0;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int durationIndex = cursor.getColumnIndex(CallLog.Calls.DURATION);
                int dateIndex = cursor.getColumnIndex(CallLog.Calls.DATE);
                do {
                    long callTime = cursor.getLong(dateIndex);
                    if (callTime >= time) {
                        int duration = cursor.getInt(durationIndex);
                        totalCallDuration += duration;
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return (int) (totalCallDuration / 60);
    }

    /**
     * Calculates only total outbox sms
     *
     * @param context
     * @return
     */
    public static int calculateSms(Context context) {
        Cursor cursor = context.getContentResolver().query(Uri.parse("content://sms/sent"), null,
                null, null, null);
        long time = getStartDateTime();
        int totalSms = 0;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int dateIndex = cursor.getColumnIndex(Telephony.Sms.DATE);
                do {
                    long smsTime = cursor.getLong(dateIndex);
                    if (smsTime >= time) {
                        totalSms++;
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return totalSms;
    }
}
