package com.app.Billbachao.usage.activities;

import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.cards.CardLogicEngine;
import com.app.Billbachao.common.permissions.PermissionWrapper;
import com.app.Billbachao.usage.helper.UsageHelper;
import com.app.Billbachao.usage.widget.MinsValueFormatter;
import com.app.Billbachao.usage.widget.SmsValueFormatter;
import com.app.Billbachao.utils.OperatorUtils;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.util.ArrayList;

/**
 * Created by mihir on 05-04-2016.
 */
public class InternalUsageActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internal_usage);
        if (!(PermissionWrapper.hasCallLogsPermissions(this) && PermissionWrapper
                .hasSmsReadPermissions(this))) {
            Toast.makeText(this, R.string.no_permissions, Toast.LENGTH_SHORT).show();
            if (!CardLogicEngine.isDataUsageToBeShown(this)) {
                finish();
                return;
            }
        }
        loadData();
    }

    void loadData() {

        // Call details
        View callGroup = findViewById(R.id.call_usage);
        if (UsageHelper.getTotalOutGoingCall() > 0) {
            ((TextView) callGroup.findViewById(R.id.usage_title)).setText(R.string.call_details);
            initiateChart(callGroup, R.id.chart_1, 2, new int[]{UsageHelper.getLocalOutGoingCall(),
                    UsageHelper.getNationalOutGoingCall()}, new String[]{"LOCAL", "STD"}, new
                    int[]{R.color.chart_color_1, R.color.chart_color_2}, new MinsValueFormatter
                    (this));

            initiateChart(callGroup, R.id.chart_2, 2, new int[]{UsageHelper.getOnnetOutGoingcall(),
                    UsageHelper.getOffnetOutGoingcall()}, new String[]{OperatorUtils.getOperatorName
                    (this), "OTHERS"}, new int[]{R.color.chart_color_3, R.color.chart_color_4}, new
                    MinsValueFormatter(this));
        } else {
            callGroup.setVisibility(View.GONE);
        }

        // SMS details
        View smsGroup = findViewById(R.id.sms_usage);
        if (UsageHelper.getTotalOutboxSms() > 0) {
            ((TextView) smsGroup.findViewById(R.id.usage_title)).setText(R.string.sms_details);
            initiateChart(smsGroup, R.id.chart_1, 2, new int[]{UsageHelper.getLocalOutSms(),
                    UsageHelper.getNationalOutSms()}, new String[]{"LOCAL", "NATIONAL"}, new
                    int[]{R.color.chart_color_1, R.color.chart_color_2}, new SmsValueFormatter
                    (this));
            initiateChart(smsGroup, R.id.chart_2, 2, new int[]{UsageHelper.getOnnetSms(),
                    UsageHelper.getOffnetSms()}, new String[]{OperatorUtils.getOperatorName(this)
                    , "OTHERS"}, new int[]{R.color.chart_color_3, R.color.chart_color_4}, new
                    SmsValueFormatter(this));
        } else {
            smsGroup.setVisibility(View.GONE);
        }

        // Data details
        View dataGroup = findViewById(R.id.data_usage_group);
        if (!isDataToBeShown()) {
            dataGroup.setVisibility(View.GONE);
            return;
        }

        ((TextView) dataGroup.findViewById(R.id.usage_title)).setText(R.string.internet_details);
        initiateDataChart(dataGroup, R.id.wifi_usage, R.string.wifi, (int) UsageHelper
                .getWifiTrafficStats(), R.color.chart_color_5, R.drawable.ic_wifi);
        initiateDataChart(dataGroup, R.id.mobile_usage, R.string.mobile, (int) UsageHelper
                .getMobileDataTrafficStats(), R.color.chart_color_6, R.drawable.ic_network);

    }

    private void initiateChart(View parentView, int chartId, int count, int values[], String
            labels[], int colorIds[], ValueFormatter valueFormatter) {
        ArrayList<Entry> yVals = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            yVals.add(new Entry(values[i], i));
        }

        PieDataSet pieDataSet = new PieDataSet(yVals, "");
        pieDataSet.setColors(colorIds, this);

        PieData data = new PieData(labels, pieDataSet);
        data.setValueFormatter(valueFormatter);
        data.setValueTextColor(Color.WHITE);
        data.setValueTextSize(10.0f);

        PieChart pieChart = (PieChart) parentView.findViewById(chartId);
        pieChart.setHoleRadius(25f);
        pieChart.setData(data);
        pieChart.setTouchEnabled(false);
        pieChart.setDrawSliceText(false);
        pieChart.setTransparentCircleRadius(0f);
        pieChart.setDescription("");
        pieChart.setRotationAngle(0);
        Legend legend = pieChart.getLegend();
        legend.setWordWrapEnabled(true);
        pieChart.animateY(2000);
    }

    private void initiateDataChart(View dataGroup, int parentId, int labelId, int valueMb, int
            colorId, int iconResId) {
        View parentView = dataGroup.findViewById(parentId);
        ((ImageView) parentView.findViewById(R.id.image)).setImageResource(iconResId);
        ((TextView) parentView.findViewById(R.id.value)).setText(getString(R.string.nd_mb,
                valueMb));
        ((TextView) parentView.findViewById(R.id.label)).setText(labelId);
        parentView.setBackgroundDrawable(getCircleDrawable(ContextCompat.getColor(this, colorId),
                R.dimen.data_usage_chart_size));
    }

    boolean isDataToBeShown() {
        return CardLogicEngine.isDataUsageToBeShown(this);
    }

    ShapeDrawable getCircleDrawable(int color, int sizeResId) {
        ShapeDrawable shapeDrawable = new ShapeDrawable(new OvalShape());
        int size = getResources().getDimensionPixelSize(sizeResId);
        shapeDrawable.setIntrinsicHeight(size);
        shapeDrawable.setIntrinsicWidth(size);
        shapeDrawable.getPaint().setColor(color);
        return shapeDrawable;
    }
}
