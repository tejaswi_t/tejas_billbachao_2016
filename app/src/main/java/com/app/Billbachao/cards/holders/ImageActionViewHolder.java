package com.app.Billbachao.cards.holders;

import android.view.View;
import android.widget.ImageView;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.model.CardDataObject;
import com.squareup.picasso.Picasso;

/**
 * Created by mihir on 19-03-2016.
 */
public class ImageActionViewHolder extends MessageActionViewHolder {

    ImageView imageView;

    public ImageActionViewHolder(View itemView) {
        super(itemView);
        imageView = (ImageView) itemView.findViewById(R.id.image);
    }

    @Override
    public void bindView(CardDataObject cardDataObject) {
        super.bindView(cardDataObject);
        if (imageView != null) {
            if (cardDataObject.getImageUrl() != null) {
                imageView.setVisibility(View.VISIBLE);
                Picasso.with(imageView.getContext()).load(cardDataObject.getImageUrl()).into(imageView);
            } else {
                imageView.setVisibility(View.GONE);
            }
        }
    }

}
