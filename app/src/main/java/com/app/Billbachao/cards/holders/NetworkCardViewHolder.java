package com.app.Billbachao.cards.holders;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.apis.NetworkCardApi;
import com.app.Billbachao.cards.model.CardDataObject;
import com.app.Billbachao.recommendations.network.model.NetworkOperatorInfo;
import com.app.Billbachao.recommendations.network.model.NetworkRecoCardGsonResponse;
import com.app.Billbachao.utils.UiUtils;

/**
 * Created by mihir on 26-04-2016.
 */
public class NetworkCardViewHolder extends DashboardCardViewHolder {

    public static final String TAG = NetworkCardViewHolder.class.getSimpleName();

    View mNetworkDataView, placeHolderView, homeView, workView;

    Context mContext;

    NetworkRecoCardGsonResponse networkRecoData;

    public NetworkCardViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        mNetworkDataView = itemView.findViewById(R.id.network_data_card);
        placeHolderView = itemView.findViewById(R.id.network_placeholder_card);
        initDataView(itemView);
        initPlaceHolderView(itemView);
        setDataVisibility(false);
    }

    @Override
    public void bindView(CardDataObject cardDataObject) {

    }

    void initDataView(View itemView) {
        homeView = itemView.findViewById(R.id.home_group);
        workView = itemView.findViewById(R.id.work_group);
    }

    void initPlaceHolderView(final View itemView) {
        ((TextView) itemView.findViewById(R.id.message_placeholder)).setText(R.string
                .network_card_message);
        Button callToAction = (Button) itemView.findViewById(R.id.call_to_action_placeholder);
        callToAction.setText(R.string.network_card_action);
        callToAction.setOnClickListener(mOnClickListener);
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            UiUtils.launchNetworkScreen(itemView.getContext());
        }
    };

    public void updateData(NetworkRecoCardGsonResponse networkRecoCardGsonResponse) {
        if (!networkRecoCardGsonResponse.isSuccess()) {
            setNoDataView(null);
        } else {
            networkRecoData = networkRecoCardGsonResponse;
            loadData();
        }
    }

    public void setNoDataView(String error) {
        setDataVisibility(false);
    }

    void setDataVisibility(boolean isVisible) {
        mNetworkDataView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        placeHolderView.setVisibility(isVisible ? View.GONE : View.VISIBLE);
    }

    void loadData() {
        if (networkRecoData == null) return;
        boolean dataVisible = false;
        NetworkOperatorInfo homeNetwork = networkRecoData
                .getCurrentHomeNetwork();
        if (homeNetwork != null) {
            loadGroup(homeView, NetworkCardApi.MODE_HOME, homeNetwork);
            dataVisible = true;
        }
        NetworkOperatorInfo workNetwork = networkRecoData
                .getCurrentWorkNetwork();
        if (workNetwork != null) {
            loadGroup(workView, NetworkCardApi.MODE_WORK, workNetwork);
            dataVisible = true;
        }
        setDataVisibility(dataVisible);
    }

    void loadGroup(View view, int key, NetworkOperatorInfo info) {
        ((ImageView) view.findViewById(R.id.network_location_icon)).setImageResource(getIcon(key));
        ((TextView) view.findViewById(R.id.text1)).setText(mContext.getString(R.string
                .signal_strength_n, info.getSignalStrength()));
        ((TextView) view.findViewById(R.id.text2)).setText(mContext.getString(R.string
                .speed_n_kbps, info.getDataSpeed()));
    }

    int getIcon(int key) {
        switch (key) {
            case NetworkCardApi.MODE_HOME:
                return R.drawable.ic_home_colored;
            case NetworkCardApi.MODE_WORK:
                return R.drawable.ic_work_colored;
        }
        return R.drawable.ic_my_location_colored;
    }
}
