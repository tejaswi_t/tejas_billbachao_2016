package com.app.Billbachao.cards.binders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.holders.HorizontalScrollCardViewHolder;

/**
 * Created by mihir on 13-04-2016.
 */
public class HorizontalScrollCardDataBinder extends CardDataBinder<HorizontalScrollCardViewHolder> {

    public HorizontalScrollCardDataBinder(Context context) {
        super(context);
    }

    @Override
    public HorizontalScrollCardViewHolder newViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_scroll_card, parent, false);
        return new HorizontalScrollCardViewHolder(view);
    }
}
