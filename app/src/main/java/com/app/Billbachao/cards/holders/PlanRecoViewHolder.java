package com.app.Billbachao.cards.holders;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.model.CardDataObject;
import com.app.Billbachao.model.OperatorWisePlanInfo;
import com.app.Billbachao.payment.helper.PaymentIntermediaryHelper;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.widget.view.DotPagerIndicator;

import java.util.ArrayList;

/**
 * Created by mihir on 21-04-2016.
 */
public class PlanRecoViewHolder extends DashboardCardViewHolder {

    public static final String TAG = PlanRecoViewHolder.class.getSimpleName();

    View dataContainer, progressContainer;

    ViewPager planViewPager;

    DotPagerIndicator pagerIndicator;

    TextView noItemText;

    Context mContext;

    public PlanRecoViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        progressContainer = itemView.findViewById(R.id.progress_container);
        dataContainer = itemView.findViewById(R.id.data_container);
        planViewPager = (ViewPager) itemView.findViewById(R.id.pager);
        pagerIndicator = (DotPagerIndicator) itemView.findViewById(R.id.pager_indicator);
        noItemText = (TextView) itemView.findViewById(R.id.no_item);
        setDataVisibility(false);
    }

    @Override
    public void bindView(CardDataObject cardDataObject) {

    }

    public void updateData(ArrayList<OperatorWisePlanInfo> planList) {
        setDataVisibility(true);
        if (planList == null || planList.size() == 0 || planList.get(0).planList.size() == 0) {
            updateError(null);
            ILog.d(TAG, "No reco plans");
        } else {
            noItemText.setVisibility(View.GONE);
            CustomPagerAdapter adapter = new CustomPagerAdapter(mContext, planList.get(0).planList);
            planViewPager.setAdapter(adapter);
            if (planList.get(0).planList.size() > 1) {
                pagerIndicator.setViewPager(planViewPager);
                pagerIndicator.setVisibility(View.VISIBLE);
                ((TextView) itemView.findViewById(R.id.card_title)).setText(R.string.recommendations);
            } else {
                pagerIndicator.setVisibility(View.GONE);
                ((TextView) itemView.findViewById(R.id.card_title)).setText(R.string.recommendation);
            }
            ILog.d(TAG, "Plans count " + planList.get(0).planList.size());
        }
    }

    public void updateError(String error) {
        setDataVisibility(true);
        noItemText.setText(error != null ? error : mContext.getString(R.string.no_plans));
        noItemText.setVisibility(View.VISIBLE);
        pagerIndicator.setVisibility(View.GONE);
        planViewPager.setVisibility(View.GONE);
    }

    void setDataVisibility(boolean isVisible) {
        dataContainer.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        progressContainer.setVisibility(isVisible ? View.GONE : View.VISIBLE);
    }

    static class CustomPagerAdapter extends PagerAdapter {

        ArrayList<OperatorWisePlanInfo.PlanInfo> mPlanList;

        Context mContext;

        boolean mIsPrepaid;

        CustomPagerAdapter(Context context, ArrayList<OperatorWisePlanInfo.PlanInfo> planList) {
            mContext = context;
            mPlanList = planList;
            mIsPrepaid = PreferenceUtils.isPrepaid(mContext);
        }

        @Override
        public int getCount() {
            return mPlanList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return object instanceof View && view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = View.inflate(mContext, R.layout.plan_reco_card_item, null);

            final OperatorWisePlanInfo.PlanInfo planInfo = mPlanList.get(position);

            ((ImageView) itemView.findViewById(R.id.image)).setImageResource(getPlanIconId(getPlanType(planInfo.name)));
            String message;
            if (mIsPrepaid) {
                if (planInfo.saveAmnt == 0) {
                    message = mContext.getString(R.string.you_are_on_best_plan_s_category,
                            planInfo.type);
                } else if (planInfo.cost > 50) {
                    message = mContext.getString(R.string.save_inr_n2_recharge_inr_n_pack_s,
                            planInfo.saveAmnt, planInfo.cost, planInfo.type);
                } else {
                    message = mContext.getString(R.string.save_n_percent_recharge_inr_n_pack_s,
                            planInfo.savePercentage, planInfo.cost, planInfo.type);
                }
            } else {
                message = planInfo.description;
            }
            ((TextView) itemView.findViewById(R.id.message)).setText(message);
            ((TextView) itemView.findViewById(R.id.title)).setText(mContext.getString(R.string.rupee_n_pack, planInfo.cost));
            itemView.findViewById(R.id.call_to_action).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PaymentIntermediaryHelper.launchFromReco(mContext, planInfo);
                }
            });

            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    static final int TALKTIME = 1, SMS = 2, DATA = 3, ROAMING = 4, COMBO = 5;

    static int getPlanIconId(int type) {
        switch (type) {
            case TALKTIME:
            case ROAMING:
                return R.drawable.std;
            case DATA:
                return R.drawable.internet_pack;
            case SMS:
            case COMBO:
            default:
                return R.drawable.v_e;
        }
    }

    static int getPlanType(String planName) {
        planName = planName.toUpperCase();

        if (planName.contains("SMS")) return SMS;
        if (planName.contains("DATA")) return DATA;
        if (planName.contains("TALKTIME") || planName.contains("TOPUP")) return TALKTIME;
        if (planName.contains("ROAMING")) return ROAMING;
        if (planName.contains("COMBO")) return COMBO;

        return SMS;
    }
}