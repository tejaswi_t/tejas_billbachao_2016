package com.app.Billbachao.cards.server;

import android.content.Context;

import com.app.Billbachao.cards.CardFactory;
import com.app.Billbachao.cards.CardUtils;
import com.app.Billbachao.cards.model.CardDataObject;
import com.google.myjson.Gson;
import com.google.myjson.GsonBuilder;
import com.google.myjson.JsonSyntaxException;

import java.util.ArrayList;

/**
 * Created by mihir on 06-05-2016.
 */
public class CardServerConverter {

    public static final String CARD_LIST = "cardsList";

    public static CardDataObject getCardObject(Context context, CardServerObject serverObject) {
        switch (serverObject.templateId << CardUtils.TEMPLATE_SHIFT_BITS) {
            case CardUtils.TEMPLATE_NONE:
            default:
                return CardFactory.generateCard(context, serverObject.cardId);
        }
    }

    public static ArrayList<CardDataObject> getCardListFromServerList(Context context,
                                                                      ArrayList<CardServerObject>
                                                                              serverObjects) {
        ArrayList<CardDataObject> cardDataObjects = new ArrayList<>();
        for (CardServerObject serverObject : serverObjects) {
            CardDataObject cardDataObject = getCardObject(context, serverObject);
            if (cardDataObject != null) cardDataObjects.add(cardDataObject);
        }
        return cardDataObjects;
    }

    public static ArrayList<CardServerObject> getCardsListFromJson(String jsonObject) {
        Gson gson = new GsonBuilder().create();
        try {
            CardsListResponse response = gson.fromJson(jsonObject, CardsListResponse.class);
            return response == null ? null : response.getCardList();
        } catch (JsonSyntaxException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
