package com.app.Billbachao.cards.holders;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.CardUtils;
import com.app.Billbachao.cards.model.CardDataObject;
import com.app.Billbachao.cards.widget.ActionClickListener;
import com.app.Billbachao.widget.view.DotPagerIndicator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by mihir on 13-04-2016.
 */
public class HorizontalScrollCardViewHolder extends DashboardCardViewHolder {

    ViewPager mHorizontalScrollView;

    Context mContext;

    DotPagerIndicator pagerIndicator;

    public HorizontalScrollCardViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        mHorizontalScrollView = (ViewPager) itemView.findViewById(R.id.horizontal_scroll_view);
        pagerIndicator = (DotPagerIndicator) itemView.findViewById(R.id.pager_indicator);
    }

    @Override
    public void bindView(CardDataObject cardDataObject) {
        mHorizontalScrollView.setAdapter(new CustomViewPagerAdapter(mContext, cardDataObject
                .getHorizontalCards()));
        int height = cardDataObject.getPagerHeight();
        if (height == 0)
            height = mContext.getResources().getDimensionPixelSize(R.dimen.horizontal_card_height);
        ViewGroup.LayoutParams layoutParams = mHorizontalScrollView.getLayoutParams();
        layoutParams.height = height;
        mHorizontalScrollView.setLayoutParams(layoutParams);

        if (cardDataObject.getHorizontalCards().size() > 1) {
            pagerIndicator.setViewPager(mHorizontalScrollView);
            pagerIndicator.setVisibility(View.VISIBLE);
        } else {
            pagerIndicator.setVisibility(View.GONE);
        }
    }

    class CustomViewPagerAdapter extends PagerAdapter {

        Context mContext;

        ArrayList<CardDataObject> mCardsList;

        int layoutId;

        public CustomViewPagerAdapter(Context context, ArrayList<CardDataObject> cardsList) {
            mContext = context;
            mCardsList = cardsList;
            CardDataObject cardDataObject = mCardsList.get(0);
            layoutId = getLayoutId(cardDataObject.getDesignId());
        }

        @Override
        public int getCount() {
            return mCardsList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return object instanceof View && view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = View.inflate(mContext, layoutId, null);
            CardDataObject cardDataObject = mCardsList.get(position);

            instantiateCard(itemView, cardDataObject);
            instantiateItemActionButtons(itemView, cardDataObject);
            instantiateItemTitle(itemView, cardDataObject);
            instantiateItemMessage(itemView, cardDataObject);
            instantiateItemImage(itemView, cardDataObject);

            container.addView(itemView);
            return itemView;
        }

        void instantiateCard(View itemView, CardDataObject cardDataObject) {
            int color = cardDataObject.getColor();
            if (color != -1) {
                if (itemView instanceof CardView) {
                    ((CardView) itemView).setCardBackgroundColor(color);
                } else {
                    itemView.setBackgroundColor(color);
                }
            }
        }

        void instantiateItemActionButtons(View itemView, CardDataObject cardDataObject) {
            Button callToAction = (Button) itemView.findViewById(R.id.call_to_action);
            Button callToActionSecond = (Button) itemView.findViewById(R.id.call_to_action_second);

            if (callToAction != null) {
                if (cardDataObject.getCallToAction() != null) {
                    callToAction.setVisibility(View.VISIBLE);
                    callToAction.setText(cardDataObject.getCallToAction());
                    if (cardDataObject.isDarkTheme())
                        callToAction.setTextColor(ContextCompat.getColor(mContext, R.color
                                .textPrimaryColorInverse));
                    if (cardDataObject.getActionLink() != null)
                        callToAction.setOnClickListener(new ActionClickListener(cardDataObject
                                .getActionLink()));
                } else {
                    callToAction.setVisibility(View.GONE);
                }
            }

            if (callToActionSecond != null) {
                if (cardDataObject.getCallToActionAdditional() != null) {
                    callToActionSecond.setVisibility(View.VISIBLE);
                    callToActionSecond.setText(cardDataObject.getCallToActionAdditional());
                    if (cardDataObject.isDarkTheme())
                        callToActionSecond.setTextColor(ContextCompat.getColor(mContext, R.color
                                .textPrimaryColorInverse));
                    if (cardDataObject.getActionLinkAdditional() != null)
                        callToActionSecond.setOnClickListener(new ActionClickListener
                                (cardDataObject.getActionLinkAdditional()));
                } else {
                    callToActionSecond.setVisibility(View.GONE);
                }
            }
        }

        void instantiateItemTitle(View itemView, CardDataObject cardDataObject) {
            TextView title = (TextView) itemView.findViewById(R.id.title);
            if (title != null) {
                if (cardDataObject.getTitle() != null) {
                    title.setVisibility(View.VISIBLE);
                    title.setText(cardDataObject.getTitle());
                    if (cardDataObject.isDarkTheme())
                        title.setTextColor(ContextCompat.getColor(mContext, R.color
                                .textPrimaryColorInverse));
                } else {
                    title.setVisibility(View.GONE);
                }
            }
        }

        void instantiateItemMessage(View itemView, CardDataObject cardDataObject) {
            TextView message = (TextView) itemView.findViewById(R.id.message);
            if (message != null) {
                if (cardDataObject.getMessage() != null) {
                    message.setVisibility(View.VISIBLE);
                    message.setText(cardDataObject.getMessage());
                    if (cardDataObject.isDarkTheme())
                        message.setTextColor(ContextCompat.getColor(mContext, R.color
                                .textPrimaryColorInverse));
                } else {
                    message.setVisibility(View.GONE);
                }
            }
        }

        void instantiateItemImage(View itemView, CardDataObject cardDataObject) {
            ImageView icon = (ImageView) itemView.findViewById(R.id.image);
            View iconParent = itemView.findViewById(R.id.image_parent);
            if (icon != null) {
                if (cardDataObject.getIconResId() != 0) {
                    icon.setVisibility(View.VISIBLE);
                    icon.setImageResource(cardDataObject.getIconResId());
                    int iconBgColor = cardDataObject.getIconBgColor();
                    if (iconBgColor != CardDataObject.INVALID_COLOR && iconParent != null) {
                        //iconParent.setBackgroundColor(iconBgColor);
                    }
                } else if (cardDataObject.getImageUrl() != null) {
                    icon.setVisibility(View.VISIBLE);
                    Picasso.with(icon.getContext()).load(cardDataObject.getImageUrl()).into(icon);
                } else {
                    icon.setVisibility(View.GONE);
                }
            }
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        int getLayoutId(int designId) {
            int layoutId = R.layout.card_title_description_action;
            switch (designId) {
                case CardUtils.TEMPLATE_MSG_ACTION:
                    return R.layout.card_message_action;
                case CardUtils.TEMPLATE_ICON_MSG_ACTION:
                    return R.layout.card_icon_message_action;
                case CardUtils.TEMPLATE_TITLE_MSG_ACTION:
                    return R.layout.card_title_description_action;
                case CardUtils.TEMPLATE_IMAGE_ACTION:
                    return R.layout.card_image_action;
            }
            return layoutId;
        }
    }
}
