package com.app.Billbachao.cards.model;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.app.Billbachao.cards.CardUtils;

import java.util.ArrayList;

/**
 * Builder pattern for creating CardDataObject
 * Created by mihir on 18-03-2016.
 */
public class CardDataObjectBuilder {

    Context context;

    CardDataObject mCardDataObject;

    public CardDataObjectBuilder(Context context, int id) {
        mCardDataObject = new CardDataObject(id);
        this.context = context;
    }

    private CardDataObjectBuilder() {

    }

    /**
     * Build a title, message card
     *
     * @param title
     * @param message
     * @return
     */
    public CardDataObject buildTitleMsgCard(String title, String message) {
        buildMessageCard(message);
        mCardDataObject.title = title;
        mCardDataObject.designId = CardUtils.TEMPLATE_TITLE_MSG_ACTION;
        return mCardDataObject;
    }

    /**
     * Build a title, message card
     *
     * @param titleId   String id of title
     * @param messageId String id of message
     * @return
     */
    public CardDataObject buildTitleMsgCard(int titleId, int messageId) {
        return buildTitleMsgCard(context.getString(titleId), context.getString(messageId));
    }

    /**
     * Build a simple message card
     *
     * @param message
     * @return
     */
    public CardDataObject buildMessageCard(String message) {
        addMessage(message);
        return mCardDataObject;
    }

    /**
     * Build a simple message card
     *
     * @param messageId String id of message
     * @return
     */
    public CardDataObject buildMessageCard(int messageId) {
        return buildMessageCard(context.getString(messageId));
    }

    public CardDataObject buildIconCard(int iconResId) {
        return buildIconCardWithColor(iconResId, CardDataObject.INVALID_COLOR);
    }

    public CardDataObject buildIconCardWithColor(int iconResId, int iconBgColor) {
        mCardDataObject.iconResId = iconResId;
        mCardDataObject.iconBgColor = iconBgColor;
        mCardDataObject.designId = CardUtils.TEMPLATE_ICON_MSG_ACTION;
        return mCardDataObject;
    }

    public CardDataObject buildIconCardWithColorId(int iconResId, int iconBgColorId) {
        return buildIconCardWithColor(iconResId, ContextCompat.getColor(context, iconBgColorId));
    }

    /**
     * Add action button with text and link
     *
     * @param action
     * @param actionLink
     * @return
     */
    public CardDataObjectBuilder addAction(String action, String actionLink) {
        mCardDataObject.callToAction = action;
        mCardDataObject.actionLink = actionLink;
        return this;
    }

    /**
     * Add action button with text and link
     *
     * @param actionId   string ID of action text
     * @param actionLink
     * @return
     */
    public CardDataObjectBuilder addAction(int actionId, String actionLink) {
        return addAction(context.getString(actionId), actionLink);
    }

    /**
     * Add action button with text
     *
     * @param action
     * @return
     */
    public CardDataObjectBuilder addAction(String action) {
        return addAction(action, null);
    }

    /**
     * Add action button with action string id
     *
     * @param actionId
     * @return
     */
    public CardDataObjectBuilder addAction(int actionId) {
        return addAction(context.getString(actionId));
    }

    /**
     * Add secondary action button with text and link
     *
     * @param action
     * @param actionLink
     * @return
     */
    public CardDataObjectBuilder addSecondaryAction(String action, String actionLink) {
        mCardDataObject.callToActionAdditional = action;
        mCardDataObject.actionLinkAdditional = actionLink;
        return this;
    }

    /**
     * Add secondary action button with text and link
     *
     * @param actionId   string ID of action text
     * @param actionLink
     * @return
     */
    public CardDataObjectBuilder addSecondaryAction(int actionId, String actionLink) {
        return addSecondaryAction(context.getString(actionId), actionLink);
    }

    /**
     * Add secondary action button with text
     *
     * @param action
     * @return
     */
    public CardDataObjectBuilder addSecondaryAction(String action) {
        return addSecondaryAction(action, null);
    }

    /**
     * Add secondary action button with action string id
     *
     * @param actionId
     * @return
     */
    public CardDataObjectBuilder addSecondaryAction(int actionId) {
        return addSecondaryAction(context.getString(actionId));
    }

    /**
     * Add message for the card
     *
     * @param messageId resource ID of the message
     * @return
     */
    public CardDataObjectBuilder addMessage(int messageId) {
        return addMessage(context.getString(messageId));
    }

    /**
     * Add message for the card
     *
     * @param message
     * @return
     */
    public CardDataObjectBuilder addMessage(String message) {
        mCardDataObject.message = message;
        mCardDataObject.designId = CardUtils.TEMPLATE_MSG_ACTION;
        return this;
    }

    /**
     * Color for card background, color provided by resource
     *
     * @param colorResId
     * @return
     */
    public CardDataObjectBuilder setColorResource(int colorResId) {
        return setColor(ContextCompat.getColor(context, colorResId));
    }

    /**
     * Color for card background
     *
     * @param color
     * @return
     */
    public CardDataObjectBuilder setColor(int color) {
        mCardDataObject.color = color;
        return this;
    }

    /**
     * Dark theme needed for color change in title, action button etc
     *
     * @return
     */
    public CardDataObjectBuilder setDarkTheme() {
        mCardDataObject.darkTheme = true;
        return this;
    }


    /**
     * Build image card, with URL from server
     *
     * @param imageUrl
     * @return
     */
    public CardDataObject buildImageCard(String imageUrl) {
        mCardDataObject.imageUrl = imageUrl;
        mCardDataObject.designId = CardUtils.TEMPLATE_IMAGE_ACTION;
        return mCardDataObject;
    }

    /**
     * Fixed UI card, UI will be inflated from static binder classes
     *
     * @return Card
     */
    public CardDataObject buildFixedCard() {
        mCardDataObject.designId = CardUtils.TEMPLATE_NONE;
        return mCardDataObject;
    }

    /**
     * Horizontal scrollable cards, dont need any other attributes, as they would be assigned to card list.
     * @param horizontalCardDataObjects
     * @return
     */
    public CardDataObject buildHorizontalListCard(ArrayList<CardDataObject> horizontalCardDataObjects) {
        mCardDataObject.mHorizontalCards = horizontalCardDataObjects;
        mCardDataObject.designId = CardUtils.TEMPLATE_HORIZONTAL_LIST;
        return mCardDataObject;
    }

    /**
     * Horizontal scrollable cards with pager height
     *
     * @param horizontalCardDataObjects
     * @param height                    Pager height, use it intelligently with type of cards.
     * @return
     */
    public CardDataObject buildHorizontalListCard(ArrayList<CardDataObject>
                                                          horizontalCardDataObjects, int height) {
        mCardDataObject.mHorizontalCards = horizontalCardDataObjects;
        mCardDataObject.pagerHeight = height;
        mCardDataObject.designId = CardUtils.TEMPLATE_HORIZONTAL_LIST;
        return mCardDataObject;
    }
}
