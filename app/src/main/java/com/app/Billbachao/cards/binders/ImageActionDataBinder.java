package com.app.Billbachao.cards.binders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.holders.IconMessageActionViewHolder;

/**
 * Created by mihir on 29-03-2016.
 */
public class ImageActionDataBinder extends CardDataBinder<IconMessageActionViewHolder> {

    public ImageActionDataBinder(Context context) {
        super(context);
    }

    @Override
    public IconMessageActionViewHolder newViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_image_action, parent, false);
        return new IconMessageActionViewHolder(view);
    }
}
