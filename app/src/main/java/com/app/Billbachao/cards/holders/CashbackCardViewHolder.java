package com.app.Billbachao.cards.holders;

import android.content.Context;
import android.view.View;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.model.CardDataObject;
import com.app.Billbachao.payment.cashback.model.CashbackInfo;

/**
 * Created by mihir on 25-04-2016.
 */
public class CashbackCardViewHolder extends IconMessageActionViewHolder {

    Context mContext;

    CashbackInfo mCashbackInfo;

    String error;

    public CashbackCardViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        icon.setImageResource(R.drawable.cashback);
    }

    @Override
    public void bindView(CardDataObject cardDataObject) {
        updateView();
        bindActionView(cardDataObject);
    }

    public void setCashbackInfo(CashbackInfo cashbackInfo) {
        if (cashbackInfo == null) return;
        mCashbackInfo = cashbackInfo;
        updateView();
    }

    void updateView() {
        if (mCashbackInfo != null) {
            if (mCashbackInfo.isSuccess() && mCashbackInfo.getCashbackAmount() > 0)
                message.setText(mContext.getString(R.string.cashback_n2_amount_and_d_validity,
                        mCashbackInfo.getCashbackAmount(), mCashbackInfo.getExpiryDate()));
            else
                message.setText(R.string.no_cashback_card_description);
        } else {
            if (error != null)
                message.setText(error);
            else
                message.setText(R.string.fetching_cashback);
        }
    }

    public void setError(String error) {
        this.error = error;
        updateView();
    }
}
