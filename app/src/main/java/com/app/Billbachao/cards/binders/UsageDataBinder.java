package com.app.Billbachao.cards.binders;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.CardLogicEngine;
import com.app.Billbachao.cards.holders.ActionCardViewHolder;
import com.app.Billbachao.cards.model.CardDataObject;
import com.app.Billbachao.usage.helper.UsageHelper;
import com.app.Billbachao.utils.UiUtils;

/**
 * Created by mihir on 06-01-2016.
 */
public class UsageDataBinder extends CardDataBinder<UsageDataBinder.ViewHolder> {

    ViewHolder mHolder;

    String mDataText;

    int mDataUnit = R.string.ns_mb;

    public UsageDataBinder(Context context) {
        super(context);
        mUsageHelperThread.start();
    }

    @Override
    public ViewHolder newViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.usage_detail_card, parent, false);
        mHolder = new ViewHolder(view);
        return mHolder;
    }

    class ViewHolder extends ActionCardViewHolder {

        TextView dataMb, callUsage, smsUsage;

        boolean isDataToBeShown;

        public ViewHolder(View itemView) {
            super(itemView);
            isDataToBeShown = CardLogicEngine.isDataUsageToBeShown(getContext());
            initUsageView(itemView);
        }

        @Override
        public void bindView(CardDataObject cardDataObject) {
            super.bindView(cardDataObject);
            updateUsage();
        }

        private void initUsageView(View itemView) {
            ViewGroup circleGroupCall = (ViewGroup) itemView.findViewById(R.id.circle_call);
            ((ImageView) circleGroupCall.findViewById(R.id.usage_icon_view)).setImageResource(R.drawable.calls);
            callUsage = (TextView) circleGroupCall.findViewById(R.id.usage_value);
            circleGroupCall.findViewById(R.id.usage_icon_container).setOnClickListener(mOnClickListener);

            ViewGroup circleGroupSms = (ViewGroup) itemView.findViewById(R.id.circle_sms);
            ((ImageView) circleGroupSms.findViewById(R.id.usage_icon_view)).setImageResource(R.drawable.sms);
            smsUsage = (TextView) circleGroupSms.findViewById(R.id.usage_value);
            circleGroupSms.findViewById(R.id.usage_icon_container).setOnClickListener(mOnClickListener);

            ViewGroup circleGroupData = (ViewGroup) itemView.findViewById(R.id.circle_data);
            if (isDataToBeShown) {
                ((ImageView) circleGroupData.findViewById(R.id.usage_icon_view)).setImageResource(R.drawable.internet);
                dataMb = (TextView) circleGroupData.findViewById(R.id.usage_value);
                circleGroupData.findViewById(R.id.usage_icon_container).setOnClickListener(mOnClickListener);
            } else {
                circleGroupData.setVisibility(View.GONE);
            }
        }

        void updateUsage() {
            Context context = getContext();
            callUsage.setText(context.getString(R.string.n_min, UsageHelper.getTotalOutGoingCall()));
            smsUsage.setText(context.getString(R.string.n_sms, UsageHelper.getTotalOutboxSms()));
            if (isDataToBeShown) {
                calculateDataUsage();
                dataMb.setText(context.getString(mDataUnit, mDataText));
            }
        }
    }

    void calculateDataUsage() {
        long dataUsage = UsageHelper.getDataMb();
        mDataText = String.valueOf(dataUsage);
        if (dataUsage >= Math.pow(2, 10)) {
            mDataUnit = R.string.ns_gb;
            if (dataUsage % Math.pow(2, 10) == 0) {
                mDataText = String.valueOf((int) (dataUsage / Math.pow(2, 10)));
            } else {
                mDataText = String.format("%.2f", dataUsage / Math.pow(2, 10));
            }
        }
    }

    Thread mUsageHelperThread = new Thread(new Runnable() {
        public void run() {
            UsageHelper.init(getContext());
            mUiHandler.removeCallbacksAndMessages(null);
            mUiHandler.sendEmptyMessage(0);
        }
    });

    Handler mUiHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (mHolder != null) {
                mHolder.updateUsage();
            }
        }
    };

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.usage_icon_container:
                    UiUtils.launchInternalUsage(getContext());
                    break;
            }
        }
    };
}
