package com.app.Billbachao.cards.widget;

import android.view.View;

import com.app.Billbachao.launch.ActionLinkHelper;

/**
 * Created by mihir on 13-04-2016.
 */
public class ActionClickListener implements View.OnClickListener {

    String mActionLink;

    public ActionClickListener(String actionLink) {
        mActionLink = actionLink;
    }

    @Override
    public void onClick(View v) {
        ActionLinkHelper.parseActionLink(v.getContext(), mActionLink);
    }
}
