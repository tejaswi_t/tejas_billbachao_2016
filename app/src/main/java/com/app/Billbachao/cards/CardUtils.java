package com.app.Billbachao.cards;

/**
 * Created by mihir on 06-01-2016.
 */
public class CardUtils {

    public static final int TYPE_NONE = 0xFF, TYPE_USAGE = 0x01, TYPE_BEST_PLAN= 0x03,
            TYPE_HAND_SET = 0x04, TYPE_BEST_NETWORK = 0x05, TYPE_DATA_CARD = 0x06,
            TYPE_APP_DAY = 0x07, TYPE_OFFERS = 0x08, TYPE_INVITE = 0x09, TYPE_RATE = 0x0A,
            TYPE_USSD = 0x0B, TYPE_CASHBACK = 0x0C;

    // Due to this masking, other cards should be used upto 0x3F only
    public static final int MASK_SELF_HELP = 0x40;

    // Masking Self Help cards in one category
    public static final int TYPE_DND = 0x01 | MASK_SELF_HELP, TYPE_CHECK_BALANCE = 0x02 |
            MASK_SELF_HELP, TYPE_DATA_SETTINGS = 0x03 | MASK_SELF_HELP, TYPE_PLAN_ALERTS = 0x04 |
            MASK_SELF_HELP, TYPE_SELF_HELP_LIST = 0x0F | MASK_SELF_HELP;

    /* Templates will be stored as 0x100, 0x200 etc. Bit shifter count below.
    This is done to separate out card ids and design ids for view types
     */
    public static final int TEMPLATE_SHIFT_BITS = 8;

    // Design templates, have to offset them by some positions, for ViewType logic enhancement
    public static final int TEMPLATE_TITLE_MSG_ACTION = 6 << TEMPLATE_SHIFT_BITS,
            TEMPLATE_MSG_ACTION = 2 << TEMPLATE_SHIFT_BITS,
            TEMPLATE_IMAGE_ACTION = 3 << TEMPLATE_SHIFT_BITS, TEMPLATE_ICON_MSG_ACTION = 4 <<
            TEMPLATE_SHIFT_BITS, TEMPLATE_NONE = 1 << TEMPLATE_SHIFT_BITS,
            TEMPLATE_HORIZONTAL_LIST = 5 << TEMPLATE_SHIFT_BITS;

    public static boolean isSelfHelpCard(int id) {
        return (MASK_SELF_HELP & id) == MASK_SELF_HELP;
    }
}
