package com.app.Billbachao.cards.contents.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.CardLogicEngine;
import com.app.Billbachao.cards.contents.adapters.DashboardCardAdapter;
import com.app.Billbachao.cards.model.CardDataObject;
import com.app.Billbachao.cards.server.CardServerConverter;
import com.app.Billbachao.cards.server.CardServerObject;
import com.app.Billbachao.cards.widget.swipe.ItemTouchHelperCallback;
import com.app.Billbachao.usagelog.LogHelper;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.widget.view.DividerItemDecoration;

import java.util.ArrayList;

/**
 * Created by mihir on 07-01-2016.
 */
public class DashboardCardFragment extends Fragment {

    public static final String TAG = DashboardCardFragment.class.getSimpleName();

    View mRootView;

    RecyclerView mRecyclerView;

    DashboardCardAdapter mCardAdapter;

    SharedPreferences mLogPreferences, mApiDataPreferences;

    ArrayList<CardDataObject> mCardList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_card_dashboard, container, false);
        initializeView();
        return mRootView;
    }

    void initializeView() {
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Intent intent = getActivity().getIntent();
        if (intent.hasExtra(CardServerConverter.CARD_LIST)) {
            mCardList = CardLogicEngine.filterCardSet(getActivity(), CardServerConverter
                    .getCardListFromServerList(getActivity(), intent
                    .<CardServerObject>getParcelableArrayListExtra(CardServerConverter.CARD_LIST)));
        }
        ILog.d(TAG, "Cards list from server " + mCardList + " Size " + (mCardList != null ? mCardList
                .size() : 0));
        if (mCardList == null || mCardList.isEmpty())
            mCardList = CardLogicEngine.generateCardSet(getActivity());
        mCardAdapter = new DashboardCardAdapter(getActivity(), mCardList);
        mRecyclerView.setAdapter(mCardAdapter);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getResources()
                .getDimensionPixelSize(R.dimen.large_widget_spacing)));

        ItemTouchHelper.Callback touchHelperCallback = new ItemTouchHelperCallback(mCardAdapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(touchHelperCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
        initPreferences();
    }

    void initPreferences() {
        mLogPreferences = getActivity().getSharedPreferences(LogHelper.PREFERENCE_FILE, Context
                .MODE_PRIVATE);
        mLogPreferences.registerOnSharedPreferenceChangeListener(mPreferenceChangeListener);

        //mApiDataPreferences = getActivity().getSharedPreferences(ApiDataCacheUtils.PREFERENCES,
        // Context.MODE_PRIVATE);
        //mApiDataPreferences.registerOnSharedPreferenceChangeListener(mPreferenceChangeListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLogPreferences.unregisterOnSharedPreferenceChangeListener(mPreferenceChangeListener);
        //mApiDataPreferences.unregisterOnSharedPreferenceChangeListener(mPreferenceChangeListener);
    }

    public void updateCardList() {
        mCardList = CardLogicEngine.generateCardSet(getActivity());
        mCardAdapter.updateData(mCardList);
    }

    SharedPreferences.OnSharedPreferenceChangeListener mPreferenceChangeListener = new
            SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String
                        key) {
                    updateCardList();
                }
            };
}
