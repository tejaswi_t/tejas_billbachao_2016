package com.app.Billbachao.cards.binders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.cards.holders.ActionCardViewHolder;
import com.app.Billbachao.cards.model.CardDataObject;
import com.app.Billbachao.shareandrate.SendFeedbackTask;
import com.app.Billbachao.shareandrate.utils.ShareUtils;

/**
 * Created by mihir on 11-04-2016.
 */
public class RateAppDataBinder extends CardDataBinder<RateAppDataBinder.ViewHolder> {

    ViewHolder mHolder;

    public RateAppDataBinder(Context context) {
        super(context);
    }

    @Override
    public ViewHolder newViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rate_app_card, parent, false);
        mHolder = new ViewHolder(view);
        return mHolder;
    }

    class ViewHolder extends ActionCardViewHolder {

        RatingBar mRatingBar;

        public ViewHolder(View itemView) {
            super(itemView);
            mRatingBar = (RatingBar) itemView.findViewById(R.id.app_rating);
            itemView.findViewById(R.id.call_to_action).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mRatingBar.getRating() > 3) {
                        ShareUtils.launchStore(getContext());
                        new SendFeedbackTask(getContext(), (int) mRatingBar.getRating(), ApiUtils
                                .NA, false).trigger();
                    } else {
                        ((BaseActivity) getContext()).showFeedbackDialog();
                    }
                }
            });
        }

        @Override
        public void bindView(CardDataObject cardDataObject) {
            super.bindView(cardDataObject);
        }
    }
}
