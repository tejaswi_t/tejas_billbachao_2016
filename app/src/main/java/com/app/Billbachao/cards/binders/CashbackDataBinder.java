package com.app.Billbachao.cards.binders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.Billbachao.R;
import com.app.Billbachao.cards.holders.CashbackCardViewHolder;
import com.app.Billbachao.payment.cashback.CashbackFetcher;
import com.app.Billbachao.payment.cashback.model.CashbackInfo;

/**
 * Created by mihir on 25-04-2016.
 */
public class CashbackDataBinder extends CardDataBinder<CashbackCardViewHolder> {

    CashbackCardViewHolder mCashbackCardViewHolder;

    public CashbackDataBinder(Context context) {
        super(context);
    }

    @Override
    public CashbackCardViewHolder newViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cashback_card,
                parent, false);
        mCashbackCardViewHolder = new CashbackCardViewHolder(view);
        new CashbackFetcher(getContext(), mCashbackInfoListener).trigger(null);
        return mCashbackCardViewHolder;
    }

    CashbackFetcher.CashbackInfoListener mCashbackInfoListener = new CashbackFetcher
            .CashbackInfoListener() {

        @Override
        public void onCashbackUpdated(CashbackInfo info) {
            mCashbackCardViewHolder.setCashbackInfo(info);
        }

        @Override
        public void onCashbackError(String message) {
            mCashbackCardViewHolder.setError(message);
        }
    };

}
