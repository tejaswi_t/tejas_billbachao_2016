package com.app.Billbachao.cards.contents.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.app.Billbachao.cards.holders.DashboardCardViewHolder;
import com.app.Billbachao.cards.model.CardDataObject;

import java.util.ArrayList;

/**
 * Layer between final implementation if
 * Created by mihir on 13-04-2016.
 */
public abstract class AbstractCardAdapter extends RecyclerView.Adapter<DashboardCardViewHolder> {

    protected Context mContext;

    protected ArrayList<CardDataObject> mCardsList;

    public AbstractCardAdapter(Context context, ArrayList<CardDataObject> cardsList) {
        mContext = context;
        mCardsList = cardsList;
    }

    public Context getContext() {
        return mContext;
    }

}
