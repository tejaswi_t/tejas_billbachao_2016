package com.app.Billbachao.cards.contents.adapters;

import android.content.Context;
import android.view.ViewGroup;

import com.app.Billbachao.cards.CardFactory;
import com.app.Billbachao.cards.CardUtils;
import com.app.Billbachao.cards.binders.CardDataBinder;
import com.app.Billbachao.cards.holders.DashboardCardViewHolder;
import com.app.Billbachao.cards.model.CardDataObject;
import com.app.Billbachao.cards.widget.swipe.ItemTouchCallbackAdapter;
import com.app.Billbachao.utils.ILog;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Adapter class with multiple view types. Using data binders for encapsulating viewholder for different view types
 * Created by mihir on 06-01-2016.
 */
public class DashboardCardAdapter extends AbstractCardAdapter implements ItemTouchCallbackAdapter {

    HashMap<Integer, CardDataBinder> mBinderMap;

    public static final String TAG = DashboardCardAdapter.class.getSimpleName();

    public DashboardCardAdapter(Context context, ArrayList<CardDataObject> cardList) {
        super(context, cardList);
        mBinderMap = new HashMap<>();
        updateBinders();
    }

    private void updateBinders() {
        for (CardDataObject cardDataObject : mCardsList) {
            int cardType = getUniqueType(cardDataObject);
            if (mBinderMap.get(cardType) == null) {
                ILog.d(TAG, "Binder map put " + cardType);
                mBinderMap.put(cardType, CardFactory.getDataBinder(mContext, cardDataObject.getDesignId(), cardType));
            }
        }
    }

    /**
     * Need a distinguished UI type for different templates
     * So, use designId and cardId combo.
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        CardDataObject cardDataObject = mCardsList.get(position);
        return getUniqueType(cardDataObject);
    }

    @Override
    public DashboardCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ILog.d(TAG, "onCreateViewHolder " + viewType);
        return mBinderMap.get(viewType).newViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(DashboardCardViewHolder holder, int position) {
        ILog.d(TAG, "onBindViewHolder " + holder.getItemViewType());
        mBinderMap.get(holder.getItemViewType()).bindCommonViewHolder(holder, position, mCardsList.get(position));
    }

    @Override
    public int getItemCount() {
        return mCardsList.size();
    }

    public void updateData(ArrayList<CardDataObject> cardList) {
        mCardsList = cardList;
        updateBinders();
        notifyDataSetChanged();
    }

    /**
     * Very very important function,
     *
     * @return
     */
    public int getUniqueType(CardDataObject cardDataObject) {
        ILog.d(TAG, "Design : " + cardDataObject.getDesignId() + " CardId : " + cardDataObject.getId());
        return cardDataObject.getDesignId() == CardUtils.TEMPLATE_NONE ? cardDataObject.getId() : cardDataObject.getDesignId();
    }

    @Override
    public void onItemSwiped(int position) {
        mCardsList.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onItemMoved(int fromPosition, int toPosition) {
        ILog.d(TAG, "onItemMoved " + fromPosition + " to " + toPosition);
        CardDataObject cardDataObject = mCardsList.remove(fromPosition);
        mCardsList.add(toPosition, cardDataObject);
        notifyItemMoved(fromPosition, toPosition);
    }
}
