package com.app.Billbachao.deals.i;

import com.app.Billbachao.deals.models.DealsAddressModel;

/**
 * Created by nitesh on 08/03/2016.
 */
public interface IDealsPincodeValidation {
    void onSuccessPincode(DealsAddressModel dealsAddressModel);
    void onErrorPincode(String error);
}
