package com.app.Billbachao.deals.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.app.Billbachao.deals.utils.DealsConstant;
import com.app.Billbachao.deals.fragment.SpecificationFragment;
import com.app.Billbachao.deals.models.RelianceTvDetailsModel;
import com.app.Billbachao.utils.ILog;

import java.util.ArrayList;
import java.util.List;

/**
 * Deals Details viewpager adapter
 * Created by nitesh on 26/02/2016.
 */
public class DealsDetailsPagerAdapter extends FragmentPagerAdapter {

    private static final String TAG = DealsDetailsPagerAdapter.class.getName();
    private List<String> mFragmentTitleList = new ArrayList<>();
    private final Context mContext;
    private RelianceTvDetailsModel relianceTvDetailsModel;

    public DealsDetailsPagerAdapter(Context mContext, FragmentManager manager, RelianceTvDetailsModel relianceTvDetailsModel) {
        super(manager);
        this.relianceTvDetailsModel = relianceTvDetailsModel;
        this.mContext = mContext;
    }

    @Override
    public Fragment getItem(int position) {

        String title = mFragmentTitleList.get(position);

        ILog.d(TAG, "-|title|-" + title);

        Bundle bundle = new Bundle();
        bundle.putString(DealsConstant.KEY_FROM, title);
        bundle.putParcelableArrayList(DealsConstant.KEY_RELIANCE_GENERIC_DETAILS, relianceTvDetailsModel.genericTvModelHashMap.get(title));

        return SpecificationFragment.instantiate(mContext, SpecificationFragment.class.getName(), bundle);

    }

    @Override
    public int getCount() {
        return (mFragmentTitleList != null && mFragmentTitleList.size() > 0) ? mFragmentTitleList.size() : 0;
    }

    public void addTitle(List<String> mFragmentTitleList) {
        this.mFragmentTitleList = mFragmentTitleList;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}
