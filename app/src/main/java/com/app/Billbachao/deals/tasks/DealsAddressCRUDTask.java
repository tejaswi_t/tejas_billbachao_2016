package com.app.Billbachao.deals.tasks;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.deals.i.IDealAddressCallback;
import com.app.Billbachao.deals.models.DealsAddressModel;
import com.app.Billbachao.apis.deals.DealsAddressApi;
import com.app.Billbachao.model.BaseGsonResponse;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import java.util.Map;

/**
 * Deals address CRUD task
 * Created by nitesh on 10-05-2016.
 */
public class DealsAddressCRUDTask {
    private Context mContext;
    private int position;

    public DealsAddressCRUDTask(Context mContext, int position) {
        this.mContext = mContext;
        this.position = position;
    }

    public void performCRUDOperation(final String from, String oldPinCode, DealsAddressModel dealsAddressModel, final IDealAddressCallback iDealAddressCallback, String tag) {

        Map<String, String> params = getParams(from, oldPinCode, dealsAddressModel);

        NetworkGsonRequest<BaseGsonResponse> request = new NetworkGsonRequest<>(Request.Method.POST, DealsAddressApi.URL_SHIPPING, BaseGsonResponse.class, params, new Response.Listener<BaseGsonResponse>() {
            @Override
            public void onResponse(BaseGsonResponse response) {

                if (response == null)
                    iDealAddressCallback.onErrorAddress(from, NetworkErrorHelper.MSG_UNKNOWN);


                if (response.status.equalsIgnoreCase(ApiUtils.SUCCESS)) {
                    iDealAddressCallback.onSuccessAddress(from, response.statusDesc, position);
                } else {
                    iDealAddressCallback.onErrorAddress(from, response.statusDesc);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                iDealAddressCallback.onErrorAddress(from, NetworkErrorHelper.getErrorStatus(error).getErrorMessage());
            }
        }, true);

        VolleySingleTon.getInstance(mContext).addToRequestQueue(request, tag);

    }


    private Map<String, String> getParams(String from, String oldPinCode, DealsAddressModel
            dealsAddressModel) {

        switch (from) {
            case ApiUtils.DELETE:
                return DealsAddressApi.deleteDealsAddress(mContext, dealsAddressModel, from);
            case ApiUtils.UPDATE:
            case ApiUtils.INSERT:
                return DealsAddressApi.addInsertUpdateDealsAddress(mContext, dealsAddressModel, from, oldPinCode);
        }

        return null;
    }
}
