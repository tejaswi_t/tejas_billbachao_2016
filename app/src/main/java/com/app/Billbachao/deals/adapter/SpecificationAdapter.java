package com.app.Billbachao.deals.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.deals.models.SpecificationModel;

import java.util.List;

/**
 * Specification Adapter
 * Created by nitesh on 25/02/2016.
 */
public class SpecificationAdapter extends ArrayAdapter<SpecificationModel> {


    private final Context context;
    private final List<SpecificationModel> list;
    private final LayoutInflater layoutInflater;

    public SpecificationAdapter(Context context, int resource, List<SpecificationModel> list) {
        super(context, resource);

        this.context = context;
        this.list = list;
        layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return (list != null && list.size() > 0 ? list.size() : 0);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SpecificationViewHolder specificationViewHolder = null;
        if (convertView == null){
            specificationViewHolder = new SpecificationViewHolder();
            convertView = layoutInflater.inflate(R.layout.view_footer_deals_television, null);
            specificationViewHolder.sepecification_title = (TextView) convertView.findViewById(R.id.specification_title);
            specificationViewHolder.sepecification_description = (TextView) convertView.findViewById(R.id.specification_description);
            specificationViewHolder.sepecification_description_title = (TextView) convertView.findViewById(R.id.specification_description_title);

            convertView.setTag(specificationViewHolder);
        }else {
            specificationViewHolder = (SpecificationViewHolder) convertView.getTag();
        }

        specificationViewHolder.populateData(context, list.get(position));
        return convertView;
    }


    public static class SpecificationViewHolder{

        public TextView sepecification_title;

        public TextView sepecification_description;

        public TextView sepecification_description_title;

        public void populateData(Context context, SpecificationModel specificationModel) {

            if (specificationModel != null){
                if (specificationModel.showHeaderTitle){
                    sepecification_title.setVisibility(View.VISIBLE);
                    sepecification_description.setVisibility(View.GONE);
                    sepecification_description_title.setVisibility(View.GONE);
                    sepecification_title.setText(specificationModel.headerTitle);
                }else {
                    sepecification_title.setVisibility(View.GONE);
                    sepecification_description.setVisibility(View.VISIBLE);
                    sepecification_description_title.setVisibility(View.VISIBLE);

                    sepecification_description.setText(specificationModel.description);
                    sepecification_description_title.setText(String.format(context.getString(R.string.deals_specification_title), specificationModel.descriptionTitle).toUpperCase());
                }
            }
        }
    }
}
