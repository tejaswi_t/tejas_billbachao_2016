package com.app.Billbachao.deals.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.myjson.annotations.SerializedName;

/**
 * Created by nitesh on 02/03/2016.
 */
public class DealsAddressModel implements Parcelable {

    @SerializedName("pincode")
    public String pincode;

    @SerializedName("firstname")
    public String firstname;

    @SerializedName("lastname")
    public String lastname;

    @SerializedName("address")
    public String address;

    @SerializedName("email")
    public String email;

    @SerializedName("mobileNo")
    public String mobileNo;

    @SerializedName("city")
    public String city;

    @SerializedName("state")
    public String state;

    @SerializedName("landmark")
    public String landmark;

    @SerializedName("latitude")
    public String latitude;

    @SerializedName("longitude")
    public String longitude;


    public DealsAddressModel(String pincode, String firstname, String lastname, String address, String mobileNumber, String city, String state, String landmark, String latitude, String longitude){
        this.pincode = pincode;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.mobileNo = mobileNumber;
        this.city = city;
        this.state = state;
        this.landmark = landmark;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    protected DealsAddressModel(Parcel in) {
        pincode = in.readString();
        firstname = in.readString();
        lastname = in.readString();
        address = in.readString();
        email = in.readString();
        mobileNo = in.readString();
        city = in.readString();
        state = in.readString();
        landmark = in.readString();
        latitude = in.readString();
        longitude = in.readString();
    }

    public static final Creator<DealsAddressModel> CREATOR = new Creator<DealsAddressModel>() {
        @Override
        public DealsAddressModel createFromParcel(Parcel in) {
            return new DealsAddressModel(in);
        }

        @Override
        public DealsAddressModel[] newArray(int size) {
            return new DealsAddressModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pincode);
        dest.writeString(firstname);
        dest.writeString(lastname);
        dest.writeString(address);
        dest.writeString(email);
        dest.writeString(mobileNo);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(landmark);
        dest.writeString(latitude);
        dest.writeString(longitude);
    }
}
