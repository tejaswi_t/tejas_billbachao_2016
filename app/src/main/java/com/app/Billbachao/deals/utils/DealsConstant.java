package com.app.Billbachao.deals.utils;

/**
 * Created by nitesh on 24/02/2016.
 */
public class DealsConstant {
    public static final String KEY_RELIANCE_DETAILS = "tvDetails";
    public static final String KEY_FROM = "tvFrom";
    public static final String KEY_EDIT_ADDRESS = "tvEditAddress";
    public static final String KEY_ADD_ADDRESS = "tvAddAddress";


    public static final String KEY_RELIANCE_GENERIC_DETAILS = "tvGenericDetails";
    public static final String KEY_ADDRESS_LIST = "tvAddressList";
    public static final String KEY_ADDRESS_MODEL = "tvAddressModel";

    public static final int REQUEST_CODE_DEALS_ADDRESS = 999;
    public static final int REQUEST_CODE_BOOKING = 888;
    public static final int REQUEST_PAYMENT_RECEIPT_SUCCESS = 666;
    public static final int REQUEST_PAYMENT_RECEIPT_FAILED = 555;


    public static final String KEY_POSITION = "position";
    public static final String KEY_PRODUCT_PAYMENT = "productPaymentList";

    public static final String KEY_FROM_TV_DEEP_LINKS = "fromTvDeepLink";
    public static final String KEY_BELL_CLICKED = "bellClicked";

    public static final String KEY_DEALS_PRODUCT_ID = "tvProductId";
    public static final String INTERNET_FAILED = "internetfailed";
    public static final String KEY_BOOKING_WITH_PAYMENT_AMT = "bookingwithpayment";
    public static final String KEY_IS_USE_BOOKING = "useBookingWithPayment";

    public static final String KEY_RECEIPT_ORDER_ID = "orderid";
    public static final String KEY_RECEIPT_ORDER_AMOUNT = "txnAmount";
    public static final String KEY_IMAGE_URL = "imageUrl";
    public static final String KEY_PAYMENT_STATUS_TITLE = "key_payment_status";
}
