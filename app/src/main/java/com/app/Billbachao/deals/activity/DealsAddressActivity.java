package com.app.Billbachao.deals.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.deals.utils.DealsConstant;
import com.app.Billbachao.deals.utils.DealsValidationUtils;
import com.app.Billbachao.deals.i.IDealAddressCallback;
import com.app.Billbachao.deals.i.IDealsPincodeValidation;
import com.app.Billbachao.deals.models.DealsAddressModel;
import com.app.Billbachao.deals.tasks.DealsAddressCRUDTask;
import com.app.Billbachao.deals.tasks.DealsPinCodeFetchTask;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.volley.utils.VolleySingleTon;

/**
 * Deals Address Activity
 * Created by nitesh on 01/03/2016.
 */
public class DealsAddressActivity extends BaseActivity implements View.OnClickListener, IDealAddressCallback, IDealsPincodeValidation {

    private static final String TAG = DealsAddressActivity.class.getSimpleName();

    private Context mContext;
    private EditText nameEditText, mobileEditText, pincodeEditText, addressEditText, cityDistrictEditText, stateEditText, landmarkEditText;

    private TextInputLayout nameTextInput;
    private TextInputLayout pincodeTextInput;
    private TextInputLayout mobileTextInput;
    private TextInputLayout addressTextLayout;
    private TextInputLayout cityDistrictTextLayout;
    private TextInputLayout statesTextLayout;
    private TextInputLayout landmarkTextLayout;


    private String from;
    private DealsAddressModel dealsAddressModel;
    private int position;
    private String oldPincode = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        mContext = DealsAddressActivity.this;

        //init
        init();

        initData();

    }

    private void initData() {

        Intent intent = getIntent();

        if (intent != null) {
            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                from = bundle.getString(DealsConstant.KEY_FROM);

                if (from.equalsIgnoreCase(ApiUtils.UPDATE)) {
                    dealsAddressModel = bundle.getParcelable(DealsConstant.KEY_ADDRESS_MODEL);

                    //getolder pincode
                    oldPincode = dealsAddressModel.pincode;

                    updateAddressData();

                    setTitle(getString(R.string.deals_edit_address));
                } else {
                    mobileEditText.setText(PreferenceUtils.getMobileNumber(mContext));
                    setTitle(getString(R.string.deals_add_new_address));
                }
                position = bundle.getInt(DealsConstant.KEY_POSITION);
            }
        }
    }

    private void updateAddressData() {

        if (dealsAddressModel != null) {

            if (!DealsValidationUtils.isEmpty(dealsAddressModel.mobileNo))
                mobileEditText.setText(dealsAddressModel.mobileNo);

            if (!DealsValidationUtils.isEmpty(dealsAddressModel.firstname) && !DealsValidationUtils.isEmpty(dealsAddressModel.lastname))
                nameEditText.setText(dealsAddressModel.firstname + " " + dealsAddressModel.lastname);

            if (!DealsValidationUtils.isEmpty(dealsAddressModel.pincode))
                pincodeEditText.setText(dealsAddressModel.pincode);

            if (!DealsValidationUtils.isEmpty(dealsAddressModel.address))
                addressEditText.setText(dealsAddressModel.address);

            if (!DealsValidationUtils.isEmpty(dealsAddressModel.landmark))
                landmarkEditText.setText(dealsAddressModel.landmark);

            if (!DealsValidationUtils.isEmpty(dealsAddressModel.city))
                cityDistrictEditText.setText(dealsAddressModel.city);

            if (!DealsValidationUtils.isEmpty(dealsAddressModel.state))
                stateEditText.setText(dealsAddressModel.state);
        }
    }

    private void init() {
        mobileEditText = (EditText) findViewById(R.id.mobileEditText);
        nameEditText = (EditText) findViewById(R.id.nameEditText);
        pincodeEditText = (EditText) findViewById(R.id.pincodeEditText);
        addressEditText = (EditText) findViewById(R.id.addressEditText);
        landmarkEditText = (EditText) findViewById(R.id.landmarkEditText);

        cityDistrictEditText = (EditText) findViewById(R.id.cityDistrict);
        stateEditText = (EditText) findViewById(R.id.stateEditText);

        nameTextInput = (TextInputLayout) findViewById(R.id.nameTextInput);
        pincodeTextInput = (TextInputLayout) findViewById(R.id.pincodeTextInput);
        mobileTextInput = (TextInputLayout) findViewById(R.id.mobileTextInput);
        addressTextLayout = (TextInputLayout) findViewById(R.id.addressTextLayout);
        landmarkTextLayout = (TextInputLayout) findViewById(R.id.landmarkTextLayout);
        cityDistrictTextLayout = (TextInputLayout) findViewById(R.id.cityDistrictTextLayout);
        statesTextLayout = (TextInputLayout) findViewById(R.id.statesTextLayout);

        Button saveButton = (Button) findViewById(R.id.saveAddress);
        saveButton.setOnClickListener(this);

        pincodeEditText.addTextChangedListener(new TextWatcher() {

            String previousPincode = "";

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String pincode = s.toString().trim();

                if (!TextUtils.isEmpty(pincode) && pincode.length() == 6) {

                    if (!previousPincode.equalsIgnoreCase(pincode)) {
                        previousPincode = pincode;
                        fetchAddressInfoByPincode(pincode);
                    }

                }
            }
        });


        nameEditText.setFilters(new InputFilter[]{
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence cs, int start,
                                               int end, Spanned spanned, int dStart, int dEnd) {
                        if (cs.equals("")) { // for backspace
                            return cs;
                        }
                        if (cs.toString().matches("[a-zA-Z ]+")) {
                            return cs;
                        }
                        return "";
                    }
                }
        });
    }

    private void fetchAddressInfoByPincode(String pincode) {
        DealsPinCodeFetchTask fetchTask = new DealsPinCodeFetchTask(mContext, this);
        fetchTask.fetchPinCodeDetails(pincode, TAG);
    }

    @Override
    protected boolean isCartItemSupported() {
        return false;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.saveAddress:
                saveAddress();
                break;
        }
    }

    private void saveAddress() {

        DealsValidationUtils validationUtils = new DealsValidationUtils(mContext);

        String pincode = pincodeEditText.getText().toString();

        if (validationUtils.validatePincode(pincode, pincodeTextInput)) {
            String name = nameEditText.getText().toString();

            if (validationUtils.validateName(name, nameTextInput)) {

                String spiltArray[] = validationUtils.validateFirstLastName(name, nameTextInput);

                if (spiltArray != null) {
                    String mobile = mobileEditText.getText().toString();

                    if (validationUtils.validatePhone(mobile, mobileTextInput)) {

                        String address = addressEditText.getText().toString();

                        if (validationUtils.validateAddress(address, addressTextLayout)) {

                            String landmark = landmarkEditText.getText().toString();

                            if (validationUtils.validateLandmark(landmark, landmarkTextLayout)) {

                                String cityDistrict = cityDistrictEditText.getText().toString();
                                if (validationUtils.validateCityDistrict(cityDistrict, cityDistrictTextLayout)) {
                                    String state = stateEditText.getText().toString();

                                    if (validationUtils.validateState(state, statesTextLayout)) {

                                        dealsAddressModel = new DealsAddressModel(pincode, spiltArray[0], spiltArray[1], address, mobile, cityDistrict, state, landmark, ApiUtils.NA, ApiUtils.NA);

                                        //for insert oldpincode is same as new pincode.
                                        if (from.equalsIgnoreCase(ApiUtils.INSERT)) {
                                            oldPincode = pincode;
                                        }

                                        new DealsAddressCRUDTask(mContext, position).performCRUDOperation(from, oldPincode, dealsAddressModel, this, TAG);
                                    }
                                }
                            }
                        }

                    }
                }


            }
        }
    }


    @Override
    public void onSuccessAddress(String from, String desc, int position) {

        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString(DealsConstant.KEY_FROM, from);
        bundle.putParcelable(DealsConstant.KEY_ADDRESS_MODEL, dealsAddressModel);
        intent.putExtras(bundle);

        setResult(Activity.RESULT_OK, intent);
        showToast(desc);

        finish();
    }

    @Override
    public void onErrorAddress(String from, String error) {
        showToast(error);
    }


    @Override
    public void onSuccessPincode(DealsAddressModel dealsAddressModel) {

        updateCityState();

        this.dealsAddressModel = dealsAddressModel;

        if (dealsAddressModel != null) {
            pincodeTextInput.setErrorEnabled(false);
            updateAddressData();
        }
    }

    @Override
    public void onErrorPincode(String error) {

        if (pincodeTextInput.getChildCount() == 2)
            pincodeTextInput.getChildAt(1).setVisibility(View.VISIBLE);

        pincodeTextInput.setError(error);

        updateCityState();
    }

    private void updateCityState() {
        stateEditText.setText("");
        cityDistrictEditText.setText("");

        statesTextLayout.setErrorEnabled(false);
        cityDistrictTextLayout.setErrorEnabled(false);
    }

    @Override
    protected void onDestroy() {

        VolleySingleTon.getInstance(mContext).cancelPendingRequests(TAG);
        super.onDestroy();
    }
}