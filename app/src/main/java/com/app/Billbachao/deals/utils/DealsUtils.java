package com.app.Billbachao.deals.utils;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.Arrays;
import java.util.List;

/**
 * Created by nitesh on 25/02/2016.
 */
public class DealsUtils {

    public static List<String> getListByComma(String value){
        return Arrays.asList(value.trim().split("\\s*,\\s*"));
    }

    /*public static Bundle prepareBundle(Object object, String from) {

        Bundle bundle = new Bundle();
        bundle.putString(DealsConstant.KEY_FROM, from);

        if (object instanceof GenericTvModel){
            bundle.putSerializable(DealsConstant.KEY_RELIANCE_DETAILS, (GenericTvModel)object);
        }else if (object instanceof TvProductInformation){
            bundle.putSerializable(DealsConstant.KEY_RELIANCE_DETAILS, (TvProductInformation)object);
        }else if (object instanceof TvDisplay){
            bundle.putSerializable(DealsConstant.KEY_RELIANCE_DETAILS, (TvDisplay)object);
        }else if (object instanceof TvAudio){
            bundle.putSerializable(DealsConstant.KEY_RELIANCE_DETAILS, (TvAudio)object);
        }else if (object instanceof TvConntectivity){
            bundle.putSerializable(DealsConstant.KEY_RELIANCE_DETAILS, (TvConntectivity)object);
        }else if (object instanceof TvPhysicalDimensions){
            bundle.putSerializable(DealsConstant.KEY_RELIANCE_DETAILS, (TvPhysicalDimensions)object);
        }else if (object instanceof TvPowerConsumption){
            bundle.putSerializable(DealsConstant.KEY_RELIANCE_DETAILS, (TvPowerConsumption)object);
        }else if (object instanceof TvEnvironmentalParameters){
            bundle.putSerializable(DealsConstant.KEY_RELIANCE_DETAILS, (TvEnvironmentalParameters)object);
        }else if (object instanceof TvAdditionalFeatures){
            bundle.putSerializable(DealsConstant.KEY_RELIANCE_DETAILS, (TvAdditionalFeatures)object);
        }else if (object instanceof TvInBoxWarranty){
            bundle.putSerializable(DealsConstant.KEY_RELIANCE_DETAILS, (TvInBoxWarranty)object);
        }else if (object instanceof TvOverView){
            bundle.putSerializable(DealsConstant.KEY_RELIANCE_DETAILS, (TvOverView)object);
        }

        return bundle;
    }*/


    public static void hideSoftKey(FragmentActivity activity) {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

            if (imm != null)
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void showKeyboard(FragmentActivity context) {
        // Check if no view has focus:
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

            if (inputManager != null)
                inputManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }
}
