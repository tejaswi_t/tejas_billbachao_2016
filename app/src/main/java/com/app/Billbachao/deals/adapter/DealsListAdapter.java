package com.app.Billbachao.deals.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.deals.models.RelianceTvDetailsModel;
import com.app.Billbachao.deals.models.TvProductInformation;
import com.squareup.picasso.Picasso;
import java.util.List;

/**
 * Deals List adapter
 * Created by nitesh on 24/02/2015.
 */
public class DealsListAdapter extends RecyclerView.Adapter<DealsListAdapter.DealViewHolder> {

    private Context context;

    private List<RelianceTvDetailsModel> items;

    private IDealItemClick mDealsItemClickListener;

    public DealsListAdapter(Context context, List<RelianceTvDetailsModel> items) {
        this.context = context;
        this.items = items;
    }


    @Override
    public DealViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.deals_television_detail_view, parent, false);
        return new DealViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DealViewHolder viewHolder, int position) {

        final RelianceTvDetailsModel handsetSpecification = items.get(position);

        TvProductInformation tvProductInformation = handsetSpecification.productInformation;

        if (tvProductInformation != null) {
            viewHolder.tvHandsetName.setText(tvProductInformation.productName);
            viewHolder.tvHandsetBrand.setText("by " + tvProductInformation.brand);

            if (!tvProductInformation.cost.equals("0")) {
                viewHolder.tvHandsetPrice.setVisibility(View.VISIBLE);
                if (!tvProductInformation.offerCost.equals("0") && !tvProductInformation.offerCost.equalsIgnoreCase(ApiUtils.NA)) {
                    viewHolder.tvHandsetPrice.setText("MRP:" + " " + context.getString(R.string.inr_n, tvProductInformation.cost), TextView.BufferType.SPANNABLE);
                    Spannable spannable = (Spannable) viewHolder.tvHandsetPrice.getText();
                    spannable.setSpan(new StrikethroughSpan(), 4, viewHolder.tvHandsetPrice.getText().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                } else {
                    viewHolder.tvHandsetPrice.setText("MRP:" + " " + context.getString(R.string.inr_n, tvProductInformation.cost));
                }
            } else {
                viewHolder.tvHandsetPrice.setVisibility(View.GONE);
            }

            if (!tvProductInformation.offerCost.equals("0") && !tvProductInformation.offerCost.equalsIgnoreCase(ApiUtils.NA)) {
                viewHolder.tvHandsetOfferPrice.setText("Price:" + " " + context.getString(R.string.inr_n, tvProductInformation.offerCost));
                viewHolder.tvHandsetOfferPrice.setVisibility(View.VISIBLE);
            } else {
                viewHolder.tvHandsetOfferPrice.setVisibility(View.GONE);
            }


            if (!TextUtils.isEmpty(tvProductInformation.imageUrl)) {
                viewHolder.ivHandsetIcon.setVisibility(View.VISIBLE);
                Picasso.with(context)
                        .load(tvProductInformation.imageUrl.replace("\\", "").trim())
                        .noFade()
                        .fit()
                        .centerInside()
                        .into(viewHolder.ivHandsetIcon);
            } else {
                viewHolder.ivHandsetIcon.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0 ;
    }

    public class DealViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView ivHandsetIcon;
        private TextView tvHandsetName;
        private TextView tvHandsetBrand;
        private TextView tvHandsetPrice;
        private TextView tvHandsetOfferPrice;

        public DealViewHolder(View convertView) {
            super(convertView);
            ivHandsetIcon = (ImageView) convertView.findViewById(R.id.img_handset_icon);
            tvHandsetName = (TextView) convertView.findViewById(R.id.tv_handset_name);
            tvHandsetBrand = (TextView) convertView.findViewById(R.id.tv_handset_brand);
            tvHandsetPrice = (TextView) convertView.findViewById(R.id.tv_handset_price);
            tvHandsetOfferPrice = (TextView) convertView.findViewById(R.id.tv_handset_offer_price);

            convertView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mDealsItemClickListener != null) {
                mDealsItemClickListener.itemClick(getAdapterPosition(), view, items.get(getAdapterPosition()));
            }
        }
    }

    public interface IDealItemClick {
        void itemClick(int position, View view, RelianceTvDetailsModel relianceTvDetailsModel);
    }

    public void setDealsItemClickListener(IDealItemClick mDealsItemClickListener) {
        this.mDealsItemClickListener = mDealsItemClickListener;
    }
}