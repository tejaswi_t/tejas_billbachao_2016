package com.app.Billbachao.deals.utils;

import android.content.Context;
import android.text.TextUtils;

import com.app.Billbachao.R;
import com.app.Billbachao.deals.models.GenericTvModel;
import com.app.Billbachao.deals.models.SpecificationModel;
import com.app.Billbachao.deals.models.TvProductInformation;
import com.app.Billbachao.utils.ILog;

import java.util.List;

/**
 * Created by nitesh on 25/02/2016.
 */
public class InsertSpecificationData {

    private static final String TAG = InsertSpecificationData.class.getName();
    private final Context mContext;

    public InsertSpecificationData(Context mContext) {
        this.mContext = mContext;
    }

    public void fillGenericInfo(List<GenericTvModel> genericTvModelList, List<SpecificationModel> specificationModelList) {

        for (GenericTvModel genericTvModel : genericTvModelList) {

            String key = genericTvModel.key;
            String value = genericTvModel.value;

            if (!TextUtils.isEmpty(key) && !TextUtils.isEmpty(value)) {

                if (key.equalsIgnoreCase(TvProductInformation.PROD_BOOK_WITH_PAYMENT)) {

                    try {
                        int valueInteger = Integer.valueOf(value);

                        if (valueInteger > 0) {
                            value = String.format(mContext.getString(R.string.rs_n), valueInteger);
                            specificationModelList.add(new SpecificationModel(false, "", key, value));
                            continue;
                        }
                    } catch (NumberFormatException e) {
                        ILog.d(TAG, "NumberFormateException " + e.getMessage());
                    }
                }


                if (!key.equalsIgnoreCase(TvProductInformation.PROD_BOOK_WITH_PAYMENT) && !key.equalsIgnoreCase(TvProductInformation.PROD_URL))
                    specificationModelList.add(new SpecificationModel(false, "", key, value));

            }
        }
    }
}
