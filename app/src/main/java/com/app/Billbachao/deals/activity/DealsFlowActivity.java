package com.app.Billbachao.deals.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.deals.adapter.AddressListAdapter;
import com.app.Billbachao.deals.utils.DealsConstant;
import com.app.Billbachao.deals.fragment.DealsAddressListingFragment;
import com.app.Billbachao.deals.i.IDealAddressCallback;
import com.app.Billbachao.deals.i.IDealAddressListingCallback;
import com.app.Billbachao.deals.models.DealsBaseAddressModel;
import com.app.Billbachao.deals.models.DealsAddressModel;
import com.app.Billbachao.deals.models.RelianceTvDetailsModel;
import com.app.Billbachao.apis.deals.DealsAddressApi;
import com.app.Billbachao.deals.tasks.DealsAddressCRUDTask;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import java.util.ArrayList;

public class DealsFlowActivity extends BaseActivity implements IDealAddressListingCallback, IDealAddressCallback, AddressListAdapter.IDealsDeleteAddress {

    private static final String TAG = DealsFlowActivity.class.getSimpleName();
    private FrameLayout deals_flow_container;
    private View step2CompletedView;
    private View step1CompletedView;

    private TextView step1DescTv;
    private TextView step2DescTv;
    private TextView step3DescTv;

    private TextView step1IndicatorTv;
    private TextView step2IndicatorTv;
    private TextView step3IndicatorTv;
    private Context mContext;
    private DealsAddressListingFragment dealsAddressFragment;
    private RelianceTvDetailsModel relianceTvDetailsModel;
    private String from;
    private boolean isBooking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deals_flow);

        mContext = DealsFlowActivity.this;

        initView();

        initData();

        makeNetworkCall();

    }

    private void makeNetworkCall() {
        NetworkGsonRequest<DealsBaseAddressModel> request = new NetworkGsonRequest<>(Request.Method.POST, DealsAddressApi.URL_SHIPPING, DealsBaseAddressModel.class, DealsAddressApi.getDealsAddressByMobile(mContext), new Response.Listener<DealsBaseAddressModel>() {
            @Override
            public void onResponse(DealsBaseAddressModel response) {

                if (response != null && response.status.equalsIgnoreCase(ApiUtils.SUCCESS)){
                    onSuccessAddressListing(response.dealsAddressModels);
                }else {
                    onErrorAddressListing();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showSnack(NetworkErrorHelper.getErrorStatus(error).getErrorMessage());
                onErrorAddressListing();
            }
        }, true);

        VolleySingleTon.getInstance(mContext).addToRequestQueue(request, TAG);
    }

    private void initData() {
        Intent intent = getIntent();

        if (intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                relianceTvDetailsModel = bundle.getParcelable(DealsConstant.KEY_RELIANCE_DETAILS);
                from = bundle.getString(DealsConstant.KEY_FROM);
                isBooking = bundle.getBoolean(DealsConstant.KEY_IS_USE_BOOKING);
            }
        }
    }

    private void addAddAddressListingFragment(ArrayList<DealsAddressModel> dealsAddressModels) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(DealsConstant.KEY_ADDRESS_LIST, dealsAddressModels);
        bundle.putParcelable(DealsConstant.KEY_RELIANCE_DETAILS, relianceTvDetailsModel);
        bundle.putString(DealsConstant.KEY_FROM, from);
        bundle.putBoolean(DealsConstant.KEY_IS_USE_BOOKING, isBooking);

        dealsAddressFragment = (DealsAddressListingFragment) Fragment.instantiate(mContext, DealsAddressListingFragment.class.getName(), bundle);
        fragmentTransaction.replace(deals_flow_container.getId(), dealsAddressFragment).commit();
    }

    private void initView() {

        deals_flow_container = (FrameLayout) findViewById(R.id.deals_flow_container);

        step1CompletedView = findViewById(R.id.step1CompletedView);
        step2CompletedView = findViewById(R.id.step2CompletedView);

        step1DescTv = (TextView) findViewById(R.id.step1DescTv);
        step2DescTv = (TextView) findViewById(R.id.step2DescTv);
        step3DescTv = (TextView) findViewById(R.id.step3DescTv);

        step1IndicatorTv = (TextView) findViewById(R.id.step1IndicatorTv);
        step2IndicatorTv = (TextView) findViewById(R.id.step2IndicatorTv);
        step3IndicatorTv = (TextView) findViewById(R.id.step3IndicatorTv);

        changeSelectedSteps(true, false);

        step1CompletedView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));

    }


    private void changeSelectedSteps(boolean isStep1Completed, boolean isStep12Completed) {

        Drawable drawableSelected = ContextCompat.getDrawable(mContext, R.drawable.deals_steps_background_selected);
        Drawable drawableUnSelected = ContextCompat.getDrawable(mContext, R.drawable.deals_steps_background_unselected);

        int secondaryTextColorSelected = ContextCompat.getColor(mContext, R.color.colorAccent);
        int secondaryTextColorUnSelected = ContextCompat.getColor(mContext, R.color.textSecondaryColor);


        if (isStep1Completed) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                step1IndicatorTv.setBackground(drawableSelected);
                step2IndicatorTv.setBackground(drawableSelected);
                step3IndicatorTv.setBackground(drawableUnSelected);
            } else {
                step1IndicatorTv.setBackgroundDrawable(drawableSelected);
                step2IndicatorTv.setBackgroundDrawable(drawableSelected);
                step3IndicatorTv.setBackgroundDrawable(drawableUnSelected);
            }


            step1CompletedView.setBackgroundColor(secondaryTextColorSelected);
            step2CompletedView.setBackgroundColor(secondaryTextColorUnSelected);

            step1DescTv.setTextColor(secondaryTextColorSelected);
            step2DescTv.setTextColor(secondaryTextColorSelected);
            step3DescTv.setTextColor(secondaryTextColorUnSelected);

        } else if (isStep12Completed) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                step1IndicatorTv.setBackground(drawableSelected);
                step2IndicatorTv.setBackground(drawableSelected);
                step3IndicatorTv.setBackground(drawableSelected);
            } else {
                step1IndicatorTv.setBackgroundDrawable(drawableSelected);
                step2IndicatorTv.setBackgroundDrawable(drawableSelected);
                step3IndicatorTv.setBackgroundDrawable(drawableSelected);
            }


            step1CompletedView.setBackgroundColor(secondaryTextColorSelected);
            step2CompletedView.setBackgroundColor(secondaryTextColorSelected);

            step1DescTv.setTextColor(secondaryTextColorSelected);
            step2DescTv.setTextColor(secondaryTextColorSelected);
            step3DescTv.setTextColor(secondaryTextColorSelected);
        }
    }

    @Override
    public void onSuccessAddressListing(ArrayList<DealsAddressModel> dealsAddressModel) {
        addAddAddressListingFragment(dealsAddressModel);
    }

    @Override
    public void onErrorAddressListing() {
        addAddAddressListingFragment(null);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {

            if (fragment != null && fragment instanceof DealsAddressListingFragment) {
                fragment.onActivityResult(requestCode, resultCode, data);
                break;
            }
        }
    }

    @Override
    public void onSuccessAddress(String from, String desc, int position) {

        dismissProgressDialog();
        if (dealsAddressFragment != null) {
            dealsAddressFragment.onSuccessAddress(from, desc, position);
        }
    }

    @Override
    public void onErrorAddress(String from, String error) {

        dismissProgressDialog();

        if (dealsAddressFragment != null) {
            dealsAddressFragment.onErrorAddress(from, error);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        getString(R.string.deals);
    }


    @Override
    public void deleteDealsAddress(int position, DealsAddressModel dealsAddressModel) {
        showProgressDialog(getString(R.string.deals_deleting_address));
        new DealsAddressCRUDTask(mContext, position).performCRUDOperation(ApiUtils.DELETE,  ApiUtils.NA,dealsAddressModel, (IDealAddressCallback) mContext, DealsAddressListingFragment.TAG);
    }
}
