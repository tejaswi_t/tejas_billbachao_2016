package com.app.Billbachao.deals.utils;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;

import com.app.Billbachao.R;

/**
 * Created by nitesh on 01/03/2016.
 */
public class DealsValidationUtils {

    private Context mContext;

    public DealsValidationUtils(Context mContext) {
        this.mContext = mContext;
    }

    public boolean validateName(String nameVal, TextInputLayout nameTextLayout) {

        if (isEmpty(nameVal)) {
            if (nameTextLayout.getChildCount() == 2)
                nameTextLayout.getChildAt(1).setVisibility(View.VISIBLE);

            nameTextLayout.setError(mContext.getString(R.string.msg_enter_name));
            return false;
        } else {
            nameTextLayout.setErrorEnabled(false);
        }
        return true;
    }

    public String[] validateFirstLastName(String nameVal, TextInputLayout textInputLayout) {

        String spiltArray[] = nameVal.split(" ");

        if (spiltArray != null && spiltArray.length == 2) {
            textInputLayout.setErrorEnabled(false);
            return spiltArray;
        } else {
            if (textInputLayout.getChildCount() == 2)
                textInputLayout.getChildAt(1).setVisibility(View.VISIBLE);

            textInputLayout.setError(mContext.getString(R.string.msg_enter_name));
        }

        return null;
    }

    public boolean validatePhone(String mobileVal, TextInputLayout addressPhoneInputTextLayout) {

        if (isEmpty(mobileVal)) {

            if (addressPhoneInputTextLayout.getChildCount() == 2)
                addressPhoneInputTextLayout.getChildAt(1).setVisibility(View.VISIBLE);

            addressPhoneInputTextLayout.setError(mContext.getString(R.string.msg_enter_mobile_no));

            return false;
        } else if (mobileVal.length() != 10) {

            if (addressPhoneInputTextLayout.getChildCount() == 2)
                addressPhoneInputTextLayout.getChildAt(1).setVisibility(View.VISIBLE);

            addressPhoneInputTextLayout.setError(mContext.getString(R.string.msg_enter_valid_mobile_no));
            return false;
        } else {
            addressPhoneInputTextLayout.setErrorEnabled(false);
        }

        return true;
    }


    public boolean validateAddress(String addressVal, TextInputLayout addressTextLayout) {

        if (isEmpty(addressVal)) {
            if (addressTextLayout.getChildCount() == 2)
                addressTextLayout.getChildAt(1).setVisibility(View.VISIBLE);

            addressTextLayout.setError(mContext.getString(R.string.msg_enter_address));
            return false;
        } else {
            addressTextLayout.setErrorEnabled(false);
        }
        return true;
    }

    public boolean validateLandmark(String landmarkVal, TextInputLayout addressTextLayout) {

        if (isEmpty(landmarkVal)) {
            if (addressTextLayout.getChildCount() == 2)
                addressTextLayout.getChildAt(1).setVisibility(View.VISIBLE);

            addressTextLayout.setError(mContext.getString(R.string.msg_enter_address));
            return false;
        } else {
            addressTextLayout.setErrorEnabled(false);
        }
        return true;
    }


    public boolean validateLocationTown(String locationTownVal, TextInputLayout locationTextLayout) {

        if (isEmpty(locationTownVal)) {
            if (locationTextLayout.getChildCount() == 2)
                locationTextLayout.getChildAt(1).setVisibility(View.VISIBLE);

            locationTextLayout.setError(mContext.getString(R.string.msg_enter_location));
            return false;
        } else {
            locationTextLayout.setErrorEnabled(false);
        }
        return true;
    }

    public boolean validateCityDistrict(String cityDistrict, TextInputLayout cityDistrictTextLayout) {

        if (isEmpty(cityDistrict)) {
            if (cityDistrictTextLayout.getChildCount() == 2)
                cityDistrictTextLayout.getChildAt(1).setVisibility(View.VISIBLE);

            cityDistrictTextLayout.setError(mContext.getString(R.string.msg_enter_city_district));
            return false;
        } else {
            cityDistrictTextLayout.setErrorEnabled(false);
        }
        return true;
    }

    public boolean validateState(String cityDistrict, TextInputLayout stateTextLayout) {

        if (isEmpty(cityDistrict)) {
            if (stateTextLayout.getChildCount() == 2)
                stateTextLayout.getChildAt(1).setVisibility(View.VISIBLE);

            stateTextLayout.setError(mContext.getString(R.string.msg_enter_state));
            return false;
        } else {
            stateTextLayout.setErrorEnabled(false);
        }
        return true;
    }

    public boolean validatePincode(String pincodeVal, TextInputLayout pincodeTextLayout) {

        if (isEmpty(pincodeVal)) {

            if (pincodeTextLayout.getChildCount() == 2)
                pincodeTextLayout.getChildAt(1).setVisibility(View.VISIBLE);

            pincodeTextLayout.setError(mContext.getResources().getString(R.string.msg_enter_pincode_no));

            return false;
        } else if (pincodeVal.length() != 6) {

            if (pincodeTextLayout.getChildCount() == 2)
                pincodeTextLayout.getChildAt(1).setVisibility(View.VISIBLE);

            pincodeTextLayout.setError(mContext.getString(R.string.msg_enter_pincode_no));
            return false;
        } else {
            pincodeTextLayout.setErrorEnabled(false);
        }

        return true;
    }

    public static boolean isEmpty(String val) {

        return TextUtils.isEmpty(val) || val.equalsIgnoreCase("null") || val.equalsIgnoreCase("na");

    }
}
