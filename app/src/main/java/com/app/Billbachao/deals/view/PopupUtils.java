package com.app.Billbachao.deals.view;

import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListPopupWindow;

/**
 * Created by nitesh on 09/03/2016.
 */
public class PopupUtils {

    public static ListPopupWindow showPopUp(Context mContext, View anchor, ArrayAdapter<String> arrayAdapter) {
        return show(mContext, anchor, arrayAdapter);
    }

    /**
     * show popup window method return PopupWindow
     */
    private static ListPopupWindow show(Context mContext, View anchor, ArrayAdapter<String> adapter) {

        ListPopupWindow popup = new ListPopupWindow(mContext);
        popup.setModal(true);
        popup.setAdapter(adapter);
        popup.setAnchorView(anchor);
        popup.setWidth(250);
        popup.setContentWidth(250);
        popup.setHeight(ListPopupWindow.WRAP_CONTENT);


        return popup;
    }

}
