package com.app.Billbachao.deals.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.Billbachao.model.BaseGsonResponse;
import com.google.myjson.annotations.SerializedName;

/**
 * DealsAddressPincodeModel
 * Created by nitesh on 10-05-2016.
 */
public class DealsBaseAddressPincodeModel extends BaseGsonResponse implements Parcelable {

    @SerializedName("pincodeDetails")
    public DealsAddressModel dealsAddressModel;

    protected DealsBaseAddressPincodeModel(Parcel in) {
        dealsAddressModel = in.readParcelable(DealsAddressModel.class.getClassLoader());
    }

    public static final Creator<DealsBaseAddressPincodeModel> CREATOR = new Creator<DealsBaseAddressPincodeModel>() {
        @Override
        public DealsBaseAddressPincodeModel createFromParcel(Parcel in) {
            return new DealsBaseAddressPincodeModel(in);
        }

        @Override
        public DealsBaseAddressPincodeModel[] newArray(int size) {
            return new DealsBaseAddressPincodeModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(dealsAddressModel, flags);
    }
}
