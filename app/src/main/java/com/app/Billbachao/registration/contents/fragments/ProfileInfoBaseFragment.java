package com.app.Billbachao.registration.contents.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.apis.MobileVerificationApi;
import com.app.Billbachao.database.MobileOperatorDBHelper;
import com.app.Billbachao.database.OperatorCircleNetworkMappingDBHelper;
import com.app.Billbachao.dualsim.telephony.DualSimManager;
import com.app.Billbachao.dualsim.telephony.TelephonyInfo;
import com.app.Billbachao.externalsdk.AppsFlyerUtils;
import com.app.Billbachao.model.DualSIMBean;
import com.app.Billbachao.model.OperatorCircleInfo;
import com.app.Billbachao.utils.CircleUtils;
import com.app.Billbachao.utils.MailUtils;
import com.app.Billbachao.utils.NumberUtils;
import com.app.Billbachao.utils.OperatorUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.utils.USSDUtils;
import com.app.Billbachao.volley.request.NetworkStringRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Abstract class for functionality common to {@link RegistrationFragment}
 * Created by mihir on 11-03-2016.
 */
public abstract class ProfileInfoBaseFragment extends Fragment {

    public static final String TAG = RegistrationFragment.class.getSimpleName();

    View mRootView;

    TextView mheadingText, mtermsConditionText;

    EditText mNumberText, mMailIdText, mOperatorText, mCircleText;

    RadioGroup mPrepaidPostpaidGroup, mDataTypeGroup;

    Button mConfirmButton;

    String mMobileNumber, mCircleId, mOperatorId, mEmailId, mConnectionType, mNetworkType;

    String[] networkTypes = new String[]{PreferenceUtils.NETWORK_4G, PreferenceUtils.NETWORK_2G, PreferenceUtils.NETWORK_3G};

    int networkRadioIds[] = new int[]{R.id.type_4g, R.id.type_2g, R.id.type_3g};

    public static final String UPDATE_PROFILE = "UPDATEPROFILE", CREATE_USER = "CREATEUSER", UPDATE_USER
            = "UPDATEUSER";

    public static final String IMEI = "imei", IMSI = "imsi", NETCODE = "netCode", OPNAME = "opName", OPID = "opID", SIM_STATUS = "status", BB_USE = "bbUse", OS_VERSION = "osVersion", MANUFACTURER = "make", DEVICE_MODEL = "model";

    public ProgressDialog mProgressDialog;

    public static final String progressMsg = "Please wait...";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_registration, container, false);
        initView();
        return mRootView;
    }

    protected void initView() {
        mheadingText = (TextView) mRootView.findViewById(R.id.heading);
        mNumberText = (EditText) mRootView.findViewById(R.id.mobile_number);
        mNumberText.addTextChangedListener(mNumberTextWatcher);
        mOperatorText = (EditText) mRootView.findViewById(R.id.operator);
        mCircleText = (EditText) mRootView.findViewById(R.id.circle);
        mMailIdText = (EditText) mRootView.findViewById(R.id.mail_id);
        mPrepaidPostpaidGroup = (RadioGroup) mRootView.findViewById(R.id.prepaid_postpaid_group);
        mDataTypeGroup = (RadioGroup) mRootView.findViewById(R.id.data_type_group);
        mConfirmButton = (Button) mRootView.findViewById(R.id.get_started);
        mOperatorText.setOnClickListener(mClickListener);
        mCircleText.setOnClickListener(mClickListener);
        mConfirmButton.setOnClickListener(mClickListener);
        mtermsConditionText = (TextView) mRootView.findViewById(R.id.terms_condition);
        mtermsConditionText.setMovementMethod(LinkMovementMethod.getInstance());
        mtermsConditionText.setText(Html.fromHtml(getString(R.string.terms_condition)));
    }

    TextWatcher mNumberTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String number = s.toString();
            if (s.length() == 0)
                return;
            if (!NumberUtils.isValidNumber(number, true)) {
                mNumberText.setText("");
                ((BaseActivity) getActivity()).showToast(R.string.wrong_mobile_number_validation_msg);
            } else if (number.length() == 10) {
                // Take action
                checkForInitialEntry();
                updateCircleOperatorData();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /**
     * Empty function here, will be overriden for {@link RegistrationFragment} case
     */
    protected void checkForInitialEntry() {

    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.circle:
                    showCircleOptions();
                    break;
                case R.id.operator:
                    showOperatorOptions();
                    break;
                case R.id.get_started:
                    if (isDataValid()) {
                        onConfirmClicked();
                    }
                    break;
            }
        }
    };

    protected abstract void onConfirmClicked();

    protected void updateCircleOperatorData() {
        mNumberText.clearFocus();
        String mobile = mNumberText.getText().toString();
        MobileOperatorDBHelper operatorDBHelper = MobileOperatorDBHelper.getInstance(getActivity());
        OperatorCircleInfo info = operatorDBHelper.getOperatorCircleInfo(mobile);

        mCircleId = String.valueOf(info.getCircleId());
        if (info.getCircleId() > 0) {
            mCircleText.setText(CircleUtils.getCircleNameFromId(mCircleId));
        }
        mOperatorId = String.valueOf(info.getOperatorId());
        if (info.getOperatorId() > 0) {
            mOperatorText.setText(OperatorUtils.getOperatorNameFromId(String.valueOf(mOperatorId)));
        }
        updateNetworkType();
    }

    void updateNetworkType() {
        OperatorCircleNetworkMappingDBHelper operatorCircleNetworkMappingDBHelper = OperatorCircleNetworkMappingDBHelper.getInstance(getActivity());
        ArrayList<String> networkList = operatorCircleNetworkMappingDBHelper.getNetworkList(String.valueOf(mOperatorId), String.valueOf(mCircleId));

        int validId = -1;
        mDataTypeGroup.clearCheck();
        for (int i = 0; i < networkTypes.length; i++) {
            String networkType = networkTypes[i];
            RadioButton radioButton = (RadioButton) mDataTypeGroup.findViewById(networkRadioIds[i]);
            if (networkList.contains(networkType)) {
                validId = networkRadioIds[i];
                radioButton.setEnabled(true);
            } else {
                radioButton.setEnabled(false);
            }
        }
        if (validId > -1) {
            RadioButton radioButton = (RadioButton) mDataTypeGroup.findViewById(validId);
            radioButton.setChecked(true);
        }
    }

    private void showCircleOptions() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(CircleUtils.sCircleNames, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mCircleId = CircleUtils.sCircleId[which];
                mCircleText.setText(CircleUtils.sCircleNames[which]);
                updateNetworkType();
            }
        });
        builder.show();
    }

    private void showOperatorOptions() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(OperatorUtils.sOperatorNamesSorted, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mOperatorId = OperatorUtils.sOperatorIdSorted[which];
                mOperatorText.setText(OperatorUtils.sOperatorNamesSorted[which]);
                updateNetworkType();
            }
        });
        builder.show();
    }

    private boolean isDataValid() {
        String mobile = mNumberText.getText().toString();
        if (!NumberUtils.isValidNumber(mobile)) {
            ((BaseActivity) getActivity()).showSnack(R.string.wrong_mobile_number_validation_msg);
            return false;
        }
        if (!CircleUtils.isCircleIdMatchFound(mCircleId)) {
            ((BaseActivity) getActivity()).showSnack(R.string.no_circle_validation_msg);
            return false;
        }
        if (!OperatorUtils.isOperatorIdMatchFound(mOperatorId)) {
            ((BaseActivity) getActivity()).showSnack(R.string.no_operator_validation_msg);
            return false;
        }
        if (mDataTypeGroup.getCheckedRadioButtonId() < 0) {
            ((BaseActivity) getActivity()).showSnack(R.string.wrong_circle_operator_selection);
            return false;
        }
        String mail = mMailIdText.getText().toString();
        if (!MailUtils.isValidMail(mail)) {
            ((BaseActivity) getActivity()).showSnack(R.string.email_validation_msg);
            return false;
        }
        return true;
    }

    void saveProfileInfo() {
        String mobile = mNumberText.getText().toString();
        String mail = mMailIdText.getText().toString();
        boolean isPrepaid = mPrepaidPostpaidGroup.getCheckedRadioButtonId() == R.id.prepaid;
        String dataType = (String) (mDataTypeGroup.findViewById(mDataTypeGroup.getCheckedRadioButtonId())).getTag();
        PreferenceUtils.saveProfileInfo(getActivity(), mobile, mOperatorId, mCircleId, mail, isPrepaid, dataType);
        AppsFlyerUtils.trackEvent(getActivity(), AppsFlyerUtils.REGISTERED);
    }

    boolean isUserDataUpdated() {
        mConnectionType = mPrepaidPostpaidGroup.getCheckedRadioButtonId() == R.id.prepaid ? PreferenceUtils.PREPAID : PreferenceUtils.POSTPAID;
        mNetworkType = (String) (mDataTypeGroup.findViewById(mDataTypeGroup.getCheckedRadioButtonId())).getTag();
        if ((!mNumberText.getText().toString().trim().equals(PreferenceUtils.getMobileNumber(getActivity()))
                || !OperatorUtils.getOperatorIdFromName(mOperatorText.getText().toString()).equals(PreferenceUtils.getOperatorId(getActivity()))
                || !CircleUtils.getCircleIdFromName(mCircleText.getText().toString()).equals(PreferenceUtils.getCircleId(getActivity()))
                || !mConnectionType.equalsIgnoreCase(PreferenceUtils.getCType(getActivity()))
                || !mMailIdText.getText().toString().equalsIgnoreCase(PreferenceUtils.getMailId(getActivity())))
                || !mNetworkType.equalsIgnoreCase(PreferenceUtils.getNetworkType(getActivity()))) {
            return true;
        }
        return false;
    }

    protected void requestVerificationOnServer() {
        final String mobile = mNumberText.getText().toString();
        final String mail = mMailIdText.getText().toString();
        final String dataType = (String) (mDataTypeGroup.findViewById(mDataTypeGroup.getCheckedRadioButtonId())).getTag();
        final String connectionType = mPrepaidPostpaidGroup.getCheckedRadioButtonId() == R.id.prepaid ? PreferenceUtils.PREPAID : PreferenceUtils.POSTPAID;

        NetworkStringRequest stringRequest = new NetworkStringRequest(Request.Method.POST,
                MobileVerificationApi.URL,
                MobileVerificationApi.getParams(getActivity(), mobile, mOperatorId, mCircleId, connectionType, dataType, mail),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        if (ApiUtils.isValidResponse(s)) {
                            USSDUtils.storeSelfHelpDetails(getActivity(), s);
                            onVerificationDone();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        showSnack(NetworkErrorHelper.getErrorStatus(volleyError).getErrorMessage());
                    }
                });

        VolleySingleTon.getInstance(getActivity()).addToRequestQueue(stringRequest, TAG);
    }


    @Override
    public void onDestroy() {
        VolleySingleTon.getInstance(getActivity().getApplicationContext()).cancelPendingRequests(TAG);
        super.onDestroy();
    }

    protected void showSnack(int resId) {
        ((BaseActivity) getActivity()).showSnack(resId);
    }

    protected void showSnack(String string) {
        ((BaseActivity) getActivity()).showSnack(string);
    }

    protected abstract void onVerificationDone();

    public static JSONArray getDevicedata(Context ctx) {
        int sim_slot;
        String sim_IMEI, sim_IMSI, sim_OP_Code, sim_OP_Name;

        ArrayList<DualSIMBean> list = new ArrayList<DualSIMBean>();
        JSONArray device_details = new JSONArray();
        DualSIMBean dual_sim_bean;

        DualSimManager dual_sim_manager = new DualSimManager(ctx);
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(ctx);
        Boolean isDualSim = dual_sim_manager.isDualSIMSupported();


        if (isDualSim) {
            sim_slot = 2;
        } else {
            sim_slot = 1;
        }
        for (int i = 0; i < sim_slot; i++) {
            dual_sim_bean = new DualSIMBean();
            sim_IMEI = dual_sim_manager.getIMEI(i);
            if (sim_IMEI != null)
                dual_sim_bean.setSim_IMEI(sim_IMEI);
            else
                dual_sim_bean.setSim_IMEI("");

            sim_IMSI = dual_sim_manager.getIMSI(i);
            if (sim_IMSI != null)
                dual_sim_bean.setSim_IMSI(sim_IMSI);
            else
                dual_sim_bean.setSim_IMSI("");

            sim_OP_Code = dual_sim_manager.getNETWORK_OPERATOR_CODE(i);
            if (sim_OP_Code != null)
                dual_sim_bean.setSim_OP_Code(sim_OP_Code);
            else
                dual_sim_bean.setSim_OP_Code("");

            sim_OP_Name = dual_sim_manager.getNETWORK_OPERATOR_NAME(i);
            if (sim_OP_Name != null)
                dual_sim_bean.setSim_OP_Name(sim_OP_Name);
            else
                dual_sim_bean.setSim_OP_Name("");

            list.add(dual_sim_bean);
        }
        try {
            if (list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    DualSIMBean duaSimBean = list.get(i);
                    JSONObject jsonOBJ = new JSONObject();
                    String opName = "";

                    if (duaSimBean.getSim_OP_Name() != null) {
                        String arr[] = duaSimBean.getSim_OP_Name().split(" ", 2);
                        opName = arr[0];
                    }

                    jsonOBJ.put(IMEI, duaSimBean.getSim_IMEI());
                    jsonOBJ.put(IMSI, duaSimBean.getSim_IMSI());
                    jsonOBJ.put(NETCODE, duaSimBean.getSim_OP_Code());
                    jsonOBJ.put(OPNAME, duaSimBean.getSim_OP_Name());
                    for (int j = 0; j < OperatorUtils.sOperatorNames.length; j++) {
                        if (OperatorUtils.sOperatorNames[j].equalsIgnoreCase(opName)) {
                            jsonOBJ.put(OPID, OperatorUtils.sOperatorId[j]);
                            break;
                        } else {
                            jsonOBJ.put(OPID, "");
                        }
                    }

                    if (i == 0) {
                        jsonOBJ.put(SIM_STATUS, telephonyInfo.isSIM1Ready());
                        jsonOBJ.put(BB_USE, "Y");
                    } else {
                        jsonOBJ.put(SIM_STATUS, telephonyInfo.isSIM2Ready());
                        jsonOBJ.put(BB_USE, "N");
                    }

                    jsonOBJ.put(OS_VERSION, dual_sim_manager.getDeviceVersion());
                    jsonOBJ.put(MANUFACTURER, dual_sim_manager.getDeviceManufacturer());
                    jsonOBJ.put(DEVICE_MODEL, dual_sim_manager.getDeviceModel());
                    device_details.put(jsonOBJ);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return device_details;
    }

    // Progress dialog common code
    protected void showProgressDialog() {
        showProgressDialog(null);
    }

    protected void showProgressDialog(String message) {
        initProgressDialog();
        if (!mProgressDialog.isShowing())
            mProgressDialog.show();
        if (message != null) mProgressDialog.setMessage(message);
    }

    protected void initProgressDialog() {
        if (mProgressDialog != null) return;
        mProgressDialog = new ProgressDialog(getActivity());
    }

    protected void dismissProgressDialog() {
        if (mProgressDialog == null) return;
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }
}
