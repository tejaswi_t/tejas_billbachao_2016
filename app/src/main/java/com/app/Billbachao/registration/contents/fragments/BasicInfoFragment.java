package com.app.Billbachao.registration.contents.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.utils.NumberUtils;

/**
 * Created by mihir on 14-03-2016.
 */
public class BasicInfoFragment extends Fragment {

    View mRootView;

    EditText mNumberText;

    OnInfoListener mInfoListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_basic_info, container, false);
        initView();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnInfoListener) {
            mInfoListener = (OnInfoListener) context;
        }
    }

    @Override
    public void onDetach() {
        mInfoListener = null;
        super.onDetach();
    }

    void initView() {
        mNumberText = (EditText) mRootView.findViewById(R.id.mobile_number);
        mNumberText.addTextChangedListener(mNumberTextWatcher);
    }

    TextWatcher mNumberTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String number = s.toString();
            if (s.length() == 0)
                return;
            if (!NumberUtils.isValidNumber(number, true)) {
                mNumberText.setText("");
                ((BaseActivity) getActivity()).showToast(R.string.wrong_mobile_number_validation_msg);
            } else if (number.length() == 10) {
                // Take action
                onNumberEntered(number);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    void onNumberEntered(String number) {
        if (mInfoListener != null) {
            mInfoListener.onNumberEntered(number);
        }
    }

    public interface OnInfoListener {
        void onNumberEntered(String number);
    }
}
