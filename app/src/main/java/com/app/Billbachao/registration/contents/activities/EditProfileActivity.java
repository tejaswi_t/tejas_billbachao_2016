package com.app.Billbachao.registration.contents.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.registration.contents.fragments.EditProfileFragment;

/**
 * Created by mihir on 10-03-2016.
 */
public class EditProfileActivity extends BaseActivity {

    Fragment mRegistrationFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        mRegistrationFragment = new EditProfileFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, mRegistrationFragment).commit();
    }

}