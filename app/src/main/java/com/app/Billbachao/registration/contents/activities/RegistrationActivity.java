package com.app.Billbachao.registration.contents.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.background.IpAddressFetchTask;
import com.app.Billbachao.registration.contents.fragments.BasicInfoFragment;
import com.app.Billbachao.registration.contents.fragments.RegistrationFragment;

/**
 * Created by mihir on 10-03-2016.
 */
public class RegistrationActivity extends BaseActivity implements BasicInfoFragment.OnInfoListener {

    Fragment mBasicInfoFragment, mRegistrationFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        mBasicInfoFragment = new BasicInfoFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, mBasicInfoFragment).commit();
    }

    @Override
    public void onNumberEntered(String number) {
        mRegistrationFragment = RegistrationFragment.getInstance(number);
        getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right).remove(mBasicInfoFragment).replace(R.id.fragment, mRegistrationFragment).commit();
    }
}
