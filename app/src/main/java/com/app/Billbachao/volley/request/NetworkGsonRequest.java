package com.app.Billbachao.volley.request;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.myjson.Gson;
import com.google.myjson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Request to fetch serialised object based on the Class<T>
 * Created by nitesh on 18-04-2016.
 */
public class NetworkGsonRequest<T> extends BaseRequest<T> {

    private final Gson gson = new Gson();

    private final Class<T> clazz;

    /**
     * Gson request constructor
     * @param method GET, POST, PUT , DELETE
     * @param url api url
     * @param headers </>Map<String, String> headers = new LinkedHashMap<>() </>params to form body of the POST,PUT,DELETE request , null in GET
     * @param successListener success listener for request
     * @param errorListener error listener for request
     * @param enableCustomRequestBody flag to distinguish form encoded params v/s raw body of request
     */

    public NetworkGsonRequest(int method, String url, Class<T> clazz, Map<String, String> headers,
                              Response.Listener<T> successListener, Response.ErrorListener errorListener, boolean enableCustomRequestBody) {
        super(method, url, headers, successListener, errorListener, enableCustomRequestBody);
        this.clazz = clazz;
    }

    public NetworkGsonRequest(String url, Class<T> clazz, Map<String, String> headers,
                              Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(Request.Method.GET, url, headers, listener, errorListener, false);
        this.clazz = clazz;
    }




    public NetworkGsonRequest(int method, String url, Class<T> clazz, Map<String, String> headers,
                              Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, headers, listener, errorListener, false);
        this.clazz = clazz;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = baseNetworkResponseParsing(response);
            return Response.success(gson.fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return super.getParams();
    }
}