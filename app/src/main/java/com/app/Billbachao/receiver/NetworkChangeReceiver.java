package com.app.Billbachao.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.app.Billbachao.background.TaskScheduler;
import com.app.Billbachao.common.ConnectionManager;

/**
 * Created by BB-001 on 5/5/2016.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String connectionType = ConnectionManager.getConnectivityStatus(context);
        if (connectionType != null && !connectionType.equals("") && ConnectionManager.WIFI.equalsIgnoreCase(connectionType)) {
            TaskScheduler.verifyUser(context);
            TaskScheduler.performWifiTasks(context);
        }
        if (connectionType != null && !connectionType.equals("") && ConnectionManager.MOB.equalsIgnoreCase(connectionType)) {
            TaskScheduler.verifyUser(context);
        }
    }
}
