package com.app.Billbachao.usagelog.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.usagelog.LogHelper;
import com.app.Billbachao.usagelog.contents.utils.LogUtils;
import com.app.Billbachao.utils.DateUtils;
import com.app.Billbachao.utils.USSDUtils;

public class UsageHeaderItem {

    Context mContext;

    View mParentView, mRefreshView;

    TextView mTitleText, mUpdateText, mBalanceText;

    ImageView mIconView, mRefreshIconView;

    int mKey;

    public UsageHeaderItem(View containerView, int key) {
        mContext = containerView.getContext();
        mParentView = containerView;
        mKey = key;
        init();
    }

    void init() {
        mRefreshView = mParentView.findViewById(R.id.refresh);
        mTitleText = (TextView) mParentView.findViewById(R.id.description);
        mUpdateText = (TextView) mParentView.findViewById(R.id.updated_time);
        mBalanceText = (TextView) mParentView.findViewById(R.id.balance);
        mIconView = (ImageView) mParentView.findViewById(R.id.icon);
        mRefreshIconView = (ImageView) mParentView.findViewById(R.id.refresh_icon);
        mRefreshView.setOnClickListener(mClickListener);
        mRefreshIconView.setOnClickListener(mClickListener);
    }

    public void update() {
        String DATE_FORMAT = "h:mm a";
        if (mTitleText != null)
            mTitleText.setText(LogUtils.getCardTitleId(mKey));
        if (mIconView != null)
            mIconView.setImageResource(LogUtils.getCardIconId(mKey));
        if (LogHelper.hasBalance(mContext, mKey)) {
            if (mUpdateText != null)
                mUpdateText.setText(mContext.getString(R.string.updated_at_n, DateUtils.convertTimeToDate(LogHelper.getLastUpdatedTime(mContext, mKey), DATE_FORMAT)));
            if (mBalanceText != null)
                mBalanceText.setText(mContext.getString(LogUtils.getBalanceFormatString(mKey), LogHelper.getBalance(mContext, mKey)));
        } else {
            if (mBalanceText != null)
                mBalanceText.setText(mContext.getString(LogUtils.getDefaultFormatString(mKey), "--.--"));
        }
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mRefreshIconView.animate().cancel();
            mRefreshIconView.animate().rotationBy(180).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    switch (mKey) {
                        case LogUtils.TYPE_CALL:
                            USSDUtils.entertainRequest(mContext, USSDUtils.CHECK_BALANCE);
                            break;
                        case LogUtils.TYPE_DATA:
                            USSDUtils.entertainRequest(mContext, USSDUtils.DATA_BALANCE);
                            break;
                    }
                }
            });
        }
    };
}