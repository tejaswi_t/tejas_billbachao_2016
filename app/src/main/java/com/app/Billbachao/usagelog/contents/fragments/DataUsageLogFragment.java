package com.app.Billbachao.usagelog.contents.fragments;

import android.os.Bundle;

import com.app.Billbachao.usagelog.contents.adapters.BaseResourceCursorAdapter;
import com.app.Billbachao.usagelog.contents.adapters.DataLogsAdapter;
import com.app.Billbachao.usagelog.contents.utils.ListUtils;
import com.app.Billbachao.usagelog.widget.AppBitmapCache;

/**
 * Created by mihir on 28-10-2015.
 */
public class DataUsageLogFragment extends BaseUsageFragment {

    public static final String TAG = DataUsageLogFragment.class.getSimpleName();

    public static DataUsageLogFragment getInstance() {
        return new DataUsageLogFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppBitmapCache.ensureCache();
    }

    @Override
    public BaseResourceCursorAdapter getAdapter() {
        return new DataLogsAdapter(null, getActivity());
    }

    @Override
    public ListUtils.QueryArgs getQueryArgs() {
        return new ListUtils.DayWiseDataArgs();
    }

    @Override
    public void onDestroy() {
        AppBitmapCache.clearCache();
        super.onDestroy();
    }
}
