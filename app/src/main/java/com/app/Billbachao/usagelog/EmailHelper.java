package com.app.Billbachao.usagelog;

import android.content.Context;

import com.app.Billbachao.usagelog.helper.CallSmsUsageHelper;
import com.app.Billbachao.usagelog.helper.DataUsageHelper;
import com.app.Billbachao.usagelog.model.UsageDetailsJsonInfo;
import com.app.Billbachao.utils.DateUtils;
import com.google.myjson.Gson;

/**
 * Created by mihir on 18-11-2015.
 */
public class EmailHelper {

    Context mContext;

    UsageDetailsJsonInfo mJsonInfo;

    String mDataObject;

    int mTotalCost = 0;

    public EmailHelper(Context context) {
        mContext = context;
        mJsonInfo = new UsageDetailsJsonInfo();
    }

    public void getUsageDetails(int days) {
        long time = DateUtils.getBeforeDays(days);
        CallSmsUsageHelper.getCallSmsLogsInfo(mContext, time, mJsonInfo);
        DataUsageHelper.getDataLogsInfo(mContext, time, mJsonInfo);
        mDataObject = new Gson().toJson(mJsonInfo);
        mTotalCost = mJsonInfo.getTotalCost();
    }

    public String getDataObject() {
        return mDataObject;
    }

    public int getTotalCost() {
        return mTotalCost;
    }
}
