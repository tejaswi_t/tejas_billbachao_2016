package com.app.Billbachao.usagelog.alerts.contents;

import android.os.Bundle;

import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;

/**
 * Created by mihir on 20-08-2015.
 */
public class PreferenceActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preference_activity);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, PreferencesFragment
                .getInstance()).commit();
    }
}
