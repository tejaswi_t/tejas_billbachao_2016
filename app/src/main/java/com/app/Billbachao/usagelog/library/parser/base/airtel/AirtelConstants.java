package com.app.Billbachao.usagelog.library.parser.base.airtel;

/**
 * Created by mihir on 02-11-2015.
 */
public interface AirtelConstants {

    String CALL_COST = "CALLCOST", CALL__COST = "CALL COST", DATA = "DATA", SMS = "SMS";

    String COST = "COST";
}
