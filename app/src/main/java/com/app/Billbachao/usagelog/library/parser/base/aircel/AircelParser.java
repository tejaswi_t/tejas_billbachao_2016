package com.app.Billbachao.usagelog.library.parser.base.aircel;

import com.app.Billbachao.usagelog.library.parser.base.BaseParser;
import com.app.Billbachao.usagelog.library.parser.model.CostInfo;

/**
 * Created by mihir on 23-11-2015.
 */
public class AircelParser extends BaseParser implements AircelConstants {

    @Override
    public boolean parse(String data) {
        if (data.contains(CALL_COST) || data.contains(VOICE_COST) || data.contains(DURATION)) {
            return parseCallUsage(data);
        } else if(data.contains(SMS_COST)) {
            return parseSmsUsage(data);
        } else if(data.contains(DATA) || data.contains(VOLUME) || data.contains(SESSION)) {
            return parseDataUsage(data);
        } else {
            return parseUssdData(data);
        }
    }

    @Override
    protected boolean parseDataUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.DATA;
        if (findBalance(data)) {
            // TODO needs to be implemented, no sample present for study
        }
        return false;
    }

    @Override
    protected boolean parseCallUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.CALL;
        return findCostAndBalance(data);
    }

    @Override
    protected boolean parseSmsUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.SMS;
        return findCostAndBalance(data);
    }

    @Override
    protected boolean parseUssdData(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.USSD;
        return findBalance(data);
    }

    boolean findCostAndBalance(String data) {
        return (findBalance(data) && findCost(data));
    }

    boolean findBalance(String data) {
        int talkTimeIndex = -1;
        if (data.contains(BALANCE)) {
            talkTimeIndex = data.indexOf(BALANCE);
        } else if (data.contains(BAL)) {
            talkTimeIndex = data.indexOf(BAL);
        }

        if (talkTimeIndex >= 0) {
            mCostInfo.mainBalance = extractFloats(data.substring(talkTimeIndex), 1)[0];
            return true;
        }
        return false;
    }

    boolean findCost(String data) {
        if (data.contains(COST)) {
            int costIndex = data.indexOf(COST);
            mCostInfo.cost = extractFloats(data.substring(costIndex), 1)[0];
            return true;
        }
        return false;
    }
}
