package com.app.Billbachao.usagelog.library.parser.base.uninor;

import com.app.Billbachao.usagelog.library.parser.base.BaseParser;
import com.app.Billbachao.usagelog.library.parser.model.CostInfo;

/**
 * Pattern found in UNINOR, to know the balance, find String TALKTIME.
 *
 * Created by mihir on 02-11-2015.
 */
public class TelenorParser extends BaseParser implements UninorConstants {

    @Override
    public boolean parse(String data) {
        if (data.contains(CALL_DURATION) || (data.contains(CALL_COST) && data.contains(DURATION))) {
            return parseCallUsage(data);
        } else if(data.contains(SMS)) {
            return parseSmsUsage(data);
        } else if(data.contains(DATA)) {
            if(data.contains(REMAINING)) {
                return parseDataCheck(data);
            } else {
                return parseDataUsage(data);
            }
        } else {
            return parseUssdData(data);
        }
    }

    @Override
    protected boolean parseDataUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.DATA;
        if (findBalance(data)) {
            // TODO needs to be implemented, no sample present for study
        }
        return false;
    }

    @Override
    protected boolean parseCallUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.CALL;
        return findCostAndBalance(data);
    }

    @Override
    protected boolean parseSmsUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.SMS;
        return findCostAndBalance(data);
    }

    @Override
    protected boolean parseUssdData(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.USSD;
        return findBalance(data);
    }

    boolean parseDataCheck(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.DATA_CHECK;
        float dataLeft = extractDataMbKb(data, 1)[0];
        if (dataLeft >= 0) {
            mCostInfo.dataLeft = dataLeft;
            mCostInfo.type = CostInfo.DATA_CHECK;
            return true;
        }
        return false;
    }

    boolean findCostAndBalance(String data) {
        if (findBalance(data)) {
            float[] amounts = extractFloats(data, 2);
            mCostInfo.cost = (Math.abs(amounts[1] - mCostInfo.mainBalance) < EPSILON) ? amounts[0] : amounts[1];
            return true;
        }
        return false;
    }

    boolean findBalance(String data) {
        if (data.contains(TALKTIME)) {
            int talkTimeIndex = data.indexOf(TALKTIME);
            if (talkTimeIndex >= 0) {
                mCostInfo.mainBalance = extractFloats(data.substring(talkTimeIndex), 1)[0];
                return true;
            }
        }
        return false;
    }
}

