package com.app.Billbachao.usagelog.library.parser.base.idea;

import com.app.Billbachao.usagelog.library.parser.base.BaseParser;
import com.app.Billbachao.usagelog.library.parser.model.CostInfo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Unique pattern found in IDEA, to know the cost amount find string CHRG/CHARG. FOr finding balance, find String BAL_LEFT
 *
 * Created by mihir on 02-11-2015.
 */
public class IdeaParser extends BaseParser implements IdeaConstants {

    @Override
    public boolean parse(String data) {
        if (!(data.contains(CHARG) || data.contains(CHRG))) {
            return parseUssdData(data);
        } else if(data.contains(VOL) || data.contains(VOU)) {
            return parseDataUsage(data);
        } else if(data.contains(DUR)) {
            return parseCallUsage(data);
        } else {
            return parseSmsUsage(data);
        }
    }

    @Override
    protected boolean parseDataUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.DATA;
        if (!findCostAndBalance(data)) {
            return false;
        }

        boolean isDataPack = false;
        if (mCostInfo.cost == 0) {
            isDataPack = true;
        }

        Pattern mbkbPattern = Pattern.compile(DATA_PATTERN);
        Matcher mbkbMatcher = mbkbPattern.matcher(data);
        if (mbkbMatcher.find()) {
            boolean isMb = (mbkbMatcher.group().contains("MB"));

            float volume[] = extractNumericData(data, 3, DATA_PATTERN);
            mCostInfo.dataUsed = volume[0] / (isMb ? 1 : MB_KB_RATIO);

            if (isDataPack) {
                mCostInfo.dataLeft = (Math.max(volume[1], volume[2]) / (isMb ? 1 : MB_KB_RATIO));
            }
            return true;
        }

        return false;
    }

    @Override
    protected boolean parseCallUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.CALL;
        return findCostAndBalance(data);
    }

    @Override
    protected boolean parseSmsUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.SMS;
        return findCostAndBalance(data);
    }

    @Override
    protected boolean parseUssdData(String data) {
        mCostInfo = new CostInfo();

        if (parseDataCheck(data)) {
            return true;
        }

        mCostInfo.type = CostInfo.USSD;

        int index1 = data.indexOf(MAIN);
        int index2 = data.indexOf(BALANCE);
        int finalIndex = index1 >= 0 ? index2 >= 0 ? Math.min(index1, index2) : index1 : index2
                >= 0 ? index2 : -1;
        if (finalIndex >= 0) {
            mCostInfo.mainBalance = extractFloats(data.substring(finalIndex), 1)[0];
            return true;
        }
        return false;
    }

    boolean parseDataCheck(String data) {
        int index1 = data.indexOf(VOL);
        int index2 = data.indexOf(BAL);
        int finalIndex = index1 >= 0 ? index2 >= 0 ? Math.min(index1, index2) : index1 : index2
                >= 0 ? index2 : -1;
        if (finalIndex >= 0) {
            float dataLeft = extractDataMbKb(data.substring(finalIndex), 1)[0];
            if (dataLeft >= 0) {
                mCostInfo.dataLeft = dataLeft;
                mCostInfo.type = CostInfo.DATA_CHECK;
                return true;
            }
        }
        return false;
    }

    boolean findCostAndBalance(String data) {
        Pattern chrgPattern = Pattern.compile(IDEA_CHRG);
        Matcher chrgMatcher = chrgPattern.matcher(data);

        boolean dataFound = false;

        if (chrgMatcher.find()) {
            int index = chrgMatcher.end();
            if (index < data.length()) {
                mCostInfo.cost = extractFloats(data.substring(index), 1)[0];
                dataFound = true;
            }
        }

        Pattern leftPattern = Pattern.compile(LEFT);
        Matcher leftMatcher = leftPattern.matcher(data);

        if (leftMatcher.find()) {
            int index = leftMatcher.end();
            if (index < data.length()) {
                mCostInfo.mainBalance = extractFloats(data.substring(index), 1)[0];
                dataFound = true;
            }
        }

        return dataFound;
    }
}

