package com.app.Billbachao.usagelog;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.TrafficStats;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;

import com.app.Billbachao.background.TaskScheduler;
import com.app.Billbachao.common.permissions.PermissionWrapper;
import com.app.Billbachao.model.PackageDataInfo;
import com.app.Billbachao.usage.helper.UsageHelper;
import com.app.Billbachao.usagelog.library.parser.model.CostInfo;
import com.app.Billbachao.usagelog.model.DayCostInfo;
import com.app.Billbachao.usagelog.provider.UsageLogsProvider;
import com.app.Billbachao.usagelog.widget.AppBitmapFactory;
import com.app.Billbachao.utils.AppManagerUtils;
import com.app.Billbachao.utils.DateUtils;

import java.util.List;

/**
 * Created by mihir on 19-10-2015.
 */
public class LogHelper {

    public static final String PREFERENCE_FILE = "LogsPreference";

    public static final String CALL_BALANCE = "callBalance", DATA_BALANCE = "dataBalance",
            CALL_UPDATED = "callUpdated", DATA_UPDATED = "dataUpdated";

    public static final float DEFAULT_BALANCE = 0f;

    public static final String TYPE_USAGE = "usage";

    public static final int TYPE_CALL = 0x01, TYPE_DATA = 0x02;

    static final int APPS_COUNT = 8;

    public static final String DAILY_USAGE_ALERT = "daily_usage";

    public static float getBalance(Context context, int type) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_FILE, Context
                .MODE_PRIVATE);
        return preferences.getFloat(getBalanceKey(type), DEFAULT_BALANCE);
    }

    public static boolean hasBalance(Context context, int type) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_FILE, Context
                .MODE_PRIVATE);
        return preferences.contains(getBalanceKey(type));
    }

    /**
     * This method returns if value updated was too old. As of now, the time gap is one day.
     *
     * @param context
     * @param type
     * @return
     */
    public static boolean isInfoExpired(Context context, int type) {
        long lastUpdatedTime = getLastUpdatedTime(context, type);
        long baseTime = DateUtils.getBeforeDays(0);
        return lastUpdatedTime < baseTime;
    }

    public static long getLastUpdatedTime(Context context, int type) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_FILE, Context
                .MODE_PRIVATE);
        return preferences.getLong(getBalanceUpdatedKey(type), System.currentTimeMillis());
    }

    public static void storeBalance(Context context, int type, float balance) {
        if (balance < 0) {
            return;
        }
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_FILE, Context
                .MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(getBalanceKey(type), balance);
        editor.putLong(getBalanceUpdatedKey(type), System.currentTimeMillis());
        editor.apply();
    }

    public static String getBalanceKey(int type) {
        switch (type) {
            case TYPE_CALL:
                return CALL_BALANCE;
            case TYPE_DATA:
                return DATA_BALANCE;
        }
        return null;
    }

    public static String getBalanceUpdatedKey(int type) {
        switch (type) {
            case TYPE_CALL:
                return CALL_UPDATED;
            case TYPE_DATA:
                return DATA_UPDATED;
        }
        return "";
    }

    public static boolean isDailyUsageActive(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_FILE, Context
                .MODE_PRIVATE);
        return preferences.getBoolean(DAILY_USAGE_ALERT, true);
    }

    public static void setDailyUsageActive(Context context, boolean active) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_FILE, Context
                .MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(DAILY_USAGE_ALERT, active).apply();

         TaskScheduler.setUsageNotificationAlarm(context);
    }

    /**
     * Populate outgoing logs (7 days) for showing some initial data
     *
     * @param context
     */
    public static boolean populateInitialLogs(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        long startDate = DateUtils.getBeforeDays(7);
        Cursor callLogCursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI,
                null, CallLog.Calls.DATE + ">? AND " + CallLog.Calls.TYPE + "=? AND " + CallLog
                        .Calls.DURATION + ">?", new String[]{String.valueOf(startDate), String
                        .valueOf(CallLog.Calls.OUTGOING_TYPE), "0"}, CallLog.Calls.DEFAULT_SORT_ORDER);

        if (callLogCursor != null) {
            if (callLogCursor.moveToFirst()) {
                ContentValues[] callLogValues = new ContentValues[callLogCursor.getCount()];

                int dateIndex = callLogCursor.getColumnIndex(CallLog.Calls.DATE);
                int durationIndex = callLogCursor.getColumnIndex(CallLog.Calls.DURATION);
                int typeIndex = callLogCursor.getColumnIndex(CallLog.Calls.TYPE);
                int numberIndex = callLogCursor.getColumnIndex(CallLog.Calls.NUMBER);
                int nameIndex = callLogCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
                int index = 0;
                do {
                    ContentValues values = new ContentValues();
                    values.put(UsageLogsProvider.CallColumns.NUMBER, callLogCursor.getString(numberIndex));
                    values.put(UsageLogsProvider.CallColumns.NAME, callLogCursor.getString(nameIndex));
                    values.put(UsageLogsProvider.CallColumns.CALL_TYPE, callLogCursor.getInt(typeIndex));
                    values.put(UsageLogsProvider.CallColumns.DATE, callLogCursor.getLong(dateIndex));
                    values.put(UsageLogsProvider.CallColumns.DURATION, callLogCursor.getLong(durationIndex));
                    values.put(UsageLogsProvider.CallColumns.USAGE_TYPE, UsageLogsProvider.CallColumns.TYPE_CALL);
                    values.put(UsageLogsProvider.CallColumns.COST, -1);

                    callLogValues[index++] = values;
                } while (callLogCursor.moveToNext());

                context.getContentResolver().bulkInsert(UsageLogsProvider.CallColumns.CONTENT_URI, callLogValues);
            }
            callLogCursor.close();
        }
        return true;
    }

    /**
     * Saves last call log irrevelant of call type. Invoked on USSD callback.
     *
     * @param context
     * @return false for error cases like no permissions
     */
    public static boolean saveLastCallLog(Context context) {
        return saveLastCallLog(context, false);
    }

    /**
     * Saves last call log, possess filter power for outgoing calls. Invoked on Call state change.
     *
     * @param context
     * @return false for error cases like no permissions
     */
    public static boolean saveLastCallLog(Context context, boolean onlyOutgoing) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        ContentResolver cr = context.getContentResolver();
        Cursor callLogCursor = cr.query(CallLog.Calls.CONTENT_URI,
                null, null, null, CallLog.Calls.DEFAULT_SORT_ORDER);
        if (callLogCursor != null) {
            if (callLogCursor.moveToFirst()) {

                long date = callLogCursor.getLong(callLogCursor.getColumnIndex(CallLog.Calls.DATE));
                long duration = callLogCursor.getLong(callLogCursor.getColumnIndex(CallLog.Calls
                        .DURATION));
                int callType = callLogCursor.getInt(callLogCursor.getColumnIndex(CallLog.Calls.TYPE));

                if (onlyOutgoing && (callType != CallLog.Calls.OUTGOING_TYPE || duration <= 0)) {
                    callLogCursor.close();
                    return true;
                }

                Cursor usageCursor = cr.query(UsageLogsProvider.CallColumns.CONTENT_URI, null,
                        UsageLogsProvider.CallColumns.DATE + "=? AND " + UsageLogsProvider.CallColumns
                                .DURATION + "=?", new String[]{String.valueOf(date), String.valueOf
                                (duration)}, null);

                if (usageCursor == null || usageCursor.getCount() == 0) {

                    if (usageCursor != null) {
                        usageCursor.close();
                    }

                    ContentValues values = new ContentValues();
                    values.put(UsageLogsProvider.CallColumns.NUMBER, callLogCursor.getString
                            (callLogCursor.getColumnIndex(CallLog.Calls.NUMBER)));
                    values.put(UsageLogsProvider.CallColumns.NAME, callLogCursor.getString
                            (callLogCursor.getColumnIndex(CallLog.Calls.CACHED_NAME)));
                    values.put(UsageLogsProvider.CallColumns.CALL_TYPE, callType);
                    values.put(UsageLogsProvider.CallColumns.DATE, date);
                    values.put(UsageLogsProvider.CallColumns.DURATION, duration);
                    values.put(UsageLogsProvider.CallColumns.USAGE_TYPE, UsageLogsProvider
                            .CallColumns.TYPE_CALL);
                    values.put(UsageLogsProvider.CallColumns.COST, -1);

                    if (!isLogAlreadyPresent(context, String.valueOf(date))) {
                        cr.insert(UsageLogsProvider.CallColumns.CONTENT_URI,
                                values);
                    }
                }
            }
            callLogCursor.close();
        }
        return true;
    }

    public static boolean isLogAlreadyPresent(Context context, String searchItem) {

        Cursor cursor = context.getContentResolver().query(UsageLogsProvider.CallColumns
                .CONTENT_URI, null, UsageLogsProvider.CallColumns.DATE + "=?", new String[]{searchItem}, UsageLogsProvider.CallColumns
                .DEFAULT_SORT_ORDER);

        boolean exists = false;

        if (null != cursor) {
            exists = (cursor.getCount() > 0);
            cursor.close();
        }
        return exists;
    }

    public static void storeCallCostInfo(Context context, CostInfo info) {
        if (info.cost >= 0) {
            Cursor cursor = context.getContentResolver().query(UsageLogsProvider.CallColumns
                    .CONTENT_URI, null, UsageLogsProvider.CallColumns.USAGE_TYPE + "=? AND " +
                    UsageLogsProvider.CallColumns.DURATION + "> ?", new String[]{String.valueOf
                    (UsageLogsProvider.CallColumns.TYPE_CALL), "0"}, UsageLogsProvider.CallColumns.DEFAULT_SORT_ORDER);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    int id = cursor.getInt(cursor.getColumnIndex(UsageLogsProvider.CallColumns
                            ._ID));
                    ContentValues values = new ContentValues();
                    values.put(UsageLogsProvider.CallColumns.COST, info.cost);
                    context.getContentResolver().update(UsageLogsProvider.CallColumns
                            .CONTENT_URI, values, UsageLogsProvider.CallColumns._ID + "=?", new
                            String[]{String.valueOf(id)});
                }
                cursor.close();
            }
        }
    }

    public static void storeDayWiseDataUsage(Context context) {
        storeDayWiseDataUsage(context, false);
    }

    public static void storeDayWiseDataUsage(Context context, boolean onlyInsertIfNotPresent) {
        String date = DateUtils.convertTimeToDate(System.currentTimeMillis(), DateUtils
                .FORMAT_DATE_ONLY);

        boolean isPresent = false;
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(UsageLogsProvider.DayWiseDataColumns.CONTENT_URI, null,
                UsageLogsProvider.DayWiseDataColumns.DATE + "=?", new String[]{String.valueOf
                        (date)}, null);
        if (cursor != null) {
            isPresent = cursor.getCount() != 0;
            cursor.close();
        }

        if (isPresent && onlyInsertIfNotPresent) {
            return;
        }

        ContentValues dayWiseValues = new ContentValues();
        dayWiseValues.put(UsageLogsProvider.DayWiseDataColumns.DATE, date);
        long mobileBytes = (TrafficStats.getMobileRxBytes() + TrafficStats.getMobileTxBytes()) / 1024;
        long wifiBytes = (TrafficStats.getTotalRxBytes() + TrafficStats.getTotalTxBytes()) / 1024 - mobileBytes;
        dayWiseValues.put(UsageLogsProvider.DayWiseDataColumns.USAGE_MOBILE, mobileBytes);
        dayWiseValues.put(UsageLogsProvider.DayWiseDataColumns.USAGE_WIFI, wifiBytes);

        if (isPresent) {
            cr.update(UsageLogsProvider.DayWiseDataColumns.CONTENT_URI, dayWiseValues,
                    UsageLogsProvider.DayWiseDataColumns.DATE + "=?", new String[]{String
                            .valueOf(date)});
        } else {
            cr.insert(UsageLogsProvider.DayWiseDataColumns.CONTENT_URI, dayWiseValues);
        }

    }

    public static void addToDayWiseDataCostOrConsumption(Context context, float cost, float dataUsed) {
        if (cost <= 0 && dataUsed <= 0) {
            return;
        }
        cost = Math.max(0, cost);
        dataUsed = Math.max(0, dataUsed);

        float previousCost = 0, previousData = 0;
        String date = DateUtils.convertTimeToDate(System.currentTimeMillis(), DateUtils
                .FORMAT_DATE_ONLY);

        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(UsageLogsProvider.DayWiseDataColumns.CONTENT_URI, null,
                UsageLogsProvider.DayWiseDataColumns.DATE + "=?", new String[]{String.valueOf
                        (date)}, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                previousCost = cursor.getFloat(cursor.getColumnIndex(UsageLogsProvider
                        .DayWiseDataColumns.COST));
                previousData = cursor.getFloat(cursor.getColumnIndex(UsageLogsProvider
                        .DayWiseDataColumns.USSD_USAGE_MOBILE));
            }
            cursor.close();
        }

        ContentValues values = new ContentValues();
        values.put(UsageLogsProvider.DayWiseDataColumns.COST, cost + previousCost);
        values.put(UsageLogsProvider.DayWiseDataColumns.USSD_USAGE_MOBILE, dataUsed + previousData);
        context.getContentResolver().update(UsageLogsProvider.DayWiseDataColumns.CONTENT_URI,
                values,
                UsageLogsProvider.DayWiseDataColumns.DATE + "=?", new String[]{String
                        .valueOf(date)});

    }

    public static void storeAppWiseDataUsage(Context context) {
        String date = DateUtils.convertTimeToDate(System.currentTimeMillis(), DateUtils
                .FORMAT_DATE_ONLY);
        List<PackageDataInfo> dataInfoList = AppManagerUtils.getPackageWiseDataUsage(context,
                APPS_COUNT);
        ContentValues[] dataValues = new ContentValues[dataInfoList.size()];
        int index = 0;

        for (PackageDataInfo dataInfo : dataInfoList) {
            ContentValues values = new ContentValues();
            values.put(UsageLogsProvider.DataColumns.PACKAGE_NAME, dataInfo.getPackageName());
            values.put(UsageLogsProvider.DataColumns.NAME, dataInfo.getName());
            values.put(UsageLogsProvider.DataColumns.BITMAP, AppBitmapFactory.getBlobFromDrawable
                    (dataInfo.getDrawable()));
            values.put(UsageLogsProvider.DataColumns.DATE, date);
            values.put(UsageLogsProvider.DataColumns.USAGE_KB, dataInfo.getData() / 1024);
            values.put(UsageLogsProvider.DataColumns.DAY_ORDER, index + 1);
            values.put(UsageLogsProvider.DataColumns.DATA_TYPE, UsageLogsProvider.DataColumns.TYPE_NA);
            dataValues[index++] = values;
        }

        if (dataInfoList.size() > 0) {
            context.getContentResolver().delete(UsageLogsProvider.DataColumns.CONTENT_URI,
                    UsageLogsProvider.DataColumns.DATE + "=?", new String[]{date});
            context.getContentResolver().bulkInsert(UsageLogsProvider.DataColumns.CONTENT_URI, dataValues);
        }
    }

    public static void saveLastSmsLog(Context context) {
        ContentResolver cr = context.getContentResolver();

        Cursor smsLogCursor = cr.query(Uri.parse("content://sms"), null, null, null, "date DESC");
        if (smsLogCursor != null) {
            if (smsLogCursor.moveToFirst()) {

                long date = smsLogCursor.getLong(smsLogCursor.getColumnIndex("date"));
                Cursor usageCursor = cr.query(UsageLogsProvider.CallColumns.CONTENT_URI, null,
                        UsageLogsProvider.CallColumns.DATE + "=?", new String[]{String.valueOf(date)}, null);

                if (usageCursor == null || usageCursor.getCount() == 0) {
                    if (usageCursor != null) {
                        usageCursor.close();
                    }

                    ContentValues values = new ContentValues();
                    String number = smsLogCursor.getString(smsLogCursor.getColumnIndex("address"));
                    values.put(UsageLogsProvider.CallColumns.NUMBER, number);
                    String name = smsLogCursor.getString(smsLogCursor.getColumnIndex("person"));
                    if (name == null) {
                        name = LogHelper.getContactName(context, number);
                    }
                    if (name != null) {
                        values.put(UsageLogsProvider.CallColumns.NAME, name);
                    }
                    values.put(UsageLogsProvider.CallColumns.DATE, date);
                    values.put(UsageLogsProvider.CallColumns.USAGE_TYPE, UsageLogsProvider.CallColumns.TYPE_SMS);
                    cr.insert(UsageLogsProvider.CallColumns.CONTENT_URI, values);
                }
            }
            smsLogCursor.close();
        }
    }

    public static void storeSmsCostInfo(Context context, CostInfo info) {
        if (info.cost >= 0) {
            Cursor cursor = context.getContentResolver().query(UsageLogsProvider.CallColumns
                            .CONTENT_URI, null, UsageLogsProvider.CallColumns.USAGE_TYPE + "= ?", new
                            String[]{String.valueOf(UsageLogsProvider.CallColumns.TYPE_SMS)},
                    UsageLogsProvider.CallColumns.DEFAULT_SORT_ORDER);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    int id = cursor.getInt(cursor.getColumnIndex(UsageLogsProvider.CallColumns
                            ._ID));
                    ContentValues values = new ContentValues();
                    values.put(UsageLogsProvider.CallColumns.COST, info.cost);
                    context.getContentResolver().update(UsageLogsProvider.CallColumns
                            .CONTENT_URI, values, UsageLogsProvider.CallColumns._ID + "=?", new
                            String[]{String.valueOf(id)});
                }
                cursor.close();
            }
        }
    }

    public static boolean isLogsEmpty(Context context) {
        return isCallLogsEmpty(context) && isDataLogsEmpty(context);
    }

    public static boolean isCallLogsEmpty(Context context) {
        Cursor callCursor = context.getContentResolver().query(UsageLogsProvider.CallColumns
                .CONTENT_URI, null, null, null, null);
        if (callCursor != null) {
            if (callCursor.getCount() != 0) {
                callCursor.close();
                return false;
            }
            callCursor.close();
        }
        return true;
    }

    // Twist here, consider data usage empty if no USSD entries
    public static boolean isDataLogsEmpty(Context context) {
        Cursor dataCursor = context.getContentResolver().query(UsageLogsProvider.DayWiseDataColumns
                .CONTENT_URI, null, UsageLogsProvider.DayWiseDataColumns.USSD_USAGE_MOBILE + ">? OR "
                + UsageLogsProvider.DayWiseDataColumns.COST + " >?", new String[]{String.valueOf(0), String.valueOf(0)}, null);
        if (dataCursor != null) {
            if (dataCursor.getCount() != 0) {
                dataCursor.close();
                return false;
            }
            dataCursor.close();
        }
        return true;
    }

    public static String getContactName(Context context, String number) {
        String name = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        Cursor cursor = context.getContentResolver().query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            }
            cursor.close();
        }
        return name;
    }

    public static DayCostInfo getCallDurationPostPaid(Context context) {
        long startDate = DateUtils.getBeforeDays(0);
        long totalDuration = 0;
        DayCostInfo dayCostInfo = new DayCostInfo();

        if (!PermissionWrapper.hasCallLogsPermissions(context)) {
            return dayCostInfo;
        }

        Cursor callLogCursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI,
                null, CallLog.Calls.DATE + ">? AND " + CallLog.Calls.TYPE + "=? AND " + CallLog
                        .Calls.DURATION + ">?", new String[]{String.valueOf(startDate), String
                        .valueOf(CallLog.Calls.OUTGOING_TYPE), "0"}, CallLog.Calls.DEFAULT_SORT_ORDER);

        if (callLogCursor != null) {
            if (callLogCursor.moveToFirst()) {
                int durationIndex = callLogCursor.getColumnIndex(CallLog.Calls.DURATION);
                do {
                    totalDuration += callLogCursor.getLong(durationIndex);
                } while (callLogCursor.moveToNext());
            }
            callLogCursor.close();
        }
        dayCostInfo.duration = totalDuration;
        return dayCostInfo;
    }

    public static DayCostInfo getSMSCountPostPaid(Context context) {
        long startDate = DateUtils.getBeforeDays(0);
        DayCostInfo dayCostInfo = new DayCostInfo();
        Uri message = Uri.parse("content://sms/" + "sent");
        String filter = "date >=" + startDate;
        Cursor cursor = context.getContentResolver().query(message, null, filter, null, null);
        if (cursor != null) {
            dayCostInfo.count = cursor.getCount();
            cursor.close();
        }

        return dayCostInfo;
    }
    public static DayCostInfo getMobileDataTrafficStatsPostPaid() {
        DayCostInfo dayCostInfo = new DayCostInfo();
        dayCostInfo.dataMb = UsageHelper.getMobileDataTrafficStats();
        return dayCostInfo;
    }

}
