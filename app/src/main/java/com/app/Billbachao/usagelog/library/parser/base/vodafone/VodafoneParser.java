package com.app.Billbachao.usagelog.library.parser.base.vodafone;

import com.app.Billbachao.usagelog.library.parser.base.BaseParser;
import com.app.Billbachao.usagelog.library.parser.model.CostInfo;

/**
 * Created by mihir on 03-11-2015.
 */
public class VodafoneParser extends BaseParser implements VodafoneConstants {

    @Override
    public boolean parse(String data) {

        if (data.contains(SESSION) && data.contains(CONSUMED)) {
            return parseDataUsage(data);
        } else if (data.contains(LAST) || data.contains(DURATION) || data.contains(DEDUCTION)) {
            if (data.contains(CALL)) {
                return parseCallUsage(data);
            } else if (data.contains(SMS)) {
                return parseSmsUsage(data);
            }
        }else if(data.contains(BAL) || data.contains(BALANCE)) {
            return parseUssdData(data);
        }
        return false;
    }

    @Override
    protected boolean parseDataUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.DATA;

        boolean isDataPack = false;

        // Decide datapack present or not

        float amount[] = extractINRGroups(data, isDataPack ? 1 : 2);
        mCostInfo.cost = amount[0];

        float dataUsage[] = extractDataMbKb(data, isDataPack ? 2 : 1);
        mCostInfo.dataUsed = dataUsage[0];

        if (!isDataPack) {
            mCostInfo.mainBalance = amount[1];
        } else {
            mCostInfo.dataLeft = dataUsage[1];
        }

        return true;
    }

    @Override
    protected boolean parseCallUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.CALL;

        float amount[] = extractFloats(data, 2);
        mCostInfo.cost = amount[0];
        mCostInfo.mainBalance = amount[1];

        return true;
    }

    @Override
    protected boolean parseSmsUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.SMS;

        float amount[] = extractFloats(data, 2);
        mCostInfo.cost = amount[0];
        mCostInfo.mainBalance = amount[1];

        return false;
    }
}
