package com.app.Billbachao.usagelog.library.parser.base.vodafone;

/**
 * Created by mihir on 03-11-2015.
 */
public interface VodafoneConstants {

    String SESSION = "SESSION", CONSUMED = "CONSUMED", COST = "COST", CALL = "CALL", LAST = "LAST", DURATION = "DURATION";

    String SMS = "SMS", DEDUCTION = "DEDUCTION";
}
