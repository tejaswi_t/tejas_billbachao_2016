package com.app.Billbachao.usagelog.receiver;

import android.content.Context;
import android.os.Handler;

import com.app.Billbachao.usagelog.LogHelper;

import java.util.Date;

/**
 * Created by Tejaswi on 16-03-2016.
 */
public class PhoneCallReceiver extends AbstractCallReceiver {

    @Override
    protected void onIncomingCallReceived(Context context, String number, Date start) {
    }

    @Override
    protected void onIncomingCallAnswered(Context context, String number, Date start) {
    }

    @Override
    protected void onIncomingCallEnded(Context context, String number, Date start, Date end) {
    }

    @Override
    protected void onOutgoingCallStarted(Context context, String number, Date start) {
    }

    @Override
    protected void onOutgoingCallEnded(final Context context, String number, Date start, Date end) {
        // Unfortunately, have to add delay because of delay in system call logs update
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                LogHelper.saveLastCallLog(context, true);
            }
        }, 2000);
    }

    @Override
    protected void onMissedCall(Context context, String number, Date start) {
    }

}