package com.app.Billbachao.usagelog.library.parser.base.idea;

/**
 * Created by mihir on 02-11-2015.
 */
public interface IdeaConstants {

    String CHRG = "CHRG", CHARG = "CHARG";

    String LEFT = "LEFT", MAIN = "MAIN", VOL = "VOL", VOU = "VOU", DUR = "DUR";
}
