package com.app.Billbachao.usagelog.alerts.contents;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.apis.ApiUtils;
import com.app.Billbachao.usagelog.helper.UsageAlertsHelper;
import com.app.Billbachao.utils.PreferenceUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by mihir on 20-08-2015.
 */
public class PreferencesFragment extends Fragment {

    boolean mChanged = false;

    final int DEFAULT_DAYS = 20, DEFAULT_BILL_DUE = 2;

    SeekBar mDataSeek, mTalkTimeSeek, mBillDueSeek;

    DatePicker mDatePicker;

    int mDataValue, mTalkTimeValue, mBillDueValue;

    static final String NA = ApiUtils.NA;

    String mData = NA, mBalance = NA;

    boolean mIsPrepaid = true;

    Handler mUiHandler;

    public static final String DATE_FORMAT = "yyyyMMdd";

    public static PreferencesFragment getInstance() {
        return new PreferencesFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsPrepaid = PreferenceUtils.isPrepaid(getActivity());
        initValues();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_preference, container, false);
        initView(view);
        return view;
    }

    void initValues() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences
                (getActivity());
        mData = String.valueOf(preferences.getInt(PreferenceUtils.DATA_THRESHOLD,
                UsageAlertsHelper.DEFAULT_DATA));
        mBalance = String.valueOf(preferences.getInt(PreferenceUtils.TALK_THRESHOLD,
                UsageAlertsHelper.DEFAULT_TALKTIME));

    }

    void initView(View containerView) {
        if (mIsPrepaid) {
            initPrepaidView(containerView);
        } else {
            initPostpaidView(containerView);
        }

        View reset = containerView.findViewById(R.id.reset);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO events
                mChanged = true;
                if (mIsPrepaid) {
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, DEFAULT_DAYS);
                    mDatePicker.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get
                            (Calendar.DAY_OF_MONTH));
                    mDataSeek.setProgress(UsageAlertsHelper.DEFAULT_DATA);
                    mTalkTimeSeek.setProgress(UsageAlertsHelper.DEFAULT_TALKTIME);
                } else {
                    mBillDueSeek.setProgress(DEFAULT_BILL_DUE);
                }
            }
        });
    }

    void initPrepaidView(View containerView) {
        final ScrollView scrollView = (ScrollView) containerView.findViewById(R.id
                .prepaid_preferences_container);
        scrollView.setVisibility(View.VISIBLE);

        ViewGroup callBalanceGroup = (ViewGroup) containerView.findViewById(R.id
                .talktime_preference);
        initializeGroup(callBalanceGroup, R.string.balance_is_low, R.string.never, R.string
                .inr_nd, R.string.inr_nd, 80);
        mTalkTimeSeek = (SeekBar) callBalanceGroup.findViewById(R.id.preference_seekbar);

        ViewGroup dataBalanceGroup = (ViewGroup) containerView.findViewById(R.id.data_preference);
        initializeGroup(dataBalanceGroup, R.string.data_plan_consumption, R.string.never, R
                .string.nd_mb, R.string.nd_mb, 100);
        mDataSeek = (SeekBar) dataBalanceGroup.findViewById(R.id.preference_seekbar);

        initDatePicker(containerView);


        scrollView.post(new Runnable() {
            public void run() {
                scrollView.scrollTo(0, 0);
            }
        });
    }

    void initPostpaidView(View containerView) {
        containerView.findViewById(R.id.postpaid_preferences_container).setVisibility(View.VISIBLE);

        ViewGroup billDueGroup = (ViewGroup) containerView.findViewById(R.id
                .bill_due_preference);
        initializeGroup(billDueGroup, R.string.bill_due, R.string.never, R.string.n_days, R
                .string.n_days, 7);
        mBillDueSeek = (SeekBar) billDueGroup.findViewById(R.id.preference_seekbar);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mUiHandler = new Handler();
        mUiHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Code here will run in UI thread
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences
                        (getActivity());

                if (mIsPrepaid) {
                    mDataSeek.setProgress(preferences.getInt(PreferenceUtils.DATA_THRESHOLD,
                            UsageAlertsHelper.DEFAULT_DATA));
                    mTalkTimeSeek.setProgress(preferences.getInt(PreferenceUtils.TALK_THRESHOLD,
                            UsageAlertsHelper.DEFAULT_TALKTIME));
                } else {
                    mBillDueSeek.setProgress(preferences.getInt(PreferenceUtils
                                    .BILL_DUE_DAYS_REMAIN,
                            DEFAULT_BILL_DUE));
                }
            }
        }, 500);
    }

    void initializeGroup(final ViewGroup group, int titleId, int leftTextId, int rightTextId,
                         final int
            popupTextId, final int progressRange) {
        ((TextView) group.findViewById(R.id.preference_desc)).setText(titleId);
        ((TextView) group.findViewById(R.id.left_text)).setText(leftTextId);
        ((TextView) group.findViewById(R.id.right_text)).setText(getString(rightTextId,
                progressRange));

        final TextView popup = (TextView) group.findViewById(R.id.text_popup);

        SeekBar seekbar = (SeekBar) group.findViewById(R.id.preference_seekbar);
        seekbar.setMax(progressRange);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                popup.setText(getString(popupTextId, progress));
                popup.measure(0, 0);
                // Position popup text
                float ratio = progress / (float) progressRange;
                int width = seekBar.getMeasuredWidth() - popup.getMeasuredWidth();

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) popup
                        .getLayoutParams();
                params.setMargins((int) (width * ratio), 0, 0, 0);
                popup.setLayoutParams(params);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mChanged = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        mUiHandler.removeCallbacksAndMessages(null);
        if (mIsPrepaid) {
            mDataValue = mDataSeek.getProgress();
            mTalkTimeValue = mTalkTimeSeek.getProgress();

            String date = getReminderDate(mDatePicker.getDayOfMonth(), mDatePicker.getMonth(),
                    mDatePicker.getYear());
            sendPrepaidReminderUpdate(mTalkTimeValue, mDataValue, date);
        } else {
            mBillDueValue = mBillDueSeek.getProgress();
            sendPostpaidRemainderUpdate(mBillDueValue);
        }
        super.onDestroyView();

    }

    void initDatePicker(View view) {
        mDatePicker = (DatePicker) view.findViewById(R.id.date_picker);

        Calendar calendar = Calendar.getInstance();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences
                (getActivity());
        if (preferences.contains(PreferenceUtils.PLAN_EXPIRY)) {
            String planExpiry = preferences.getString(PreferenceUtils.PLAN_EXPIRY, "");
            try {
                calendar.setTime(new SimpleDateFormat(DATE_FORMAT).parse(planExpiry));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        mDatePicker.setMinDate(getDateFromToday(-1));
        mDatePicker.setMaxDate(getDateFromToday(40));
        mDatePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get
                (Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                mChanged = true;
            }
        });
    }

    void sendPostpaidRemainderUpdate(int billDueDays) {
        if (!mChanged) {
            return;
        }
        // TODO Bill due date required

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences
                (getActivity());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(PreferenceUtils.BILL_DUE_DAYS_REMAIN, billDueDays);
        editor.apply();
    }

    void sendPrepaidReminderUpdate(int talkTime, int dataValue, String expiryDate) {
        if (!mChanged) {
            return;
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences
                (getActivity());
        if (preferences.getInt(PreferenceUtils.DATA_THRESHOLD,
                UsageAlertsHelper.DEFAULT_DATA) != dataValue) {
            // Temporary value, need to change to absolute
            mData = String.valueOf(dataValue);
            UsageAlertsHelper.setAlert(getActivity(), UsageAlertsHelper.ALERT_DATA_BALANCE, true);
            // TODO events
        }

        if (preferences.getInt(PreferenceUtils.TALK_THRESHOLD,
                UsageAlertsHelper.DEFAULT_TALKTIME) != talkTime) {
            mBalance = String.valueOf(talkTime);
            UsageAlertsHelper.setAlert(getActivity(), UsageAlertsHelper.ALERT_MAIN_BALANCE, true);
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(PreferenceUtils.DATA_THRESHOLD, dataValue);
        editor.putInt(PreferenceUtils.TALK_THRESHOLD, talkTime);
        editor.putString(PreferenceUtils.PLAN_EXPIRY, expiryDate);
        editor.apply();

    }

    static String getDate(int days) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, days);
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        String returnDate = formatter.format(cal.getTime());
        return returnDate;
    }

    public long getDateFromToday(int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, days);
        return calendar.getTimeInMillis();
    }

    private String getReminderDate(int dayOfMonth, int month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, dayOfMonth);
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        String returnDate = formatter.format(cal.getTime());
        return returnDate;
    }
}
