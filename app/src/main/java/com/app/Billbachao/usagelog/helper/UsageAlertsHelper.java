package com.app.Billbachao.usagelog.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.app.Billbachao.notifications.inapp.SimpleNotification;
import com.app.Billbachao.usagelog.notifications.BalanceThresholdNotification;
import com.app.Billbachao.usagelog.notifications.DataThresholdNotification;
import com.app.Billbachao.utils.PreferenceUtils;

/**
 * Helper class for raising usage alert notifications.
 * Created by mihir on 23-11-2015.
 */

public class UsageAlertsHelper {

    static final String KEY_ALERT_ON_OFF = "_IsAlertOn";

    // Alerts list
    public static final int ALERT_MAIN_BALANCE = 1, ALERT_DATA_BALANCE = 2, ALERT_BILL_DAYS_REMAIN = 3;

    public static final int DEFAULT_DATA = 50, DEFAULT_TALKTIME = 20;

    public static boolean isAlertOn(Context context, int alert) {
        String key = getAlertKey(alert);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(key + KEY_ALERT_ON_OFF, true);
    }

    public static void setAlert(Context context, int alert, boolean mode) {
        String key = getAlertKey(alert);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences.getBoolean(key + KEY_ALERT_ON_OFF, true) == mode) {
            return;
        }
        preferences.edit().putBoolean(key + KEY_ALERT_ON_OFF, mode).apply();
    }

    public static String getAlertKey(int alert) {
        switch (alert) {
            case ALERT_MAIN_BALANCE:
                return PreferenceUtils.TALK_THRESHOLD;
            case ALERT_DATA_BALANCE:
                return PreferenceUtils.DATA_THRESHOLD;
            case ALERT_BILL_DAYS_REMAIN:
                return PreferenceUtils.BILL_DUE_DAYS_REMAIN;
            default:
                return "";
        }
    }

    public static int getDefaultValue(int alert) {
        switch (alert) {
            case ALERT_MAIN_BALANCE:
                return DEFAULT_TALKTIME;
            case ALERT_DATA_BALANCE:
                return DEFAULT_DATA;
        }
        return 0;
    }

    public static Class getAlertStorageType(int alert) {
        switch (alert) {
            case ALERT_MAIN_BALANCE:
            case ALERT_DATA_BALANCE:
            case ALERT_BILL_DAYS_REMAIN:
                return Integer.class;
            default:
                return null;
        }
    }

    public static int getAlertValue(Context context, int alert) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(getAlertKey(alert), getDefaultValue(alert));
    }

    public static void checkDataThreshold(Context context, int balance) {
        int alert = ALERT_DATA_BALANCE;
        checkThreshold(context, alert, balance);
    }

    public static void checkMainBalanceThreshold(Context context, int balance) {
        int alert = ALERT_MAIN_BALANCE;
        checkThreshold(context, alert, balance);
    }

    static void checkThreshold(Context context, int alert, int balance) {
        if (balance < 0) {
            return;
        }
        int value = getAlertValue(context, alert);
        if (isAlertOn(context, alert)) {
            if (value > balance) {
                getNotification(context, alert, balance).show();
                setAlert(context, alert, false);
            }
        } else {
            if (value < balance) {
                setAlert(context, alert, true);
            }
        }
    }

    public static SimpleNotification getNotification(Context context, int key, int balance) {
        switch (key) {
            case ALERT_DATA_BALANCE:
                return new DataThresholdNotification(context, balance);
            case ALERT_MAIN_BALANCE:
                return new BalanceThresholdNotification(context, balance);
            default:
                return null;
        }
    }
}
