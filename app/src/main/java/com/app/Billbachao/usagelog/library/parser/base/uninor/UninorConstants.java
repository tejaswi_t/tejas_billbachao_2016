package com.app.Billbachao.usagelog.library.parser.base.uninor;

/**
 * Created by mihir on 02-11-2015.
 */
public interface UninorConstants {

    String CALL_DURATION = "CALL DURATION", CALL_COST = "CALL COST", DURATION = "DURATION", SMS = "SMS", DATA = "DATA";

    String TALKTIME = "TALKTIME", REMAINING = "REMAINING";
}
