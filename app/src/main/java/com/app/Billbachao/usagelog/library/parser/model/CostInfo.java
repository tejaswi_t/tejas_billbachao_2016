package com.app.Billbachao.usagelog.library.parser.model;

/**
 * Created by mihir on 02-11-2015.
 */
public class CostInfo {

    public final static int DATA = 1, CALL = 2, SMS = 3, USSD = 4, DATA_CHECK = 5;

    public int type, duration;

    public float cost, dataUsed, dataLeft, mainBalance;

    // Validity in milliSeconds
    public long validity;

    public CostInfo() {
        mainBalance = -1;
        dataLeft = -1;
    }
}
