package com.app.Billbachao.usagelog.contents.fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.usagelog.contents.adapters.BaseResourceCursorAdapter;
import com.app.Billbachao.usagelog.contents.utils.ListUtils;

/**
 * Created by mihir on 28-10-2015.
 */
public abstract class BaseUsageFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    ExpandableListView mListView;

    BaseResourceCursorAdapter mAdapter;

    ListUtils.QueryArgs mQueryArgs;

    OnDataUpdater mDataUpdater;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View containerView = inflater.inflate(R.layout.usage_log_list_fragment, container, false);
        initView(containerView);
        return containerView;
    }

    void initView(View containerView) {
        mListView = (ExpandableListView) containerView.findViewById(R.id.usage_list);
        mListView.setOnGroupExpandListener(mGroupExpandListener);
        TextView emptyTextView = (TextView) containerView.findViewById(R.id.empty_view);
        mListView.setEmptyView(emptyTextView);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mQueryArgs = getQueryArgs();
        mAdapter = getAdapter();
        mListView.setAdapter(mAdapter);
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDataUpdater) {
            mDataUpdater = (OnDataUpdater) context;
        }
    }

    @Override
    public void onDetach() {
        mDataUpdater = null;
        super.onDetach();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), mQueryArgs.uri,
                mQueryArgs.projection, mQueryArgs.selection, mQueryArgs.selectionArgs,
                mQueryArgs.sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.initIndices(data);
        mAdapter.changeCursor(data);
        if (mDataUpdater != null) {
            mDataUpdater.onDataUpdated();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }

    public abstract BaseResourceCursorAdapter getAdapter();

    public abstract ListUtils.QueryArgs getQueryArgs();

    ExpandableListView.OnGroupExpandListener mGroupExpandListener = new ExpandableListView
            .OnGroupExpandListener() {
        int currentPosition = -1;

        @Override
        public void onGroupExpand(int groupPosition) {
            if (groupPosition != currentPosition) {
                mListView.collapseGroup(currentPosition);
                currentPosition = groupPosition;
            }
        }
    };

    public interface OnDataUpdater {
        void onDataUpdated();
    }
}
