package com.app.Billbachao.usagelog.notifications;

import android.content.Context;

import com.app.Billbachao.R;
import com.app.Billbachao.notifications.inapp.SimpleNotification;

/**
 * Created by mihir on 23-11-2015.
 */
public class DataThresholdNotification extends SimpleNotification {

    public DataThresholdNotification(Context context, int balance) {
        super(context, context.getString(R.string.data_threshold_message, context.getString(R
                .string.nd_mb, balance)), context.getString(R.string.topup_now));
    }

    /*@Override
    protected NotificationCompat.Builder getNotificationBuilder() {
        Intent intent = new Intent(mContext, BrowsePlansActivity.class);
        intent.putExtra(ConstantUtils.LAUNCH_SELF_PLANS, true);
        intent.putExtra(ConstantUtils.LAUNCH_FROM_EXTERNAL, true);
        intent.putExtra(ConstantUtils.PREFERRED_RECHARGE_TYPE, PlanUtils.ABSTRACT_PLAN_DATA);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return super.getNotificationBuilder().setContentIntent(pendingIntent);
    }*/

    @Override
    public void show() {
        super.show();
        // TODO event
        //EventUtils.sendEventYes(EventUtils.ALERTS, EventUtils.DATA_NOTI_SENT);
    }
}
