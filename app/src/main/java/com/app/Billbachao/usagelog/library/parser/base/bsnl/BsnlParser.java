package com.app.Billbachao.usagelog.library.parser.base.bsnl;

import com.app.Billbachao.usagelog.library.parser.base.BaseParser;
import com.app.Billbachao.usagelog.library.parser.model.CostInfo;

/**
 * Unique pattern found in IDEA, to know the cost amount find string COST/CHARGE. FOr finding
 * balance, find String BAL_LEFT
 * <p/>
 * Created by mihir on 25-11-2015.
 */
public class BsnlParser extends BaseParser implements BsnlConstants {

    @Override
    public boolean parse(String data) {
        if (data.contains(BAL) && data.contains(MAIN)) {
            if (data.contains(CHARGE) || data.contains(COST)) {
                return parseCostData(data);
            } else {
                return parseUssdData(data);
            }
        }
        return false;
    }

    @Override
    protected boolean parseDataUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.DATA;
        return false;
    }

    @Override
    protected boolean parseCallUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.CALL;
        return false;
    }

    @Override
    protected boolean parseSmsUsage(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.SMS;
        return false;
    }

    @Override
    protected boolean parseUssdData(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.USSD;
        mCostInfo.mainBalance = extractFloats(data, 1)[0];
        return true;
    }

    boolean parseCostData(String data) {
        mCostInfo = new CostInfo();
        mCostInfo.type = CostInfo.USSD;
        float[] amounts = extractFloats(data, 2);

        if (amounts[1] >= 0) {
            mCostInfo.mainBalance = amounts[1];
            mCostInfo.cost = amounts[0];
        } else {
            mCostInfo.mainBalance = amounts[0];
        }
        return true;
    }
}

