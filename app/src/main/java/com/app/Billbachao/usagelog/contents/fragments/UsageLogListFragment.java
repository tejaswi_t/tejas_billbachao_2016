package com.app.Billbachao.usagelog.contents.fragments;

import com.app.Billbachao.usagelog.contents.adapters.BaseResourceCursorAdapter;
import com.app.Billbachao.usagelog.contents.adapters.CallLogsAdapter;
import com.app.Billbachao.usagelog.contents.utils.ListUtils;

/**
 * Created by mihir on 20-10-2015.
 */
public class UsageLogListFragment extends BaseUsageFragment {

    public static final String TAG = UsageLogListFragment.class.getSimpleName();

    public static UsageLogListFragment getInstance() {
        return new UsageLogListFragment();
    }

    // Flag for showing app/system call logs
    public static final boolean SHOW_APP_CALL_LOGS = true;

    @Override
    public BaseResourceCursorAdapter getAdapter() {
        return new CallLogsAdapter(null, getActivity(), SHOW_APP_CALL_LOGS);
    }

    @Override
    public ListUtils.QueryArgs getQueryArgs() {
        return SHOW_APP_CALL_LOGS ? new ListUtils.AppCallLogsArgs() : new ListUtils.CallLogsArgs();
    }

}
