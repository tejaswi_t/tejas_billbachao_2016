package com.app.Billbachao.usagelog.contents.utils;

import com.app.Billbachao.R;

/**
 * Created by mihir on 19-10-2015.
 */
public class LogUtils {

    public static final String TYPE_USAGE = "usage";

    public static final int TYPE_CALL = 0x01, TYPE_DATA = 0x02;

    public static int getDescriptionId(int key) {
        switch (key) {
            case TYPE_CALL:
                return R.string.current_balance;
            case TYPE_DATA:
                return R.string.data_balance;
        }
        return 0;
    }

    public static int getBalanceFormatString(int key) {
        switch (key) {
            case TYPE_DATA:
                return R.string.n2_mb;
            case TYPE_CALL:
                return R.string.inr_n;
        }
        return 0;
    }

    public static int getDefaultFormatString(int key) {
        switch (key) {
            case TYPE_DATA:
                return R.string.n_mb;
            case TYPE_CALL:
                return R.string.inr_n;
        }
        return 0;
    }

    public static int getCardTitleId(int key) {
        switch (key) {
            case TYPE_CALL:
                return R.string.current_balance;
            case TYPE_DATA:
                return R.string.data_balance;
        }
        return 0;
    }

    public static int getCardIconId(int key) {
        switch (key) {
            case TYPE_CALL:
                return R.drawable.icn_calls;
            case TYPE_DATA:
                default:
                return R.drawable.icn_data;
        }
    }

    public static int getTitleId(int key) {
        switch (key) {
            case TYPE_CALL:
                return R.string.main_balance_title;
            case TYPE_DATA:
                return R.string.data_balance_title;
        }
        return 0;
    }
}
