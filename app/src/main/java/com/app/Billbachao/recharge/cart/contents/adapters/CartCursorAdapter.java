package com.app.Billbachao.recharge.cart.contents.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.app.Billbachao.R;
import com.app.Billbachao.recharge.cart.CartPlanHelper;
import com.app.Billbachao.recharge.cart.provider.CartPlansProvider;
import com.app.Billbachao.utils.CircleUtils;
import com.app.Billbachao.utils.OperatorUtils;

import java.util.ArrayList;

/**
 * Created by mihir on 04-05-2016.
 */
public class CartCursorAdapter extends CursorAdapter {

    public static final String TAG = CartCursorAdapter.class.getSimpleName();

    Context mContext;

    int mMobileIdx, mPlanIdx, mNameIdx, mCostIdx, mOperatorIdx, mCircleIdx, mPrepaidIdx, mIdx;

    ArrayList<Integer> mSectionHeaderIndices;

    PlanAddRequester mPlanAddRequester;

    public CartCursorAdapter(Context context, Cursor cursor, PlanAddRequester planAddRequester) {
        super(context, cursor, false);
        mContext = context;
        mSectionHeaderIndices = new ArrayList<>();
        mPlanAddRequester = planAddRequester;
    }

    public void updateCursorIndices(Cursor cursor) {
        updateColumnIndices(cursor);
        updateHeaderIndices(cursor);
    }

    private void updateColumnIndices(Cursor cursor) {
        if (cursor == null) return;
        mNameIdx = cursor.getColumnIndex(CartPlansProvider.PlanDetails.PLAN_NAME);
        mPlanIdx = cursor.getColumnIndex(CartPlansProvider.PlanDetails.PLAN_ID);
        mCostIdx = cursor.getColumnIndex(CartPlansProvider.PlanDetails.PLAN_COST);
        mCircleIdx = cursor.getColumnIndex(CartPlansProvider.PlanDetails.CIRCLE_ID);
        mOperatorIdx = cursor.getColumnIndex(CartPlansProvider.PlanDetails.OPERATOR_ID);
        mMobileIdx = cursor.getColumnIndex(CartPlansProvider.PlanDetails.MOBILE_NO);
        mPrepaidIdx = cursor.getColumnIndex(CartPlansProvider.PlanDetails.CONNECTION_TYPE);
        mIdx = cursor.getColumnIndex(CartPlansProvider.PlanDetails.ID);
    }

    private void updateHeaderIndices(Cursor cursor) {
        mSectionHeaderIndices.clear();
        if (cursor == null || !cursor.moveToFirst()) return;
        String saveNumber = "";
        do {
            String number = cursor.getString(mMobileIdx);
            if (number != null && !number.equalsIgnoreCase(saveNumber)) {
                mSectionHeaderIndices.add(cursor.getPosition());
                saveNumber = number;
            }
        } while (cursor.moveToNext());
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = View.inflate(mContext, R.layout.cart_plan_item_row, null);
        view.setTag(new PlanViewHolder(view));
        return view;
    }

    @Override
    public void bindView(View view, Context context, final Cursor cursor) {
        PlanViewHolder holder = (PlanViewHolder) view.getTag();
        holder.nametext.setText(cursor.getString(mNameIdx));
        holder.costText.setText(mContext.getString(R.string.inr_d, cursor.getInt(mCostIdx)));
        bindSectionHeaderView(holder, context, cursor);
        final String id = cursor.getString(mIdx);
        holder.removeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CartPlanHelper.deletePlanByPrimaryId(mContext, id);
            }
        });
    }

    private void bindSectionHeaderView(PlanViewHolder holder, final Context context, final Cursor cursor) {
        if (mSectionHeaderIndices.contains(cursor.getPosition())) {
            final String mobile = cursor.getString(mMobileIdx);
            final String circleId = cursor.getString(mCircleIdx);
            final String operatorId = cursor.getString(mOperatorIdx);
            holder.sectionHeader.setVisibility(View.VISIBLE);
            holder.mobileText.setText(mobile);
            boolean isPrepaid = cursor.getInt(mPrepaidIdx) == CartPlansProvider.PlanDetails.PREPAID;
            holder.prepaidText.setText(isPrepaid ? R.string.prepaid : R.string.postpaid);
            holder.operatorCircleText.setText(mContext.getString(R.string.operator_circle,
                    OperatorUtils.getOperatorNameFromId(operatorId), CircleUtils.getCircleNameFromId(circleId)));
            if (isPrepaid) {
                holder.addMore.setVisibility(View.VISIBLE);
                holder.addMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mPlanAddRequester != null)
                            mPlanAddRequester.onPlanRequested(mobile, circleId, operatorId);
                    }
                });
            } else {
                holder.addMore.setVisibility(View.GONE);
            }
        } else {
            holder.sectionHeader.setVisibility(View.GONE);
        }
        //Divider logic
        holder.divider.setVisibility(mSectionHeaderIndices.contains(cursor.getPosition() + 1) ?
                View.VISIBLE : View.GONE);
    }

    static class PlanViewHolder {

        TextView costText, nametext, mobileText, operatorCircleText, prepaidText;

        View sectionHeader, divider;

        View removeView;

        View addMore;

        public PlanViewHolder(View itemView) {
            nametext = (TextView) itemView.findViewById(R.id.plan_name);
            costText = (TextView) itemView.findViewById(R.id.plan_cost);
            mobileText = (TextView) itemView.findViewById(R.id.mobile_number);
            operatorCircleText = (TextView) itemView.findViewById(R.id.operator_circle);
            prepaidText = (TextView) itemView.findViewById(R.id.prepaid);
            sectionHeader = itemView.findViewById(R.id.plan_item_header);
            removeView = itemView.findViewById(R.id.remove);
            divider = itemView.findViewById(R.id.divider);
            addMore = itemView.findViewById(R.id.add_more_plans);
        }
    }

    public interface PlanAddRequester {
        void onPlanRequested(String mobile, String circleId, String operatorId);
    }
}
