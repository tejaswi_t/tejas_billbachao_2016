package com.app.Billbachao.recharge.plans.utils;

import android.content.Context;

import com.app.Billbachao.R;
import com.app.Billbachao.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by mihir on 17-08-2015.
 * Utilities class for maintaining various type information, so as to make a single fragment
 * class for all recharge types
 */
public class PlanUtils {

    private static final int TYPE_TOP_UP = 1, TYPE_FULL_TALKTIME = 2, TYPE_SPECIAL_RECHARGE = 3,
            TYPE_2G = 4, TYPE_3G = 5, TYPE_SMS = 6, TYPE_ISD = 7, TYPE_ROAMING = 8;

    public static final int TYPE_INVALID = -1;

    // Keep list of types for counting types
    public static final int[] TYPE_LIST = new int[]{TYPE_FULL_TALKTIME, TYPE_TOP_UP,
            TYPE_SPECIAL_RECHARGE, TYPE_2G, TYPE_3G, TYPE_SMS, TYPE_ISD, TYPE_ROAMING};

    public static final int COUNT_TYPES = TYPE_LIST.length;

    public static final String UNKNOWN = "Unknown";

    private static ArrayList<String> sPlanCategories = new ArrayList<>();

    private static boolean sPickedFromNetwork;

    private static int sNetworkCategoriesCount = 0;

    private static ArrayList<Integer> sFixedPlanCategories = new ArrayList<>();

    static {
        for (int planType : TYPE_LIST) {
            sFixedPlanCategories.add(getTitleId(planType));
        }
    }

    public static String getMode(int type) {
        switch (type) {
            case TYPE_TOP_UP:
                return "TT";
            case TYPE_FULL_TALKTIME:
                return "FTT";
            case TYPE_ROAMING:
                return "ROAM";
            case TYPE_ISD:
                return "ISD";
            case TYPE_SMS:
                return "SMS";
            case TYPE_2G:
                return "2G";
            case TYPE_3G:
                return "3G";
            case TYPE_SPECIAL_RECHARGE:
                return "SPR";
            default:
                return "";
        }
    }

    public static int getTitleId(int type) {
        switch (type) {
            case TYPE_TOP_UP:
                return R.string.type_top_up;
            case TYPE_FULL_TALKTIME:
                return R.string.type_full_top_up;
            case TYPE_ROAMING:
                return R.string.type_roaming;
            case TYPE_ISD:
                return R.string.type_isd;
            case TYPE_SMS:
                return R.string.type_sms;
            case TYPE_2G:
                return R.string.type_2g;
            case TYPE_3G:
                return R.string.type_3g;
            case TYPE_SPECIAL_RECHARGE:
                return R.string.type_special;
            default:
                return R.string.type_invalid;
        }
    }

    public static void updateCategoriesFromNetwork(String categoryList) {
        String[] categoryTokens = categoryList.split(",");

        for (int i = 0; i < categoryTokens.length; i++)
            categoryTokens[i] = categoryTokens[i].trim();

        sPlanCategories = new ArrayList<String>(Arrays.asList(categoryTokens));
        sNetworkCategoriesCount = sPlanCategories.size();
        sPickedFromNetwork = true;
    }

    public static int getCount(boolean isPickedFromNetwork) {
        return isPickedFromNetwork ? sNetworkCategoriesCount : COUNT_TYPES;
    }

    public static String getTitle(Context context, int index, boolean isPickedFromNetwork) {
        if (isPickedFromNetwork) {
            return sPlanCategories.get(index);
        }
        return context.getString(getTitleId(TYPE_LIST[index]));
    }

    public static String getPlanTypeText(int index, boolean isPickedFromNetwork) {
        if (isPickedFromNetwork) {
            return sPlanCategories.get(index);
        }
        return getMode(PlanUtils.TYPE_LIST[index]);
    }

    public static boolean isPickedFromNetwork() {
        return sPickedFromNetwork;
    }

    public static int getPlanIndex(Context context, String planTitle, boolean isPickedFromNetwork) {
        planTitle = planTitle.toLowerCase();
        if (isPickedFromNetwork) {
            int index = 0;
            for (String matchString : sPlanCategories) {
                if (matchString.toLowerCase().contains(planTitle)) {
                    return index;
                }
                index++;
            }
        }
        int index = 0;
        for (int planType : sFixedPlanCategories) {
            if (context.getString(getTitleId(planType)).toLowerCase().contains(planTitle)) {
                return index;
            }
            index++;
        }
        return -1;
    }

    // Abstract recharge types for alerts etc purpose, like call, data, roaming etc.
    public static final int ABSTRACT_PLAN_DATA = 1, ABSTRACT_PLAN_CALL = 2, ABSTRACT_PLAN_ROAMING
            = 3;

    public static String getAbstractPlanName(Context context, int type) {
        if (type == ABSTRACT_PLAN_CALL) {
            return context.getString(R.string.abstract_type_call);
        }
        if (type == ABSTRACT_PLAN_DATA) {
            String networkType = PreferenceUtils.getNetworkType(context);
            if (networkType.equalsIgnoreCase(PreferenceUtils.NETWORK_3G)) {
                return context.getString(R.string.type_3g);
            } else if (networkType.equalsIgnoreCase(PreferenceUtils.NETWORK_2G)) {
                return context.getString(R.string.type_2g);
            } else {
                return context.getString(R.string.type_4g);
            }
        }
        if (type == ABSTRACT_PLAN_ROAMING) {
            return context.getString(R.string.type_roaming);
        }
        return "-";
    }
}
