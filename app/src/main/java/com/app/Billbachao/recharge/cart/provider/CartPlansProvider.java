package com.app.Billbachao.recharge.cart.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.app.Billbachao.utils.ILog;

/**
 * Created by rajkumar on 2/9/2016.
 */
public class CartPlansProvider extends ContentProvider {

    static final String TAG = CartPlansProvider.class.getSimpleName();

    static final String DB_NAME = "PlanDetails.db";

    // Version 3 for adding prepaid/postpaid field
    static int DB_VERSION = 3;

    SQLiteDatabase mDatabase;

    private CartSQLiteHelper mCartSQLiteHelper;

    public static final String AUTHORITY = "com.app.Billbachao.plan.cartprovider", RAW_QUERY_PATH = "/rawQuery", GROUP_BY_QUERY = "/groupBy";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final int PLAN_DETAILS = 1, GROUP_BY = 2;

    private static final UriMatcher URI_MATCHER;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(AUTHORITY, PlanDetails.TABLE_NAME, PLAN_DETAILS);
    }

    @Override
    public boolean onCreate() {
        mCartSQLiteHelper = new CartSQLiteHelper(getContext(), DB_NAME, null, DB_VERSION);
        try {
            mDatabase = mCartSQLiteHelper.getWritableDatabase();
        } catch (SQLiteException ex) {
            mDatabase = mCartSQLiteHelper.getReadableDatabase();
        }
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor = null;
        switch (URI_MATCHER.match(uri)) {
            case PLAN_DETAILS:
                cursor = mDatabase.query(PlanDetails.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
        }
        if (cursor != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        Uri insertUri = null;
        long rowId = -1;
        switch (URI_MATCHER.match(uri)) {
            case PLAN_DETAILS:
                rowId = mDatabase.insert(PlanDetails.TABLE_NAME, null, values);
                insertUri = Uri.withAppendedPath(PlanDetails.CONTENT_URI, Long.toString(rowId));
                break;
        }
        if (rowId != -1) {
            ILog.d(TAG, "Row inserted " + rowId);
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return insertUri;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        int count = 0;
        switch (URI_MATCHER.match(uri)) {
            case PLAN_DETAILS:
                for (ContentValues value : values) {
                    if (mDatabase.insert(PlanDetails.TABLE_NAME, null, value) != -1) {
                        count++;
                    }
                }
                break;
        }
        ILog.d(TAG, "Inserted rows: " + count);
        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;
        switch (URI_MATCHER.match(uri)) {
            case PLAN_DETAILS:
                count = mDatabase.delete(PlanDetails.TABLE_NAME, selection, selectionArgs);
                break;
        }
        ILog.d(TAG, "Deleted rows: " + count+ " Uri "+ uri);
        if (count != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count = -1;
        switch (URI_MATCHER.match(uri)) {
            case PLAN_DETAILS:
                count = mDatabase.update(PlanDetails.TABLE_NAME, values, selection, selectionArgs);
                break;
        }
        ILog.d(TAG, "Rows updated " + count);
        if (count != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    static class CartSQLiteHelper extends SQLiteOpenHelper {

        static CartSQLiteHelper sCartSQLiteHelper;

        public static CartSQLiteHelper getInstance(Context context) {
            if (sCartSQLiteHelper == null) {
                sCartSQLiteHelper = new CartSQLiteHelper(context.getApplicationContext(), DB_NAME, null, DB_VERSION);
            }
            return sCartSQLiteHelper;
        }

        public CartSQLiteHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        public CartSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            createPlansTable(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(PlanDetails.DROP_TABLE);
            createPlansTable(db);
        }

        void createPlansTable(SQLiteDatabase db) {
            db.execSQL(PlanDetails.CREATE_TABLE);
        }
    }

    public static class PlanDetails {

        public static final String TABLE_NAME = "plans";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(
                CartPlansProvider.CONTENT_URI, TABLE_NAME);

        public static final String ID = "_id", PLAN_ID = "planId", PLAN_NAME = "planName",
                PLAN_DESCRIPTION = "planDesc", PLAN_COST = "planCost", PLAN_COUNT = "planCount",
                PLAN_VALIDITY = "planvalidity", PLAN_MB = "planmb", CIRCLE_ID = "cId", OPERATOR_ID =
                "opId", MOBILE_NO = "mobileNo", ADDED_FROM = "addedfrom", CONNECTION_TYPE =
                "connectionType";

        public static final int PREPAID = 0, POSTPAID = 1;

        static final String CREATE_TABLE = "create table " + TABLE_NAME
                + " (" + ID + " integer primary key autoincrement"
                + ", " + PLAN_ID + " integer"
                + ", " + PLAN_NAME + " text"
                + ", " + PLAN_DESCRIPTION + " text"
                + ", " + PLAN_COST + " integer"
                + ", " + PLAN_COUNT + " integer"
                + ", " + PLAN_VALIDITY + " text"
                + ", " + PLAN_MB + " text"
                + ", " + CIRCLE_ID + " integer"
                + ", " + OPERATOR_ID + " integer"
                + ", " + MOBILE_NO + " text"
                + ", " + ADDED_FROM + " integer"
                + ", " + CONNECTION_TYPE + " integer"
                + ");";

        // Raw query column field
        public static final String TOTAL = "total";

        public static final String GROUP_BY_MOBILE_QUERY = "SELECT " + MOBILE_NO + ", " +
                CIRCLE_ID + ", " + OPERATOR_ID + ", SUM(" + PLAN_COST + ") as " + TOTAL + " FROM "
                + TABLE_NAME + " where 1 group by " + MOBILE_NO;

        public static final String DROP_TABLE = "drop table if exists " + PlanDetails.TABLE_NAME;
    }
}
