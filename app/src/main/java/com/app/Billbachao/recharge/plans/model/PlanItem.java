package com.app.Billbachao.recharge.plans.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.Billbachao.recharge.plans.utils.PlanUtils;
import com.google.myjson.annotations.SerializedName;


/**
 * Created by mihir on 14-08-2015.
 * Primarily used with browse plan screen and quick recharge
 */
public class PlanItem implements Parcelable {

    public static final String PLAN_ITEM = "plan";

    public static final String PLAN_NAME = "planName", DESCRIPTION = "description", COST =
            "cost", TALKTIME = "talkTime", VALIDITY = "validity", ID = "planId", DATA =
            "dataInMB", ONLINE = "isOnline", BLOCKED = "isBlocked", NO_PLAN = "NoPlan", TYPE =
            "type", PLAN_COUNT = "pCnt";

    // Used for postpaid plan
    public static final String POSTPAID_BILL = "POSTPAID BILL", BASIC_VALIDITY = "30";

    // Used for custom plans
    public static final String CUSTOM = "CUSTOM PLAN";

    public static final String DEFAULT_VALIDITY = "28";

    @SerializedName(ID)
    int id = -1;

    @SerializedName(PLAN_NAME)
    String title;

    @SerializedName(DESCRIPTION)
    String description;

    @SerializedName(COST)
    int value;

    @SerializedName(VALIDITY)
    String validity;

    @SerializedName(TYPE)
    private String type;

    @SerializedName(BLOCKED)
    String blocked;

    @SerializedName(ONLINE)
    String online;

    @SerializedName(DATA)
    String dataMb;

    @SerializedName(TALKTIME)
    String talkTime;

    @SerializedName(PLAN_COUNT)
    int planCount = 1;

    protected PlanItem(Parcel source) {
        id = source.readInt();
        title = source.readString();
        description = source.readString();
        validity = source.readString();
        value = source.readInt();
        type = source.readString();
        blocked = source.readString();
        online = source.readString();
        dataMb = source.readString();
        talkTime = source.readString();
        planCount = source.readInt();
    }

    private PlanItem() {
    }

    public PlanItem(int id, String title, String description, String validity, int value, String
            type, String blocked, String online, String dataMb, String talkTime, int planCount) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.validity = validity;
        this.value = value;
        this.type = type;
        this.blocked = blocked;
        this.online = online;
        this.dataMb = dataMb;
        this.talkTime = talkTime;
        this.planCount = planCount;
    }

    protected PlanItem(PlanItem item) {
        this.id = item.id;
        this.title = item.title;
        this.description = item.description;
        this.validity = item.validity;
        this.value = item.value;
        this.type = item.type;
        this.blocked = item.blocked;
        this.online = item.online;
        this.dataMb = item.dataMb;
        this.talkTime = item.talkTime;
        this.planCount = item.planCount;
    }

    public static PlanItem getPostpaidPlanInstance(String amount) {
        return new PlanItem(0, POSTPAID_BILL, "", BASIC_VALIDITY, (int) Double.parseDouble
                (amount), PlanUtils.UNKNOWN, "", "", "", "", 1);
    }

    public static PlanItem getCustomInstance(String amount) {
        return new PlanItem(0, CUSTOM, "", BASIC_VALIDITY, (int) Double.parseDouble(amount),
                PlanUtils.UNKNOWN, "", "", "", "", 1);
    }

    public int getId() {
        return id;
    }

    public String getValidity() {
        return validity;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getValue() {
        return value;
    }

    public String getType() {
        return type;
    }

    public String getBlocked() {
        return blocked;
    }

    public String getOnline() {
        return online;
    }

    public String getDataMb() {
        return dataMb;
    }

    public String getTalkTime() {
        return talkTime;
    }

    public int getPlanCount() {
        return planCount;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(validity);
        dest.writeInt(value);
        dest.writeString(type);
        dest.writeString(blocked);
        dest.writeString(online);
        dest.writeString(dataMb);
        dest.writeString(talkTime);
        dest.writeInt(planCount);
    }

    public static final Creator<PlanItem> CREATOR = new Creator<PlanItem>() {

        @Override
        public PlanItem createFromParcel(Parcel source) {
            return new PlanItem(source);
        }

        @Override
        public PlanItem[] newArray(int size) {
            return new PlanItem[size];
        }

    };

    @Override
    public boolean equals(Object o) {
        if (o instanceof PlanItem) {
            PlanItem item = (PlanItem) o;
            return (value == item.getValue());
        }
        return false;
    }

    public boolean isValid() {
        return id >= 0;
    }

    public boolean isBlocked() {
        return blocked.equalsIgnoreCase("y");
    }

    public boolean isOnline() {
        return online.equalsIgnoreCase("y");
    }
}
