package com.app.Billbachao.recharge.quick;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import com.app.Billbachao.recharge.quick.model.ContactInfo;
import com.app.Billbachao.utils.ILog;
import com.app.Billbachao.utils.NumberUtils;

import java.util.ArrayList;

/**
 * Created by mihir on 05-05-2016.
 */
public class RechargeHelper {

    public static final String TAG = RechargeHelper.class.getSimpleName();

    public static ArrayList<ContactInfo> getContactsData(Context context) {
        ILog.d(TAG, "getContactsData start");
        ArrayList<ContactInfo> infoArrayList = new ArrayList<>();
        Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone
                        .CONTENT_URI, new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER,
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME}, null, null,
                ContactsContract.CommonDataKinds.Phone.NUMBER);
        if (cursor == null) return infoArrayList;
        if (!cursor.moveToFirst()) {
            cursor.close();
            return infoArrayList;
        }
        int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        ArrayList<String> numberList = new ArrayList<>(), nameList = new ArrayList<>();
        do {
            String number = cursor.getString(numberIndex);
            number = NumberUtils.parseNumber(number);
            if (number != null) {
                String name = cursor.getString(nameIndex);
                boolean duplicate = false;
                int index = numberList.indexOf(number);
                if (index >= 0) {
                    duplicate = nameList.get(index).equalsIgnoreCase(name);
                }
                if (!duplicate) {
                    infoArrayList.add(new ContactInfo(number, name));
                    numberList.add(number);
                    nameList.add(name);
                }
            }
        } while (cursor.moveToNext());
        cursor.close();
        numberList.clear();
        nameList.clear();
        ILog.d(TAG, "getContactsData end size " + infoArrayList.size());
        return infoArrayList;
    }

    /**
     * Returns a basic contact info obtained from picker Uri data
     * @param context
     * @param contactUri
     * @return
     */
    public static ContactInfo getContactInfo(Context context, Uri contactUri) {
        Cursor cursor = context.getContentResolver().query(contactUri, null, null, null, null);
        ContactInfo info = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                String number = cursor.getString(cursor.getColumnIndex(ContactsContract
                        .CommonDataKinds.Phone.NUMBER));
                number = NumberUtils.parseNumber(number);
                if (number != null) {
                    String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract
                            .CommonDataKinds.Phone.DISPLAY_NAME));
                    info = new ContactInfo(number, contactName);
                }
            }
            cursor.close();
        }
        return info;
    }
}
