package com.app.Billbachao.recharge.plans.contents.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.app.Billbachao.BaseActivity;
import com.app.Billbachao.R;
import com.app.Billbachao.apis.BrowsePlansApi;
import com.app.Billbachao.recharge.cart.CartPlanHelper;
import com.app.Billbachao.recharge.plans.model.PlanItem;
import com.app.Billbachao.recharge.plans.utils.PlanUtils;
import com.app.Billbachao.utils.ConstantUtils;
import com.app.Billbachao.utils.PreferenceUtils;
import com.app.Billbachao.volley.request.NetworkGsonRequest;
import com.app.Billbachao.volley.utils.NetworkErrorHelper;
import com.app.Billbachao.volley.utils.VolleySingleTon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mihir on 25-04-2016.
 */
public class PlanListFragment extends BasePlanFragment<PlanItem> {

    public static final String TAG = PlanListFragment.class.getSimpleName();

    String mPlanType = PlanUtils.UNKNOWN;

    public static final String PLAN_TYPE = "planType";

    int mPendingPosition = -1;

    public static PlanListFragment getInstance(String planTypeText, String circleId, String
            operatorId, String amount, boolean launchForSelf) {
        PlanListFragment fragment = new PlanListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PLAN_TYPE, planTypeText);
        bundle.putBoolean(ConstantUtils.LAUNCH_SELF_PLANS, launchForSelf);
        bundle.putString(PreferenceUtils.CIRCLE, circleId);
        bundle.putString(PreferenceUtils.OPERATOR, operatorId);
        bundle.putString(PreferenceUtils.AMOUNT, amount);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPlanType = getArguments().getString(PLAN_TYPE);
        }
    }

    @Override
    public void onDestroy() {
        VolleySingleTon.getInstance(getActivity()).cancelPendingRequests(mPlanType);
        super.onDestroy();
    }

    @Override
    protected RecyclerView.Adapter getAdapter() {
        return new PlansAdapter(getActivity(), mPlansList);
    }

    @Override
    void fetchPlanList() {
        NetworkGsonRequest<PlanItem[]> planFetchRequest = new NetworkGsonRequest
                <>(Request.Method.POST, BrowsePlansApi.URL, PlanItem[].class, BrowsePlansApi
                .getParams(getActivity(), mCircleId, mOperatorId, mPlanType, mAmount), new
                Response.Listener<PlanItem[]>() {
                    @Override
                    public void onResponse(PlanItem[] response) {
                        mPlansList = new ArrayList<PlanItem>(Arrays.asList(response));
                        loadData();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                setEmptyText(NetworkErrorHelper.getErrorStatus(error).getErrorMessage());
            }
        });
        VolleySingleTon.getInstance(getActivity()).addToRequestQueue(planFetchRequest, mPlanType);
    }

    public void updateData() {
        if (mPlansAdapter != null) {
            mPlansAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Plan selected callback, usually called on click, or after animation end
     * @param position
     * @param checkValidation
     * @return
     */
    boolean planSelected(int position, boolean checkValidation) {
        PlanItem item = mPlansList.get(position);

        if (checkValidation && !canPlanBeAdded(item, true)) return false;

        if (mPlanListener != null) {
            item.setType(mPlanType);
            mPlanListener.onPlanSelected(item, false);
        }
        return true;
    }

    /**
     * Check if plan is valid, and show error message if needed
     * @param item
     * @param showMessage
     * @return
     */
    boolean canPlanBeAdded(PlanItem item, boolean showMessage) {
        if (item.isBlocked()) {
            if (showMessage)
                ((BaseActivity) getActivity()).showSnack(R.string.recharge_not_supported);
            return false;
        }

        if (!item.isOnline()) {
            if (showMessage)
                ((BaseActivity) getActivity()).showSnack(R.string.online_recharge_not_supported);
            return false;
        }
        return true;
    }

    public class PlansAdapter extends RecyclerView.Adapter<PlansAdapter.CustomViewHolder> {

        Context mContext;

        List<PlanItem> mItems;

        LayoutInflater inflater;

        Animation fromAnim, toAnim;


        public PlansAdapter(Context context, List<PlanItem> items) {
            mItems = items;
            mContext = context;
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            initResources();
        }

        void initResources() {
            fromAnim = AnimationUtils.loadAnimation(mContext, R.anim.from_middle);
            toAnim = AnimationUtils.loadAnimation(mContext, R.anim.to_middle);
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.plan_item, null);
            CustomViewHolder holder = new CustomViewHolder(view);
            view.setTag(holder);
            view.setOnClickListener(mClickListener);
            return holder;
        }

        @Override
        public void onBindViewHolder(CustomViewHolder holder, final int position) {
            PlanItem planItem = mItems.get(position);
            holder.value.setText(getString(R.string.inr_n, String.valueOf(planItem.getValue())));
            holder.main.setText(planItem.getDescription());
            holder.sub.setText(mContext.getString(R.string.validity_n_days, planItem.getValidity
                    ()));
            if (mLaunchForSelf) {
                if (CartPlanHelper.isPlanInCart(getActivity(), planItem.getId(), mMobileNumber)) {
                    holder.parentView.setBackgroundResource(R.color
                            .background_material_light_selected);
                    holder.planSelector.setImageResource(R.drawable.plan_selected_circle);
                    holder.value.setTextColor(Color.WHITE);
                    holder.isChecked = true;
                } else {
                    holder.parentView.setBackgroundResource(android.R.color.transparent);
                    holder.planSelector.setImageResource(R.drawable.plan_circle);
                    holder.value.setTextColor(Color.BLACK);
                    holder.isChecked = false;
                }
            }
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        View.OnClickListener mClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = mRecyclerView.getChildAdapterPosition(v);
                if (position != -1) {
                    if (mLaunchForSelf) {
                        if (canPlanBeAdded(mPlansList.get(position), true)) {
                            // Delay it in animation
                            mPendingPosition = position;
                            interpolateView(v);
                        }
                    } else {
                        planSelected(position, true);
                    }
                }
            }
        };


        void interpolateView(View view) {
            CustomViewHolder holder = (CustomViewHolder) view.getTag();
            holder.animView.clearAnimation();
            setListeners(holder);
            holder.animView.startAnimation(toAnim);
        }

        void setListeners(final CustomViewHolder holder) {

            Animation.AnimationListener toAnimationListener = new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    holder.planSelector.setImageResource(holder.isChecked ? R.drawable
                            .plan_circle : R.drawable.plan_selected_circle);
                    holder.value.setTextColor(holder.isChecked ? Color.BLACK : Color.WHITE);
                    holder.animView.startAnimation(fromAnim);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            };

            Animation.AnimationListener fromAnimationListener = new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    if (mPlansList.size() > mPendingPosition && mPendingPosition >= 0)
                        planSelected(mPendingPosition, false);
                    mPendingPosition = -1;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            };

            toAnim.setAnimationListener(toAnimationListener);
            fromAnim.setAnimationListener(fromAnimationListener);
        }

        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected TextView value, main, sub;
            protected ImageView planSelector;
            protected View parentView, animView, mainView;
            boolean isChecked;

            public CustomViewHolder(View view) {
                super(view);
                mainView = view;
                parentView = view.findViewById(R.id.parent);
                value = (TextView) view.findViewById(R.id.plan_value_text);
                main = (TextView) view.findViewById(R.id.plan_main_text);
                sub = (TextView) view.findViewById(R.id.plan_sub_text);
                planSelector = (ImageView) view.findViewById(R.id.plan_circle);
                animView = view.findViewById(R.id.anim_view);
            }
        }
    }
}